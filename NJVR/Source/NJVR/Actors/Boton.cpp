// Fill out your copyright notice in the Description page of Project Settings.


#include "Boton.h"
#include "PanelBotones.h"

// Sets default values
ABoton::ABoton() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABoton::BeginPlay() {
	Super::BeginPlay();
	//actualizar a mi material normal
	
}

// Called every frame
void ABoton::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

void ABoton::SendSignalPressed() {
    APanelBotones * Panel = Cast<APanelBotones>(GetOwner());
    if (Panel) {
        Panel->Press(TareaBoton);
    }
}

void ABoton::SendSignalReleased() {
    APanelBotones * Panel = Cast<APanelBotones>(GetOwner());
    if (Panel) {
        Panel->Release(TareaBoton);
    }
}

void ABoton::SendSignalActivated() {
    APanelBotones * Panel = Cast<APanelBotones>(GetOwner());
    if (Panel) {
        Panel->Activate(TareaBoton);
    }
}

void ABoton::SendSignalDeactivated() {
    APanelBotones * Panel = Cast<APanelBotones>(GetOwner());
    if (Panel) {
        Panel->Deactivate(TareaBoton);
    }
}

void ABoton::Presionado() {
}

void ABoton::Soltado() {
}

void ABoton::NoPresionado() {
}

void ABoton::Activado() {
}

void ABoton::Desactivado() {
}

void ABoton::UpdateMaterial() {
	Boton->SetMaterial(0, MaterialNormal);
}

