// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Materials/Material.h"
#include "Boton.generated.h"


UENUM(BlueprintType)
enum class EButtonTask : uint8 {
    ESeleccionarButton UMETA(DisplayName = "Seleccionar"),
    EColorearButton UMETA(DisplayName = "Colorear"),
    EContenidoButton UMETA(DisplayName = "Contenido"),
    ELabelButton UMETA(DisplayName = "Etiqueta"),
    ECleanButton UMETA(DisplayName = "Limpiar"),
    EDataButton UMETA(DisplayName = "Dataset"),
    EOpcionesButton UMETA(DisplayName = "Opciones")
};

UCLASS()
class NJVR_API ABoton : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoton();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Botones")
    EButtonTask TareaBoton;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
    UStaticMeshComponent * Boton;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Transformation")
    UStaticMeshComponent * Borde;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialNormal;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialPresionado;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Transformation")
    UCapsuleComponent * ColisionControl;
	
    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void SendSignalPressed();

    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void SendSignalReleased();

    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void SendSignalActivated();

    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void SendSignalDeactivated();

    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void Presionado();

    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void Soltado();

    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void NoPresionado();

    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void Activado();

    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void Desactivado();

    UFUNCTION(BlueprintCallable, Category = "Botones")
    virtual void UpdateMaterial();
};
