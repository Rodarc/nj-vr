// Fill out your copyright notice in the Description page of Project Settings.


#include "BotonPush.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "../Pawns/VRPawn.h"
#include "Engine/Engine.h"
#include "Engine/StaticMesh.h"


// Sets default values
ABotonPush::ABotonPush() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    FVector EscalaBotones = FVector(0.5f);
    
	Borde = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Borde"));
    RootComponent = Borde;
    //Borde->SetupAttachment(RootComponent);
	Borde->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	Borde->SetWorldScale3D(EscalaBotones);

    //static ConstructorHelpers::FObjectFinder<UStaticMesh> BordeAsset(TEXT("StaticMesh'/Game/Trasnformation/Assets/Meshes/Botones/BotonS_BordeBoton.BotonS_BordeBoton'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    static ConstructorHelpers::FObjectFinder<UStaticMesh> BordeAsset(TEXT("StaticMesh'/Game/Transformation/Assets/Meshes/Botones/BotonC_BordeCuadrado.BotonC_BordeCuadrado'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (BordeAsset.Succeeded()) {
        Borde->SetStaticMesh(BordeAsset.Object);
        //static ConstructorHelpers::FObjectFinder<UMaterial> BordeMaterialAsset(TEXT("Material'/Game/Trasnformation/Assets/Meshes/Botones/BordeMaterial.BordeMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
        static ConstructorHelpers::FObjectFinder<UMaterial> BordeMaterialAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBordeC.MaterialBordeC'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
        if (BordeMaterialAsset.Succeeded()) {
            Borde->SetMaterial(0, BordeMaterialAsset.Object);
        }
    }

	Boton = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Boton"));
    Boton->SetupAttachment(RootComponent);
	Boton->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	//Boton->SetWorldScale3D(EscalaBotones);
	Boton->SetWorldScale3D(FVector(1.0f));

    //static ConstructorHelpers::FObjectFinder<UStaticMesh> BotonAsset(TEXT("StaticMesh'/Game/Trasnformation/Assets/Meshes/Botones/BotonS_Boton.BotonS_Boton'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    static ConstructorHelpers::FObjectFinder<UStaticMesh> BotonAsset(TEXT("StaticMesh'/Game/Transformation/Assets/Meshes/Botones/BotonC_BotonCuadrado.BotonC_BotonCuadrado'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (BotonAsset.Succeeded()) {
        Boton->SetStaticMesh(BotonAsset.Object);
        //static ConstructorHelpers::FObjectFinder<UMaterial> BotonMaterialAsset(TEXT("Material'/Game/Trasnformation/Assets/Meshes/Botones/BotonMaterial_5.BotonMaterial_5'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
        static ConstructorHelpers::FObjectFinder<UMaterial> BotonMaterialAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC.MaterialBotonC'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
        if (BotonMaterialAsset.Succeeded()) {
            MaterialNormal = BotonMaterialAsset.Object;
            Boton->SetMaterial(0, BotonMaterialAsset.Object);
        }
    }

    static ConstructorHelpers::FObjectFinder<UMaterial> BotonMaterialPAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC.MaterialBotonC'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (BotonMaterialPAsset.Succeeded()) {
        MaterialPresionado = BotonMaterialPAsset.Object;
    }

    Boton->OnComponentBeginOverlap.AddDynamic(this, &ABotonPush::OnBeginOverlapBoton);
    Boton->OnComponentEndOverlap.AddDynamic(this, &ABotonPush::OnEndOverlapBoton);

    VelocidadNormal = 6.0f;
    AlturaContacto = -3.0f;
    AlturaFondo = -4.0f;
    AlturaNormal = 0.0f;//para on
}

// Called when the game starts or when spawned
void ABotonPush::BeginPlay() {
	Super::BeginPlay();
	
}

// Called every frame
void ABotonPush::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);


    FVector PosicionActual = Boton->GetRelativeTransform().GetLocation();
    if (bPressed) {//
        //necesito hacer release
        //aqui debo tener en cuanta si hago release co el control mientras presiono, o mantengo el contancto depsues de presionar
        if (PosicionActual.Z >= AlturaContacto) {//pressing significa en cantaco con el control, pero no necesraiamente movimiendose
            //hago release
            bPosicionNormal = false;
            Soltado();
            SendSignalReleased();
        }
    }
    else {//estoy en realeas
        if (PosicionActual.Z <= AlturaContacto) {//pressing significa en cantaco con el control, pero no necesraiamente movimiendose
            //hago pressed
            bPosicionNormal = false;
            Presionado();//bPressed = true;
            SendSignalPressed();
        }
    }

    //movimiento del boton
    if (bPressing) {//osea hay un elemento entrando en contacto con el boton
        bPosicionNormal = false;
        float h = GetTransform().InverseTransformPosition(ColisionControl->GetComponentLocation()).Z;
        //UE_LOG(LogClass, Log, TEXT("h: %f"), h);
        //UE_LOG(LogClass, Log, TEXT("h - DistanciaColision: %f"), h - DistanciaColision);
        float zPosicion = -4.0f;
        zPosicion = FMath::Clamp(h - DistanciaColision, -4.0f, AlturaNormal);
        //UE_LOG(LogClass, Log, TEXT("zPosicion: %f"), zPosicion);
        FVector NewBotonPosition = FVector(0.0f, 0.0f, zPosicion);
        //UE_LOG(LogClass, Log, TEXT("Tocando boton"));
        Boton->SetRelativeLocation(NewBotonPosition);
    }
    else {
        if(!bPosicionNormal) {//si no estoy en la posicion normal y no he sidopresionado , regreso a la posicion normal
            Boton->SetRelativeLocation(FMath::Lerp(Boton->GetRelativeTransform().GetLocation(), FVector(0.0f, 0.0f, AlturaNormal), DeltaTime * VelocidadNormal));
            if (Boton->GetRelativeTransform().GetLocation() == FVector(0.0f, 0.0f, AlturaNormal)) {//aqui hago el release
                bPosicionNormal = true;
            }
        }
    }
}

void ABotonPush::OnBeginOverlapBoton(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult) {
    if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && (OtherActor != GetOwner())) { //no es necesario el ultimo, solo para este caso particular en el que no quiero que el propio conejo active esta funconalidad
        AVRPawn * const VRPawn = Cast<AVRPawn>(OtherActor);
        if (VRPawn && !VRPawn->IsPendingKill()) {
            UCapsuleComponent * const ColisionController = Cast<UCapsuleComponent>(OtherComp);//para la casa no necesito verificar que haya tocado su staticmesh
            if (ColisionController) {
                ColisionControl = ColisionController;
                bPressing = true;
                DistanciaColision = (GetTransform().InverseTransformPosition(ColisionController->GetComponentLocation()) - Boton->GetRelativeTransform().GetLocation()).Z;
                    //ColisionController->GetComponentLocation() - Boton->GetComponentLocation()).Z;
                //UE_LOG(LogClass, Log, TEXT("Tocando boton, DC: %f"), DistanciaColision);
                /*if (ColisionController->GetName() == "ColisionControllerRight") {//podria ser util la diferencia
                }
                else if (ColisionController->GetName() == "ColisionControllerLeft") {
                }*/
            }
        }
    }
}

void ABotonPush::OnEndOverlapBoton(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex) {
    if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr) && (OtherActor != GetOwner())) { //no es necesario el ultimo, solo para este caso particular en el que no quiero que el propio conejo active esta funconalidad
        AVRPawn * const VRPawn = Cast<AVRPawn>(OtherActor);
        if (VRPawn && !VRPawn->IsPendingKill()) {
            UCapsuleComponent * const ColisionController = Cast<UCapsuleComponent>(OtherComp);//para la casa no necesito verificar que haya tocado su staticmesh
            if (ColisionController == ColisionControl) {
                ColisionControl = nullptr;
                bPressing = false;
                //UE_LOG(LogClass, Log, TEXT("Soltando boton"));
                /*if (ColisionController->GetName() == "ColisionControllerRight") {//podria ser util la diferencia
                }
                else if (ColisionController->GetName() == "ColisionControllerLeft") {
                }*/
            }
        }
    }
}

void ABotonPush::Presionado() {
    bPressed = true;
    Boton->SetMaterial(0, MaterialPresionado);
}

void ABotonPush::Soltado() {
    bPressed = false;
    Boton->SetMaterial(0, MaterialNormal);
}

void ABotonPush::NoPresionado() {
    bPressed = false;
}

