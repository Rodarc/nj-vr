// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Boton.h"
#include "BotonToggle.generated.h"

/**
 * 
 */
UCLASS()
class NJVR_API ABotonToggle : public ABoton
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABotonToggle();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    bool bOn;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    bool bPressed;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    bool bReleased;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    bool bPressing;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    bool bPosicionNormal;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    bool bPosicionPresionado;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    bool bPosicionContacto;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    bool bPosicionFondo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    float AlturaNormal;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    float AlturaPresionado;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    float AlturaContacto;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    float AlturaFondo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Transformation")
    float VelocidadNormal;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Transformation")
    float DistanciaColision;
	
    UFUNCTION()
    void OnBeginOverlapBoton(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

    UFUNCTION()
	void OnEndOverlapBoton(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex);


    virtual void Presionado() override;

    virtual void Soltado() override;

    virtual void NoPresionado() override;
};
