// Fill out your copyright notice in the Description page of Project Settings.


#include "ContentNodoActor.h"
#include "UObject/ConstructorHelpers.h"
#include "Particles/ParticleSystemComponent.h"
#include "../Pawns/VRPawn.h"
#include "../Visualizations/VRVisualization.h"
#include "../NJ/Nodo.h"


// Sets default values
AContentNodoActor::AContentNodoActor() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));//para poder aplicar la escala


    static ConstructorHelpers::FClassFinder<UUserWidget> ContentNodoClass(TEXT("WidgetBlueprintGeneratedClass'/Game/NJVR/Blueprints/Interfaz/ContentNodoBP.ContentNodoBP_C'"));
    if (ContentNodoClass.Succeeded()) {
        TypeContentNodo = ContentNodoClass.Class;
    }
    WidgetComponentContentNodo = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget Component Content Nodo"));
    WidgetComponentContentNodo->SetupAttachment(RootComponent);
    WidgetComponentContentNodo->SetWidgetSpace(EWidgetSpace::World);
    WidgetComponentContentNodo->SetDrawSize(FVector2D(700.0f, 400.0f));
    WidgetComponentContentNodo->SetPivot(FVector2D(0.5f, 0.5f));
    WidgetComponentContentNodo->SetWorldScale3D(FVector(0.05f, 0.05f, 0.05f));
    WidgetComponentContentNodo->SetRelativeLocation(FVector(0.0f, 0.0f, 10.0f));
    WidgetComponentContentNodo->SetDrawAtDesiredSize(true);
    WidgetComponentContentNodo->SetVisibility(true);
    if (ContentNodoClass.Succeeded()) {
        WidgetComponentContentNodo->SetWidgetClass(ContentNodoClass.Class);
    }

    LineaContenido = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("LineaContenido"));
    LineaContenido->SetupAttachment(RootComponent);
    //static ConstructorHelpers::FObjectFinder<UParticleSystem> EfectoResaltadoAsset(TEXT("ParticleSystem'/Game/Visualization/ParticleSystems/LaserImpact/LaserImpactRotacion.LaserImpactRotacion'"));
    static ConstructorHelpers::FObjectFinder<UParticleSystem> LineaContenidoAsset(TEXT("ParticleSystem'/Game/Visualization/ParticleSystems/LaserBeam/LineaNegro.LineaNegro'"));
    //static ConstructorHelpers::FObjectFinder<UParticleSystem> EfectoResaltadoAsset(TEXT("ParticleSystem'/Game/Visualization/ParticleSystems/LaserImpact/NodoVImpactRotacion.NodoVImpactRotacion'"));
    if (LineaContenidoAsset.Succeeded()) {
        LineaContenido->SetTemplate(LineaContenidoAsset.Object);
    }
    LineaContenido->SetRelativeLocation(FVector::ZeroVector);
    LineaContenido->bAutoActivate = true;

}

// Called when the game starts or when spawned
void AContentNodoActor::BeginPlay() {
	Super::BeginPlay();
	
}

// Called every frame
void AContentNodoActor::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
    //necesito la referencia al usaurio o a su camara
    if (Usuario) {
        FRotator NewRotation = FRotationMatrix::MakeFromX(Usuario->VRCamera->GetComponentLocation() - WidgetComponentContentNodo->GetComponentLocation()).Rotator();
        WidgetComponentContentNodo->SetWorldRotation(NewRotation);
        //AVRVisualization * vis = Cast<AVRVisualization>(GetOwner());
        FVector Origen = GetOwner()->GetTransform().TransformPosition(Node->GetLocation());
        //FVector Destino = GetActorLocation();
        LineaContenido->SetWorldLocation(Origen);
        if (WidgetComponentContentNodo->GetUserWidgetObject()->GetDesiredSize() != FVector2D::ZeroVector) {
            FVector Inter = GetPointInBorder();
            Inter = GetOwner()->GetTransform().TransformPosition(Inter);
            LineaContenido->SetBeamTargetPoint(0, Inter, 0);//o target
        }
        else {
            LineaContenido->SetBeamTargetPoint(0, WidgetComponentContentNodo->GetComponentLocation(), 0);//o target
        }
    }
}

void AContentNodoActor::Actualizar() {
    //a mi widget le digo que se actualice, si es que hay algo
}

void AContentNodoActor::SetContent(FString NewContent) {
    if (WidgetComponentContentNodo) {
        UContentNodo * WidgetContent = Cast<UContentNodo>(WidgetComponentContentNodo->GetUserWidgetObject());
        if (WidgetContent) {
            WidgetContent->Content = NewContent;
            WidgetContent->ActualizarContent();
            //Label->NameNodo = "hola cambiando el nombre del nodo.";
        }
    }
}

FVector AContentNodoActor::GetPointInBorder() {
    //proyeccion del punto del nodo al plano de esta cosa, para ello primero necesito los vectores directores del coponente
    FVector RightVector = WidgetComponentContentNodo->GetRightVector();
    RightVector = GetOwner()->GetTransform().InverseTransformVector(RightVector).GetSafeNormal();
    FVector UpVector = WidgetComponentContentNodo->GetUpVector();
    UpVector = GetOwner()->GetTransform().InverseTransformVector(UpVector).GetSafeNormal();
    FVector ForwardVector = WidgetComponentContentNodo->GetForwardVector();
    ForwardVector = GetOwner()->GetTransform().InverseTransformVector(ForwardVector).GetSafeNormal();

    FVector2D WidgetSize = WidgetComponentContentNodo->GetUserWidgetObject()->GetDesiredSize() * WidgetComponentContentNodo->GetComponentScale().X;// WidgetComponentContentNodo->GetRelativeTransform().GetScale3D().X;
    FVector Esquina = (RightVector * WidgetSize.X/2 + UpVector * WidgetSize.Y/2).GetSafeNormal();//1.58
    //FVector Esquina = (RightVector * WidgetSize.X/2 + UpVector * -1*WidgetSize.Y/2).GetSafeNormal();//0.4187
    //FVector Esquina = (RightVector * -1*WidgetSize.X/2 + UpVector * WidgetSize.Y/2).GetSafeNormal();//0.4187
    //FVector Esquina = (RightVector * -1*WidgetSize.X/2 + UpVector * -1*WidgetSize.Y/2).GetSafeNormal();//0.4187

    UE_LOG(LogClass, Log, TEXT("WidgetSize = %f, %f"), WidgetSize.X, WidgetSize.Y);

    float theta = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(RightVector, Esquina)));
    //UE_LOG(LogClass, Log, TEXT("theta = %f"), theta);
    float sign = (ForwardVector + FVector::CrossProduct(RightVector, Esquina)).Size();//si la suma vale 0 entonces son opuestos, si vale el doble de 1, entonces estan en la msma direccion
    //UE_LOG(LogClass, Log, TEXT("sign theta = %f"), sign);

    if (sign < 1.0f) {
        theta = 360.0f - theta;
    }

    float alpha = 2 * theta;
    float beta = 180 - alpha;


    //hallar la proyeccion del nodo en este plano

    FVector NodeLocation = Node->GetLocation();
    //FVector ContentLocation = GetActorLocation();//deberia ser la relativa
    FVector ContentLocation = RootComponent->GetRelativeTransform().GetLocation();//deberia ser la relativa
    //la normal del plano es el forward vector
    //el centro siempre esta en 0
    float D = -(ForwardVector.X*ContentLocation.X + ForwardVector.Y*ContentLocation.Y + ForwardVector.Z*ContentLocation.Z);
    float t = (-D - NodeLocation.X*ForwardVector.X - NodeLocation.Y*ForwardVector.Y - NodeLocation.Z*ForwardVector.Z) / (ForwardVector.X*ForwardVector.X + ForwardVector.Y*ForwardVector.Y + ForwardVector.Z*ForwardVector.Z);//coo 0 vale 0,, ya no existe lo que acompa�a a la D
    FVector PN = NodeLocation + t * ForwardVector;//en el 0,0,0, por eso no pongo punto de paso;
    
    //hallar  phi para el nodo proyectado
    float phi = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(RightVector, (PN - ContentLocation).GetSafeNormal())));
    sign = (ForwardVector + FVector::CrossProduct(RightVector, (PN - ContentLocation).GetSafeNormal())).Size();//si la suma vale 0 entonces son opuestos, si vale el doble de 1, entonces estan en la msma direccion

    if (sign < 1.0f) {
        phi = 360.0f - phi;
    }
    //UE_LOG(LogClass, Log, TEXT("phi= %f"), phi);//no esta del todo preciso// serla el content locatio?

    FVector NDireccion = (ContentLocation - PN).GetSafeNormal();

    FVector LOrigen;
    FVector LDestino;
    FVector LDireccion;

    if (phi - alpha/2 <= 0.0f) {
        //L0
        LOrigen = (RightVector * WidgetSize.X/2 + UpVector * WidgetSize.Y/2) + ContentLocation;//punto de paso
        LDestino = (RightVector * WidgetSize.X/2 + UpVector * -1*WidgetSize.Y/2) + ContentLocation;//para ayudar a alcular el vector
        LDireccion = (LDestino - LOrigen).GetSafeNormal();
    }
    else if (phi - alpha/2 - beta <= 0.0f) {
        //L1
        LOrigen = (RightVector * -1*WidgetSize.X/2 + UpVector * WidgetSize.Y/2) + ContentLocation;//para ayudar a alcular el vector
        LDestino = (RightVector * WidgetSize.X/2 + UpVector * WidgetSize.Y/2) + ContentLocation;//punto de paso
        LDireccion = (LDestino - LOrigen).GetSafeNormal();
    }
    else if (phi - alpha/2 - beta - alpha <= 0.0f) {
        //L2
        LOrigen = (RightVector * -1*WidgetSize.X/2 + UpVector * -1*WidgetSize.Y/2) + ContentLocation;//para ayudar a alcular el vector
        LDestino = (RightVector * -1*WidgetSize.X/2 + UpVector * WidgetSize.Y/2) + ContentLocation;//para ayudar a alcular el vector
        LDireccion = (LDestino - LOrigen).GetSafeNormal();
    }
    else if (phi - alpha/2 - beta - alpha - beta <= 0.0f) {
        //L3
        LOrigen = (RightVector * WidgetSize.X/2 + UpVector * -1*WidgetSize.Y/2) + ContentLocation;//para ayudar a alcular el vector
        LDestino = (RightVector * -1*WidgetSize.X/2 + UpVector * -1*WidgetSize.Y/2) + ContentLocation;//para ayudar a alcular el vector
        LDireccion = (LDestino - LOrigen).GetSafeNormal();
    }
    else {
        //L0
        LOrigen = (RightVector * WidgetSize.X/2 + UpVector * WidgetSize.Y/2) + ContentLocation;//punto de paso
        LDestino = (RightVector * WidgetSize.X/2 + UpVector * -1*WidgetSize.Y/2) + ContentLocation;//para ayudar a alcular el vector
        LDireccion = (LDestino - LOrigen).GetSafeNormal();
    }

    //hallando la interseccion de las rectas
    float tInterseccion = (PN.Y - LOrigen.Y + (LOrigen.X - PN.X)*NDireccion.Y / NDireccion.X) / (LDireccion.Y - LDireccion.X*NDireccion.Y / NDireccion.X);//deberia reescalar, igual que wl widget
    //tInterseccion *= 3.33f;

    FVector PuntoInterseccion = LOrigen + tInterseccion * LDireccion;

    UE_LOG(LogClass, Log, TEXT("distancia = %f"), (PuntoInterseccion - ContentLocation).Size());//no esta del todo preciso// serla el content locatio?

    return PuntoInterseccion;
}

