// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/WidgetComponent.h"
#include "../Interfaz/ContentNodo.h"
#include "ContentNodoActor.generated.h"

UCLASS()
class NJVR_API AContentNodoActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AContentNodoActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * WidgetComponentContentNodo;//este es el nodo label que siempre tengo activo, si en algun momento fijo el label de un nodo, pues creo uno uevo copio la inof y se guarda en un arrya, y este se oculta temporalmente hasta que le apunte a otro nodo

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    TSubclassOf<UContentNodo> TypeContentNodo;

    class AVRPawn * Usuario;

    class Nodo * Node;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UParticleSystemComponent * LineaContenido;

    void Actualizar();

    void SetContent(FString NewContent);

    FVector GetPointInBorder();//depende del punto que quiero proyectar, es decir del nodo,
};
