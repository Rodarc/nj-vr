// Fill out your copyright notice in the Description page of Project Settings.


#include "PanelBotones.h"
#include "UObject/ConstructorHelpers.h"
#include "BotonToggle.h"
#include "BotonPush.h"
#include "../Visualizations/VRVisualization.h"
#include "../Interfaz/ContentMenu.h"

// Sets default values
APanelBotones::APanelBotones()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Centro"));

    static ConstructorHelpers::FClassFinder<UUserWidget> MenuContenidoClass(TEXT("WidgetBlueprintGeneratedClass'/Game/Visualization/Blueprints/Menu/ContentMenuVR.ContentMenuVR_C'"));
    MenuContenido = CreateDefaultSubobject<UWidgetComponent>(TEXT("MenuContenido"));
    MenuContenido->SetWidgetSpace(EWidgetSpace::World);
    MenuContenido->SetupAttachment(RootComponent);
    MenuContenido->SetDrawSize(FVector2D(1200.0f, 400.0f));//deberia dibujarse de acuerdo al deseado//deberia dibujarse segun el tama�o deseado
    MenuContenido->SetPivot(FVector2D(0.0f, 1.0f));
    MenuContenido->SetRelativeLocation(FVector(6.0f, 0.0f, 5.0f));// a cierta distancia
    MenuContenido->SetRelativeRotation(FRotator(50.0f, 180.0f, 0.0f));
    MenuContenido->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
    MenuContenido->SetDrawAtDesiredSize(true);
    if (MenuContenidoClass.Succeeded()) {
        MenuContenido->SetWidgetClass(MenuContenidoClass.Class);
    }

    MenuLabel = CreateDefaultSubobject<UWidgetComponent>(TEXT("MenuLabel"));
    MenuLabel->SetWidgetSpace(EWidgetSpace::World);
    MenuLabel->SetupAttachment(RootComponent);
    MenuLabel->SetDrawSize(FVector2D(1200.0f, 400.0f));//deberia dibujarse de acuerdo al deseado//deberia dibujarse segun el tama�o deseado
    MenuLabel->SetPivot(FVector2D(1.0f, 1.0f));
    MenuLabel->SetRelativeLocation(FVector(6.0f, -25.0f, 5.0f));// a cierta distancia
    MenuLabel->SetRelativeRotation(FRotator(50.0f, 180.0f, 0.0f));
    MenuLabel->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
    MenuLabel->SetDrawAtDesiredSize(true);
    if (MenuContenidoClass.Succeeded()) {
        MenuLabel->SetWidgetClass(MenuContenidoClass.Class);
    }

    static ConstructorHelpers::FClassFinder<UUserWidget> DataSetsClass(TEXT("WidgetBlueprintGeneratedClass'/Game/Visualization/Blueprints/Menu/ControlMenuLoadVR.ControlMenuLoadVR_C'"));
    DataSets = CreateDefaultSubobject<UWidgetComponent>(TEXT("DataSets"));
    DataSets->SetWidgetSpace(EWidgetSpace::World);
    DataSets->SetupAttachment(RootComponent);
    DataSets->SetDrawSize(FVector2D(350.0f, 35.0f));
    DataSets->SetPivot(FVector2D(0.5f, 1.0f));
    DataSets->SetRelativeLocation(FVector(4.0f, 50.0f, 4.0f));
    DataSets->SetRelativeRotation(FRotator(50.0f, 180.0f, 0.0f));
    DataSets->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
    if (DataSetsClass.Succeeded()) {
        DataSets->SetWidgetClass(DataSetsClass.Class);
    }
    DataSets->SetDrawAtDesiredSize(true);
    DataSets->SetVisibility(false);

    static ConstructorHelpers::FClassFinder<UUserWidget> MenuColorClass(TEXT("WidgetBlueprintGeneratedClass'/Game/Visualization/Blueprints/Menu/ControlMenuColorVR.ControlMenuColorVR_C'"));
    MenuColores= CreateDefaultSubobject<UWidgetComponent>(TEXT("MenuColor"));
    MenuColores->SetWidgetSpace(EWidgetSpace::World);
    MenuColores->SetupAttachment(RootComponent);
    MenuColores->SetRelativeLocation(FVector(4.0f, 25.0f, 4.0f));
    MenuColores->SetRelativeRotation(FRotator(50.0f, 180.0f, 0.0f));
    MenuColores->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
    if (MenuColorClass.Succeeded()) {
        MenuColores->SetWidgetClass(MenuColorClass.Class);
    }
    MenuColores->SetDrawSize(FVector2D(250.0f, 125.0f));
    MenuColores->SetPivot(FVector2D(0.5f, 1.0f));
    MenuColores->SetVisibility(false);


	static ConstructorHelpers::FObjectFinder<UMaterial> BotonClearMaterialAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_ClearN.MaterialBotonC_ClearN'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
	if (BotonClearMaterialAsset.Succeeded()) {
		MaterialClearNormal = BotonClearMaterialAsset.Object;
	}
    static ConstructorHelpers::FObjectFinder<UMaterial> BotonClearMaterialPAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_Clear.MaterialBotonC_Clear'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (BotonClearMaterialPAsset.Succeeded()) {
        MaterialClearPresionado = BotonClearMaterialPAsset.Object;
    }

	static ConstructorHelpers::FObjectFinder<UMaterial> BotonColorMaterialAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_ColorN.MaterialBotonC_ColorN'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
	if (BotonColorMaterialAsset.Succeeded()) {
		MaterialColorNormal = BotonColorMaterialAsset.Object;
	}
    static ConstructorHelpers::FObjectFinder<UMaterial> BotonColorMaterialPAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_Color.MaterialBotonC_Color'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (BotonColorMaterialPAsset.Succeeded()) {
        MaterialColorPresionado = BotonColorMaterialPAsset.Object;
    }

	static ConstructorHelpers::FObjectFinder<UMaterial> BotonContentMaterialAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_ContentN.MaterialBotonC_ContentN'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
	if (BotonContentMaterialAsset.Succeeded()) {
		MaterialContentNormal = BotonContentMaterialAsset.Object;
	}
    static ConstructorHelpers::FObjectFinder<UMaterial> BotonContentMaterialPAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_Content.MaterialBotonC_Content'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (BotonContentMaterialPAsset.Succeeded()) {
        MaterialContentPresionado = BotonContentMaterialPAsset.Object;
    }

	static ConstructorHelpers::FObjectFinder<UMaterial> BotonDatasetMaterialAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_DatasetN.MaterialBotonC_DatasetN'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
	if (BotonDatasetMaterialAsset.Succeeded()) {
		MaterialDatasetNormal = BotonDatasetMaterialAsset.Object;
	}
    static ConstructorHelpers::FObjectFinder<UMaterial> BotonDatasetMaterialPAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_Dataset.MaterialBotonC_Dataset'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (BotonDatasetMaterialPAsset.Succeeded()) {
        MaterialDatasetPresionado = BotonDatasetMaterialPAsset.Object;
    }

	static ConstructorHelpers::FObjectFinder<UMaterial> BotonLabelMaterialAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_LabelN.MaterialBotonC_LabelN'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
	if (BotonLabelMaterialAsset.Succeeded()) {
		MaterialLabelNormal = BotonLabelMaterialAsset.Object;
	}
    static ConstructorHelpers::FObjectFinder<UMaterial> BotonLabelMaterialPAsset(TEXT("Material'/Game/Transformation/Assets/Meshes/Botones/MaterialBotonC_Label.MaterialBotonC_Label'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (BotonLabelMaterialPAsset.Succeeded()) {
        MaterialLabelPresionado = BotonLabelMaterialPAsset.Object;
    }
}

// Called when the game starts or when spawned
void APanelBotones::BeginPlay() {
	Super::BeginPlay();
	UContentMenu * ContentMenu = Cast<UContentMenu>(MenuContenido->GetUserWidgetObject());
	if (ContentMenu) {
		ContentMenu->PanelBotones = this;
	}
	ContentMenu = Cast<UContentMenu>(MenuLabel->GetUserWidgetObject());
	if (ContentMenu) {
		ContentMenu->PanelBotones = this;
	}
    UWorld * const World = GetWorld();
    if (World) {
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = GetInstigator();

        BotonLimpiar = World->SpawnActor<ABoton>(ABotonPush::StaticClass(), FVector(0.0f, -50.0f, 0.0f), FRotator::ZeroRotator, SpawnParams);
        BotonLimpiar->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
        BotonLimpiar->TareaBoton = EButtonTask::ECleanButton;
		BotonLimpiar->MaterialNormal = MaterialClearNormal;
		BotonLimpiar->MaterialPresionado = MaterialClearPresionado;
		BotonLimpiar->UpdateMaterial();

        BotonLabel = World->SpawnActor<ABoton>(ABotonPush::StaticClass(), FVector(0.0f, -25.0f, 0.0f), FRotator::ZeroRotator, SpawnParams);
        BotonLabel->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
        BotonLabel->TareaBoton = EButtonTask::ELabelButton;
		BotonLabel->MaterialNormal = MaterialLabelNormal;
		BotonLabel->MaterialPresionado = MaterialLabelPresionado;
		BotonLabel->UpdateMaterial();

        BotonContenido = World->SpawnActor<ABoton>(ABotonPush::StaticClass(), FVector(0.0f, 0.0f, 0.0f), FRotator::ZeroRotator, SpawnParams);
        BotonContenido->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
        BotonContenido->TareaBoton = EButtonTask::EContenidoButton;
		BotonContenido->MaterialNormal = MaterialContentNormal;
		BotonContenido->MaterialPresionado = MaterialContentPresionado;
		BotonContenido->UpdateMaterial();

        BotonColorear = World->SpawnActor<ABoton>(ABotonToggle::StaticClass(), FVector(0.0f, 25.0f, 0.0f), FRotator::ZeroRotator, SpawnParams);
        BotonColorear->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
        BotonColorear->TareaBoton = EButtonTask::EColorearButton;
		BotonColorear->MaterialNormal = MaterialColorNormal;
		BotonColorear->MaterialPresionado = MaterialColorPresionado;
		BotonColorear->UpdateMaterial();

        BotonDataset = World->SpawnActor<ABoton>(ABotonToggle::StaticClass(), FVector(0.0f, 50.0f, 0.0f), FRotator::ZeroRotator, SpawnParams);
        BotonDataset->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
        BotonDataset->TareaBoton = EButtonTask::EDataButton;
		BotonDataset->MaterialNormal = MaterialDatasetNormal;
		BotonDataset->MaterialPresionado = MaterialDatasetPresionado;
		BotonDataset->UpdateMaterial();

        //BotonOpciones = World->SpawnActor<ABoton>(ABotonToggle::StaticClass(), FVector(0.0f, 50.0f, 0.0f), FRotator::ZeroRotator, SpawnParams);
        //BotonOpciones->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
        //BotonOpciones->TareaBoton = EButtonTask::EOpcionesButton;
    }
}

// Called every frame
void APanelBotones::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APanelBotones::Press(EButtonTask BotonPresionado) {
    AVRVisualization * Visualization = Cast<AVRVisualization>(GetOwner());
    if (Visualization) {
        switch (BotonPresionado) {
            case EButtonTask::ECleanButton: {
				Visualization->DeseleccionarTodo();
				Visualization->UpdateNodosMesh();
            }
            break;
            case EButtonTask::ELabelButton: {
                Visualization->CreateLabelTemasForSelection(5);
                MenuLabel->SetVisibility(true);
            }
            break;
            case EButtonTask::EContenidoButton: {
                Visualization->CreateContentsForSelection();
                MenuContenido->SetVisibility(true);
            }
            break;
            case EButtonTask::EColorearButton: {
            }
            break;
            case EButtonTask::EDataButton: {
            }
            break;
            case EButtonTask::EOpcionesButton: {
            }
            break;
            default:
            case EButtonTask::ESeleccionarButton: {
            }
            break;
        }
    }
}

void APanelBotones::Release(EButtonTask BotonPresionado) {
    AVRVisualization * Visualization = Cast<AVRVisualization>(GetOwner());
    if (Visualization) {
        switch (BotonPresionado) {
            case EButtonTask::ECleanButton: {
            }
            break;
            case EButtonTask::ELabelButton: {
            }
            break;
            case EButtonTask::EContenidoButton: {
            }
            break;
            case EButtonTask::EColorearButton: {
            }
            break;
            case EButtonTask::EDataButton: {
            }
            break;
            case EButtonTask::EOpcionesButton: {
            }
            break;
            default:
            case EButtonTask::ESeleccionarButton: {
            }
            break;
        }
    }
}

void APanelBotones::Activate(EButtonTask BotonPresionado) {
    AVRVisualization * Visualization = Cast<AVRVisualization>(GetOwner());
    if (Visualization) {
        switch (BotonPresionado) {
            case EButtonTask::ECleanButton: {
            }
            break;
            case EButtonTask::ELabelButton: {
            }
            break;
            case EButtonTask::EContenidoButton: {
            }
            break;
            case EButtonTask::EColorearButton: {
                Visualization->SetVisualizationTask(EVRVisualizationTask::EColorTask);
                MenuColores->SetVisibility(true);
            }
            break;
            case EButtonTask::EDataButton: {
                DataSets->SetVisibility(true);
            }
            break;
            case EButtonTask::EOpcionesButton: {
            }
            break;
            default:
            case EButtonTask::ESeleccionarButton: {
            }
            break;
        }
    }
}

void APanelBotones::Deactivate(EButtonTask BotonPresionado) {
    AVRVisualization * Visualization = Cast<AVRVisualization>(GetOwner());
    if (Visualization) {
        switch (BotonPresionado) {
            case EButtonTask::ECleanButton: {
            }
            break;
            case EButtonTask::ELabelButton: {
            }
            break;
            case EButtonTask::EContenidoButton: {
            }
            break;
            case EButtonTask::EColorearButton: {
                Visualization->SetVisualizationTask(EVRVisualizationTask::ESelectionTask);
                MenuColores->SetVisibility(false);
            }
            break;
            case EButtonTask::EDataButton: {
                DataSets->SetVisibility(false);
            }
            break;
            case EButtonTask::EOpcionesButton: {
            }
            break;
            default:
            case EButtonTask::ESeleccionarButton: {
            }
            break;
        }
    }
}

void APanelBotones::EliminarLabel(FLinearColor ColorLabel) {
    AVRVisualization * Visualization = Cast<AVRVisualization>(GetOwner());
	if (Visualization) {
		int IdColor;
		Visualization->PaletaColores.Find(ColorLabel, IdColor);
		Visualization->DesresaltarColor(IdColor);
		Visualization->UpdateNodosMesh();
	}
}

