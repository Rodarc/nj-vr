// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Boton.h"
#include "Components/WidgetComponent.h"
#include "PanelBotones.generated.h"

UCLASS()
class NJVR_API APanelBotones : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APanelBotones();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "PanelBotones")
    ABoton * BotonLabel;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "PanelBotones")
    ABoton * BotonColorear;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "PanelBotones")
    ABoton * BotonContenido;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "PanelBotones")
    ABoton * BotonDataset;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "PanelBotones")
    ABoton * BotonLimpiar;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "PanelBotones")
    ABoton * BotonOpciones;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialLabelNormal;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialLabelPresionado;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialContentNormal;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialContentPresionado;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialColorNormal;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialColorPresionado;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialDatasetNormal;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialDatasetPresionado;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialClearNormal;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Botones")
	UMaterial * MaterialClearPresionado;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * MenuContenido;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * MenuLabel;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * DataSets;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * MenuColores;


    UFUNCTION(BlueprintCallable, Category = "PanelBotones")
    void Press(EButtonTask BotonPresionado);

    UFUNCTION(BlueprintCallable, Category = "PanelBotones")
    void Release(EButtonTask BotonPresionado);

    UFUNCTION(BlueprintCallable, Category = "PanelBotones")
    void Activate(EButtonTask BotonPresionado);

    UFUNCTION(BlueprintCallable, Category = "PanelBotones")
    void Deactivate(EButtonTask BotonPresionado);

	UFUNCTION(BlueprintCallable, Category = "PanelBotones")
	void EliminarLabel(FLinearColor ColorLabel);

};
