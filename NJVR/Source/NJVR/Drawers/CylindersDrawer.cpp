// Fill out your copyright notice in the Description page of Project Settings.


#include "CylindersDrawer.h"
#include "Materials/Material.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
ACylindersDrawer::ACylindersDrawer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    //necesito establecer un material por defecto
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("CentroCylinder"));

    static ConstructorHelpers::FObjectFinder<UMaterial> DefaultMaterialAsset(TEXT("Material'/Game/Visualization/Materials/AristasMeshMaterial.AristasMeshMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (DefaultMaterialAsset.Succeeded()) {
        DefaultMeshesMaterial = DefaultMaterialAsset.Object;
    }

    PrecisionCylinders = 8;
    //UmbralSizeElementsProceduralMesh = 1000;//el umbral para mi laptop sera de 1000 nodos
}

// Called when the game starts or when spawned
void ACylindersDrawer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACylindersDrawer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACylindersDrawer::Update() {
    for (int i = 0; i < NeedRecreateSeccion.Num(); i++) {//deberia tener un array de booleanos, para ver cual actualizo y cual no
        for (int j = 0; j < NeedRecreateSeccion[i].Num(); j++) {
            if (NeedRecreateSeccion[i][j]) {
                Meshes[i]->ClearMeshSection(j);
                Meshes[i]->CreateMeshSection_LinearColor(j, SeccionVertices[i][j], SeccionTriangles[i][j], SeccionNormals[i][j], SeccionUV0[i][j], SeccionVertexColors[i][j], SeccionTangents[i][j], false);
                NeedRecreateSeccion[i][j] = false;
                NeedUpdateSeccion[i][j] = false;
            }
            if (NeedUpdateSeccion[i][j]) {
                Meshes[i]->UpdateMeshSection_LinearColor(j, SeccionVertices[i][j], SeccionNormals[i][j], SeccionUV0[i][j], SeccionVertexColors[i][j], SeccionTangents[i][j]);
                //despues de hacer update, como ya actualice debo ponerlo en falso;
                NeedUpdateSeccion[i][j] = false;
            }
        }
    }
}

int ACylindersDrawer::CreateMesh() {
    int NewId = Meshes.Num();
    FString Name = "GeneratedCylinderMesh";
    Name = Name + FString::FromInt(NewId);
    //UProceduralMeshComponent * SphereMesh = NewObject<UProceduralMeshComponent>(FName(*Name));
    UProceduralMeshComponent * SphereMesh = NewObject<UProceduralMeshComponent>(this, FName(*Name));
    if (SphereMesh) {
        SphereMesh->RegisterComponent();
        SphereMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
        SphereMesh->bUseAsyncCooking = true;
        //SphereMesh->bRenderCustomDepth = true; //estas dos lineas son para los que seran resaltado, proveer otras funciones que modifiquen esto, esto es a nivel de capas, o meshes
        //SphereMesh->SetCustomDepthStencilValue(255 - (8-i));
        SphereMesh->ContainsPhysicsTriMeshData(false);

        Meshes.Add(SphereMesh);
        MeshesMaterials.AddDefaulted();

        //el primer nivel es a que mesh pertenece
		NeedUpdateSeccion.AddDefaulted();
        NeedRecreateSeccion.AddDefaulted();
		SeccionVertices.AddDefaulted();
		SeccionVerticesP.AddDefaulted();
		SeccionTriangles.AddDefaulted();
		SeccionNormals.AddDefaulted();
		SeccionUV0.AddDefaulted();
		SeccionTangents.AddDefaulted();
		SeccionVertexColors.AddDefaulted();

        return NewId;
        //el mesh ha sido creado, pero no tiene ninguna seccion, deberia crear sus contenedores, es decir hacer creacer el primer nivel de los arrays
    }
    return -1;
}

int ACylindersDrawer::CreateSeccionInMesh(int IdMesh) {
    if (IdMesh < Meshes.Num()) {//se asigna un material por defecto basico
		//int IdNewSeccion = SeccionNodosResaltados.AddDefaulted();//guarda los nodos que pertenencen a cada seccion
        //esta clase aun conservara array de ndoso para saber a que capa o rama pertencen, pero todo el manejo de secciones estara manejado en el actor
		int IdNewSeccion = NeedUpdateSeccion[IdMesh].Add(false);
		NeedRecreateSeccion[IdMesh].Add(false);
		SeccionVertices[IdMesh].AddDefaulted();
		SeccionVerticesP[IdMesh].AddDefaulted();
		SeccionTriangles[IdMesh].AddDefaulted();
		SeccionNormals[IdMesh].AddDefaulted();
		SeccionUV0[IdMesh].AddDefaulted();
		SeccionTangents[IdMesh].AddDefaulted();
		SeccionVertexColors[IdMesh].AddDefaulted();
		UE_LOG(LogClass, Log, TEXT("Creando seccion [%d] en mesh [%d]"), IdNewSeccion, IdMesh);
		Meshes[IdMesh]->CreateMeshSection_LinearColor(IdNewSeccion, SeccionVertices[IdMesh][IdNewSeccion], SeccionTriangles[IdMesh][IdNewSeccion], SeccionNormals[IdMesh][IdNewSeccion], SeccionUV0[IdMesh][IdNewSeccion], SeccionVertexColors[IdMesh][IdNewSeccion], SeccionTangents[IdMesh][IdNewSeccion], false);
		if (DefaultMeshesMaterial) {
			Meshes[IdMesh]->SetMaterial(IdNewSeccion, DefaultMeshesMaterial);
            MeshesMaterials[IdMesh].Add(DefaultMeshesMaterial);

		}
        return IdNewSeccion;
    }
    return -1;
}

void ACylindersDrawer::SetMaterialSeccionMesh(int IdMesh, int IdSeccion, UMaterial * NewMaterial) {
    if (NewMaterial) {
        Meshes[IdMesh]->SetMaterial(IdSeccion, NewMaterial);
    }
}

void ACylindersDrawer::DeleteMesh(int IdMesh) {
}

int ACylindersDrawer::DeleteSeccionInMesh(int IdMesh, int IdSeccion) {
    return 0;
}

FDrawElementReferenceStruct ACylindersDrawer::AddCylinderToMeshSeccion(int IdMesh, int IdSeccion, int IdCylinder, FVector Source, FVector Target, float Radio, FLinearColor Color) {
    //precision debe ser mayor a 3, represante el numero de lados del cilindro
    //la precision deberia ser de forma general,asi lo puedo usar para determinar la cantidad de vertices a�adidos
    FVector Direccion = (Target - Source).GetSafeNormal();
    //calculando vertices superioes

    float phi = PI/2;
    float theta = FMath::Asin(Direccion.Y / FMath::Sin(phi)) + PI/2;
    FVector VectorU;
    VectorU.X = 1.0f * FMath::Sin(phi) * FMath::Cos(theta);
    VectorU.Y = 1.0f * FMath::Sin(phi) * FMath::Sin(theta);
    VectorU.Z = 1.0f * FMath::Cos(phi);
    FVector VectorV = FVector::CrossProduct(Direccion, VectorU).GetSafeNormal();
    float DeltaTheta = 2 * PI / PrecisionCylinders;

    for (int i = 0; i < PrecisionCylinders; i++) {
        SeccionVertices[IdMesh][IdSeccion].Add(Target + (VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta))*Radio);
        SeccionNormals[IdMesh][IdSeccion].Add((VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta)).GetSafeNormal());
        SeccionTangents[IdMesh][IdSeccion].Add(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        SeccionUV0[IdMesh][IdSeccion].Add(FVector2D(i*(1.0f/PrecisionCylinders), 1));
        SeccionVertexColors[IdMesh][IdSeccion].Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
    }
    for (int i = 0; i < PrecisionCylinders; i++) {
        SeccionVertices[IdMesh][IdSeccion].Add(Source + (VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta))*Radio);
        SeccionNormals[IdMesh][IdSeccion].Add((VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta)).GetSafeNormal());
        SeccionTangents[IdMesh][IdSeccion].Add(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        SeccionUV0[IdMesh][IdSeccion].Add(FVector2D(i*(1.0f/PrecisionCylinders), 0));
        //VertexColorsAristas.Add(FLinearColor(0.0, 0.75, 0.75, 1.0));
        SeccionVertexColors[IdMesh][IdSeccion].Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
    }

    for (int i = 0; i < PrecisionCylinders; i++) {
        SeccionTriangles[IdMesh][IdSeccion].Add((i+1)%PrecisionCylinders + IdCylinder * PrecisionCylinders*2);
        SeccionTriangles[IdMesh][IdSeccion].Add(PrecisionCylinders + (i + 1)%PrecisionCylinders + IdCylinder * PrecisionCylinders*2);
        SeccionTriangles[IdMesh][IdSeccion].Add(i + IdCylinder * PrecisionCylinders*2);

        SeccionTriangles[IdMesh][IdSeccion].Add(PrecisionCylinders + i + IdCylinder * PrecisionCylinders*2);
        SeccionTriangles[IdMesh][IdSeccion].Add(i + IdCylinder * PrecisionCylinders*2);
        SeccionTriangles[IdMesh][IdSeccion].Add(PrecisionCylinders + (i+1)%PrecisionCylinders + IdCylinder * PrecisionCylinders*2);
    }

    //debo hacer un need recreate
    NeedRecreateSeccion[IdMesh][IdSeccion] = true;

    FDrawElementReferenceStruct Reference;
    Reference.IdLayer = IdMesh;
    Reference.IdSection = IdSeccion;
    Reference.IdElement = IdCylinder;
    return Reference;
}

void ACylindersDrawer::RemoveCylinderInMeshSection(int IdMesh, int IdSeccion, int IdCylinder) {
}

void ACylindersDrawer::RemoveCylinderInMeshSection(FDrawElementReferenceStruct CylinderReference) {
}

void ACylindersDrawer::UpdatePosicionAndRadioCylinderInMeshSeccion(int IdMesh, int IdSeccion, int IdCylinder, FVector NewSource, FVector NewTarget, float NewRadio) {
    FVector Direccion = (NewTarget - NewSource).GetSafeNormal();
    //calculando vertices superioes

    float phi = FMath::Acos(Direccion.Z);
    float theta = FMath::Acos(Direccion.X / FMath::Sin(phi)) + PI/2;
    if (Direccion.Y < 0) {
        theta = PI - theta;
    }
    phi = PI/2;
    FVector VectorU;
    VectorU.X = 1.0f * FMath::Sin(phi) * FMath::Cos(theta);
    VectorU.Y = 1.0f * FMath::Sin(phi) * FMath::Sin(theta);
    VectorU.Z = 1.0f * FMath::Cos(phi);
    FVector VectorV = FVector::CrossProduct(Direccion, VectorU).GetSafeNormal();
    float DeltaTheta = 2 * PI / PrecisionCylinders;

    for (int i = 0; i < PrecisionCylinders; i++) {
        SeccionVertices[IdMesh][IdSeccion][i + IdCylinder * PrecisionCylinders * 2] = NewTarget + (VectorU*FMath::Cos(i*DeltaTheta) + VectorV * FMath::Sin(i*DeltaTheta))* NewRadio;// RadioAristas;
        SeccionNormals[IdMesh][IdSeccion][i + IdCylinder * PrecisionCylinders*2] = (VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta)).GetSafeNormal();
        SeccionTangents[IdMesh][IdSeccion][i + IdCylinder * PrecisionCylinders*2] = FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2));
    }
    for (int i = 0; i < PrecisionCylinders; i++) {
        SeccionVertices[IdMesh][IdSeccion][i + PrecisionCylinders + IdCylinder * PrecisionCylinders * 2] = NewSource + (VectorU*FMath::Cos(i*DeltaTheta) + VectorV * FMath::Sin(i*DeltaTheta))*NewRadio;// RadioAristas;
        SeccionNormals[IdMesh][IdSeccion][i + PrecisionCylinders + IdCylinder * PrecisionCylinders *2] = (VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta)).GetSafeNormal();
        SeccionTangents[IdMesh][IdSeccion][i + PrecisionCylinders + IdCylinder * PrecisionCylinders *2] = FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2));
    }

    NeedUpdateSeccion[IdMesh][IdSeccion] = true;
}

void ACylindersDrawer::UpdatePosicionAndRadioCylinderInMeshSeccion(FDrawElementReferenceStruct CylinderReference, FVector NewSource, FVector NewTarget, float NewRadio) {
    UpdatePosicionAndRadioCylinderInMeshSeccion(CylinderReference.IdLayer, CylinderReference.IdSection, CylinderReference.IdElement, NewSource, NewTarget, NewRadio);
}

bool ACylindersDrawer::IsValidReference(const FDrawElementReferenceStruct & CylinderReference) {
    return false;
}

void ACylindersDrawer::ActivateDepthStencilOnLayer(int IdMesh) {
    if (IdMesh < Meshes.Num()) {
        Meshes[IdMesh]->bRenderCustomDepth = true;
    }
}

void ACylindersDrawer::DeactivateDepthStencilOnLayer(int IdMesh) {
    if (IdMesh < Meshes.Num()) {
        Meshes[IdMesh]->bRenderCustomDepth = false;
    }
}

void ACylindersDrawer::SetDepthStencil(int IdMesh, int NewStencil) {
    if (IdMesh < Meshes.Num()) {
        Meshes[IdMesh]->SetCustomDepthStencilValue(NewStencil);
    }
}
