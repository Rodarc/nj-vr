// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "../Structures/DrawElementReferenceStruct.h"
#include "CylindersDrawer.generated.h"

UCLASS()
class NJVR_API ACylindersDrawer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACylindersDrawer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    //Procedural mesh para los nodos resaltados 
	UPROPERTY(VisibleAnywhere, Category = "Visualization - Parametros")
	TArray<UProceduralMeshComponent *> Meshes;//layer

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial * DefaultMeshesMaterial;//para cada seccion

	TArray<TArray<UMaterial *>> MeshesMaterials;//para cada seccion

    TArray<TArray<bool>> NeedUpdateSeccion;

    TArray<TArray<bool>> NeedRecreateSeccion;

	TArray<TArray<TArray<FVector>>> SeccionVertices;

	TArray<TArray<TArray<FVector>>> SeccionVerticesP;

	TArray<TArray<TArray<int32>>> SeccionTriangles;

	TArray<TArray<TArray<FVector>>> SeccionNormals;

	TArray<TArray<TArray<FVector2D>>> SeccionUV0;

	TArray<TArray<TArray<FProcMeshTangent>>> SeccionTangents;

	TArray<TArray<TArray<FLinearColor>>> SeccionVertexColors;

    void Update();

    int CreateMesh();//o layer

    int CreateSeccionInMesh(int IdMesh);

    void SetMaterialSeccionMesh(int IdMesh, int IdSeccion, UMaterial * NewMaterial);

    void DeleteMesh(int IdMesh);
    
    int DeleteSeccionInMesh(int IdMesh, int IdSeccion);

    int PrecisionCylinders; //NumeroLadosArista;//este tambien indica el tamano de los arreglos

    FDrawElementReferenceStruct AddCylinderToMeshSeccion(int IdMesh, int IdSeccion, int IdCylinder, FVector Source, FVector Target, float Radio, FLinearColor Color);//idSphere era NumNodo

    void RemoveCylinderInMeshSection(int IdMesh, int IdSeccion, int IdCylinder);//de que seccion y cual elimino

    void RemoveCylinderInMeshSection(FDrawElementReferenceStruct CylinderReference);//de que seccion y cual elimino

    void UpdatePosicionAndRadioCylinderInMeshSeccion(int IdMesh, int IdSeccion, int IdNodo, FVector NewSource, FVector NewTarget, float NewRadio);//aun que en teoria la seccio esta en el propio ndo, no necestio recibirlo

    void UpdatePosicionAndRadioCylinderInMeshSeccion(FDrawElementReferenceStruct SphereReference, FVector NewSource, FVector NewTarget, float NewRadio);//aun que en teoria la seccio esta en el propio ndo, no necestio recibirlo

    bool IsValidReference(const FDrawElementReferenceStruct & CylinderReference);

    void ActivateDepthStencilOnLayer(int IdMesh);

    void DeactivateDepthStencilOnLayer(int IdMesh);

    void SetDepthStencil(int IdMesh, int NewStencil);

};
