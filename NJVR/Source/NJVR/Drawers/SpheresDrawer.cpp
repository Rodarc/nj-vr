// Fill out your copyright notice in the Description page of Project Settings.


#include "SpheresDrawer.h"
#include "Materials/Material.h"
#include "UObject/ConstructorHelpers.h"



// Sets default values
ASpheresDrawer::ASpheresDrawer() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    //necesito establecer un material por defecto
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Centro"));

    static ConstructorHelpers::FObjectFinder<UMaterial> DefaultMaterialAsset(TEXT("Material'/Game/Visualization/Materials/NodosMeshMaterial.NodosMeshMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (DefaultMaterialAsset.Succeeded()) {
        DefaultMeshesMaterial = DefaultMaterialAsset.Object;
    }


    UmbralSizeElementsProceduralMesh = 1000;//el umbral para mi laptop sera de 1000 nodos

}

// Called when the game starts or when spawned
void ASpheresDrawer::BeginPlay() {
	Super::BeginPlay();
    CreateSphereTemplate(1);
	
}

// Called every frame
void ASpheresDrawer::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

void ASpheresDrawer::CreateSphereTemplate(int Precision) {
    UE_LOG(LogClass, Log, TEXT("Creando esfera"));
    float DeltaPhi = 1.10714872;
    float DeltaTheta = PI * 72 / 180;
    float DiffTheta = PI * 36 / 180;
    VerticesSphereTemplate.push_back(FVector(0.0f, 0.0f, 1.0f));//puntos para radio uno
    NormalsSphereTemplate.push_back(FVector(0.0f, 0.0f, 1.0f));//puntos para radio uno
    //Tangents.Add(FProcMeshTangent(1.0f, 0.0f, 0.0f));
    TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(PI/2), 1.0f * FMath::Cos(PI/2)));
    VerticesPSphereTemplate.push_back(FVector(0.0f, 0.0f, 1.0f));
    UV0SphereTemplate.push_back(FVector2D(0.0f,1.0f));
	VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));
    for (int i = 0; i < 5; i++) {
        VerticesPSphereTemplate.push_back(FVector(DeltaPhi, i*DeltaTheta, 1.0f));//esto es en esferico, no se para que lo uso desues, creo que para las divisiones para aumentr la suavidad
        FVector posicion;
        posicion.X = 1.0f * FMath::Sin(DeltaPhi) * FMath::Cos(i*DeltaTheta);
        posicion.Y = 1.0f * FMath::Sin(DeltaPhi) * FMath::Sin(i*DeltaTheta);
        posicion.Z = 1.0f * FMath::Cos(DeltaPhi);
        //hasta aqui posicion esta como unitario, puede servir para el verctor de normales
        VerticesSphereTemplate.push_back(posicion);
        NormalsSphereTemplate.push_back(posicion);
        TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        UV0SphereTemplate.push_back(FVector2D(i*DeltaTheta/(2*PI), 1 - DeltaPhi/PI));
        VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));
    }
    for (int i = 0; i < 5; i++) {
        VerticesPSphereTemplate.push_back(FVector(PI - DeltaPhi, i*DeltaTheta + DiffTheta, 1.0f));
        FVector posicion;
        posicion.X = 1.0f * FMath::Sin(PI - DeltaPhi) * FMath::Cos(i*DeltaTheta + DiffTheta);
        posicion.Y = 1.0f * FMath::Sin(PI - DeltaPhi) * FMath::Sin(i*DeltaTheta + DiffTheta);
        posicion.Z = 1.0f * FMath::Cos(PI - DeltaPhi);
        //hasta aqui posicion esta como unitario, puede servir para el verctor de normales
        NormalsSphereTemplate.push_back(posicion);
        TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        VerticesSphereTemplate.push_back(posicion);
        UV0SphereTemplate.push_back(FVector2D((i*DeltaTheta + DiffTheta)/(2*PI), 1 - (PI - DeltaPhi)/PI));
        VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));
    }
    VerticesSphereTemplate.push_back(FVector(0.0, 0.0f, -1.0f));//puntos para radio uno
    NormalsSphereTemplate.push_back(FVector(0.0, 0.0f, -1.0f));//puntos para radio uno
    //Tangents.Add(FProcMeshTangent(-1.0f, 0.0f, 0.0f));
    TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(PI/2), 1.0f * FMath::Cos(PI/2)));
    VerticesPSphereTemplate.push_back(FVector(PI, 0.0f, 1.0f));
    UV0SphereTemplate.push_back(FVector2D(0.0f,0.0f));
    VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    //Agregados vertices y normales, faltarian agregar las tangentes

    //Tirangulos superiores
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[0], VerticesSphereTemplate[1], VerticesSphereTemplate[2], 0, 1, 2, 0, 0));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[0], VerticesSphereTemplate[2], VerticesSphereTemplate[3], 0, 2, 3, 0, 0));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[0], VerticesSphereTemplate[3], VerticesSphereTemplate[4], 0, 3, 4, 0, 0));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[0], VerticesSphereTemplate[4], VerticesSphereTemplate[5], 0, 4, 5, 0, 0));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[0], VerticesSphereTemplate[5], VerticesSphereTemplate[1], 0, 5, 1, 0, 0));

    //triangulos medios
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[1], VerticesSphereTemplate[6], VerticesSphereTemplate[2], 1, 6, 2, 1, 1));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[2], VerticesSphereTemplate[6], VerticesSphereTemplate[7], 2, 6, 7, 1, 0));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[2], VerticesSphereTemplate[7], VerticesSphereTemplate[3], 2, 7, 3, 1, 1));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[3], VerticesSphereTemplate[7], VerticesSphereTemplate[8], 3, 7, 8, 1, 0));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[3], VerticesSphereTemplate[8], VerticesSphereTemplate[4], 3, 8, 4, 1, 1));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[4], VerticesSphereTemplate[8], VerticesSphereTemplate[9], 4, 8, 9, 1, 0));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[4], VerticesSphereTemplate[9], VerticesSphereTemplate[5], 4, 9, 5, 1, 1));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[5], VerticesSphereTemplate[9], VerticesSphereTemplate[10], 5, 9, 10, 1, 0));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[5], VerticesSphereTemplate[10], VerticesSphereTemplate[1], 5, 10, 1, 1, 1));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[1], VerticesSphereTemplate[10], VerticesSphereTemplate[6], 1, 10, 6, 1, 0));

    //triangulos inferiores
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[6], VerticesSphereTemplate[11], VerticesSphereTemplate[7], 6, 11, 7, 2, 1));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[7], VerticesSphereTemplate[11], VerticesSphereTemplate[8], 7, 11, 8, 2, 1));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[8], VerticesSphereTemplate[11], VerticesSphereTemplate[9], 8, 11, 9, 2, 1));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[9], VerticesSphereTemplate[11], VerticesSphereTemplate[10], 9, 11, 10, 2, 1));
    TriangulosSphereTemplate.push_back(Triangulo(VerticesSphereTemplate[10], VerticesSphereTemplate[11], VerticesSphereTemplate[6], 10, 11, 6, 2, 1));

    while (Precision) {
        DividirTriangulos();
        Precision--;
    }
}

FVector ASpheresDrawer::PuntoTFromAToB(FVector a, FVector b, float t) {
    FVector direccion = b - a;
    FVector punto = a + direccion*t;
    //normalizar a esfera de radio 1
    punto = punto.GetSafeNormal();
    return punto;
}

FVector ASpheresDrawer::PuntoTFromAToBEsferico(FVector a, FVector b, float t) {
    FVector direccion = b - a;
    FVector punto = a + direccion*t;
    //normalizar a esfera de radio 1
    punto = punto.GetSafeNormal();
    float phi = FMath::Acos(punto.Z);
    float theta = FMath::Asin(punto.Y / FMath::Sin(phi));
    FVector puntoesferico (phi, theta, 1.0f);
    return puntoesferico;
}

void ASpheresDrawer::DividirTriangulo(Triangulo t) {
    FVector D = PuntoTFromAToB(t.C, t.A, 0.5);
    VerticesSphereTemplate.push_back(D);//0.5 es la mitad
    int IdD = VerticesSphereTemplate.size() - 1;
    FVector DP = PuntoTFromAToBEsferico(t.C, t.A, 0.5);
    VerticesPSphereTemplate.push_back(DP);//0.5 es la mitad
    NormalsSphereTemplate.push_back(D);
    TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(DP.Y + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(DP.Y + PI/2), 1.0f * FMath::Cos(PI/2)));
    UV0SphereTemplate.push_back(FVector2D(DP.Y/(2*PI), 1 - DP.X/PI));
    VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    FVector E = PuntoTFromAToB(t.B, t.A, 0.5);
    VerticesSphereTemplate.push_back(E);//0.5 es la mitad
    int IdE = VerticesSphereTemplate.size() - 1;
    FVector EP = PuntoTFromAToBEsferico(t.B, t.A, 0.5);
    VerticesPSphereTemplate.push_back(EP);//0.5 es la mitad
    NormalsSphereTemplate.push_back(E);
    TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(EP.Y + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(EP.Y + PI/2), 1.0f * FMath::Cos(PI/2)));
    UV0SphereTemplate.push_back(FVector2D(EP.Y/(2*PI), 1 - EP.X/PI));
    VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    FVector F = PuntoTFromAToB(t.C, t.B, 0.5);
    VerticesSphereTemplate.push_back(F);//0.5 es la mitad
    int IdF = VerticesSphereTemplate.size() - 1;
    FVector FP = PuntoTFromAToBEsferico(t.C, t.B, 0.5);
    VerticesPSphereTemplate.push_back(FP);//0.5 es la mitad
    NormalsSphereTemplate.push_back(F);
    TangentsSphereTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(FP.Y + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(FP.Y + PI/2), 1.0f * FMath::Cos(PI/2)));
    UV0SphereTemplate.push_back(FVector2D(FP.Y/(2*PI), 1 - FP.X/PI));
    VertexColorsSphereTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    //if (VerticesP[t.IdA].X == VerticesP[t.IdC].X) {//tringulo hacia abajo
    if (t.Orientacion) {//tringulo hacia abajo
        TriangulosSphereTemplate.push_back(Triangulo(t.A, E, D, t.IdA, IdE, IdD, t.Nivel*2, 0));
        TriangulosSphereTemplate.push_back(Triangulo(D, E, F, IdD, IdE, IdF, t.Nivel*2, 0));
        TriangulosSphereTemplate.push_back(Triangulo(D, F, t.C, IdD, IdF, t.IdC, t.Nivel*2, 0));
        TriangulosSphereTemplate.push_back(Triangulo(E, t.B, F, IdE, t.IdB, IdF, t.Nivel*2 + 1, 1));
    }
    else {//triangulo hacia arriba, por lo tanto c y b esta a la misma algutar, a arriba
        TriangulosSphereTemplate.push_back(Triangulo(t.A, E, D, t.IdA, IdE, IdD, t.Nivel*2, 0));
        TriangulosSphereTemplate.push_back(Triangulo(E, t.B, F, IdE, t.IdB, IdF, t.Nivel*2 + 1, 1));
        TriangulosSphereTemplate.push_back(Triangulo(E, F, D, IdE, IdF, IdD, t.Nivel*2+1, 1));
        TriangulosSphereTemplate.push_back(Triangulo(D, F, t.C, IdD, IdF, t.IdC, t.Nivel*2 + 1, 1));
    }
}

void ASpheresDrawer::DividirTriangulos() {
    int n = TriangulosSphereTemplate.size();
    for (int i = 0; i < n; i++) {
        DividirTriangulo(TriangulosSphereTemplate[0]);
        TriangulosSphereTemplate.erase(TriangulosSphereTemplate.begin());
    }
}

void ASpheresDrawer::Update() {//update nodos meshes
    for (int i = 0; i < NeedRecreateSeccion.Num(); i++) {//deberia tener un array de booleanos, para ver cual actualizo y cual no
        for (int j = 0; j < NeedRecreateSeccion[i].Num(); j++) {
            if (NeedRecreateSeccion[i][j]) {
                Meshes[i]->ClearMeshSection(j);
                Meshes[i]->CreateMeshSection_LinearColor(j, SeccionVertices[i][j], SeccionTriangles[i][j], SeccionNormals[i][j], SeccionUV0[i][j], SeccionVertexColors[i][j], SeccionTangents[i][j], false);
                NeedRecreateSeccion[i][j] = false;
                NeedUpdateSeccion[i][j] = false;
            }
            if (NeedUpdateSeccion[i][j]) {
                Meshes[i]->UpdateMeshSection_LinearColor(j, SeccionVertices[i][j], SeccionNormals[i][j], SeccionUV0[i][j], SeccionVertexColors[i][j], SeccionTangents[i][j]);
                //despues de hacer update, como ya actualice debo ponerlo en falso;
                NeedUpdateSeccion[i][j] = false;
            }
        }
    }
}

int ASpheresDrawer::CreateMesh() {//cuando creo un mesh deberia crear los arrays necesarios tambein
    int NewId = Meshes.Num();
    FString Name = "GeneratedSphereMesh";
    Name = Name + FString::FromInt(NewId);
    //UProceduralMeshComponent * SphereMesh = NewObject<UProceduralMeshComponent>(FName(*Name));
    UProceduralMeshComponent * SphereMesh = NewObject<UProceduralMeshComponent>(this, FName(*Name));
    if (SphereMesh) {
        SphereMesh->RegisterComponent();
        SphereMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
        SphereMesh->bUseAsyncCooking = true;
        //SphereMesh->bRenderCustomDepth = true; //estas dos lineas son para los que seran resaltado, proveer otras funciones que modifiquen esto, esto es a nivel de capas, o meshes
        //SphereMesh->SetCustomDepthStencilValue(255 - (8-i));
        SphereMesh->ContainsPhysicsTriMeshData(false);

        Meshes.Add(SphereMesh);
        MeshesMaterials.AddDefaulted();

        //el primer nivel es a que mesh pertenece
		NeedUpdateSeccion.AddDefaulted();
        NeedRecreateSeccion.AddDefaulted();
		SeccionVertices.AddDefaulted();
		SeccionVerticesP.AddDefaulted();
		SeccionTriangles.AddDefaulted();
		SeccionNormals.AddDefaulted();
		SeccionUV0.AddDefaulted();
		SeccionTangents.AddDefaulted();
		SeccionVertexColors.AddDefaulted();

        return NewId;
        //el mesh ha sido creado, pero no tiene ninguna seccion, deberia crear sus contenedores, es decir hacer creacer el primer nivel de los arrays
    }
    return -1;
}

int ASpheresDrawer::CreateSeccionInMesh(int IdMesh) {//aqui creo secciones en algun mesh que tenga
    if (IdMesh < Meshes.Num()) {//se asigna un material por defecto basico
		//int IdNewSeccion = SeccionNodosResaltados.AddDefaulted();//guarda los nodos que pertenencen a cada seccion
        //esta clase aun conservara array de ndoso para saber a que capa o rama pertencen, pero todo el manejo de secciones estara manejado en el actor
		int IdNewSeccion = NeedUpdateSeccion[IdMesh].Add(false);
		NeedRecreateSeccion[IdMesh].Add(false);
		SeccionVertices[IdMesh].AddDefaulted();
		SeccionVerticesP[IdMesh].AddDefaulted();
		SeccionTriangles[IdMesh].AddDefaulted();
		SeccionNormals[IdMesh].AddDefaulted();
		SeccionUV0[IdMesh].AddDefaulted();
		SeccionTangents[IdMesh].AddDefaulted();
		SeccionVertexColors[IdMesh].AddDefaulted();
		UE_LOG(LogClass, Log, TEXT("Creando seccion [%d] en mesh [%d]"), IdNewSeccion, IdMesh);
		Meshes[IdMesh]->CreateMeshSection_LinearColor(IdNewSeccion, SeccionVertices[IdMesh][IdNewSeccion], SeccionTriangles[IdMesh][IdNewSeccion], SeccionNormals[IdMesh][IdNewSeccion], SeccionUV0[IdMesh][IdNewSeccion], SeccionVertexColors[IdMesh][IdNewSeccion], SeccionTangents[IdMesh][IdNewSeccion], false);
		if (DefaultMeshesMaterial) {
			Meshes[IdMesh]->SetMaterial(IdNewSeccion, DefaultMeshesMaterial);
            MeshesMaterials[IdMesh].Add(DefaultMeshesMaterial);

		}
        return IdNewSeccion;
    }
    return -1;
}

void ASpheresDrawer::SetMaterialSeccionMesh(int IdMesh, int IdSeccion, UMaterial * NewMaterial) {
    if (NewMaterial) {
        Meshes[IdMesh]->SetMaterial(IdSeccion, NewMaterial);
    }
}

void ASpheresDrawer::DeleteMesh(int IdMesh) {
}

int ASpheresDrawer::DeleteSeccionInMesh(int IdMesh, int IdSeccion) {
    return 0;
}

FDrawElementReferenceStruct ASpheresDrawer::AddSphereToMeshSeccion(int IdMesh, int IdSeccion, int IdSphere, FVector Posicion, float Radio, FLinearColor Color) {
    for (int i = 0; i < VerticesSphereTemplate.size(); i++) {
        SeccionVertices[IdMesh][IdSeccion].Add(VerticesSphereTemplate[i] * Radio + Posicion);
    }
    for (int i = 0; i < VerticesPSphereTemplate.size(); i++) {
        SeccionVerticesP[IdMesh][IdSeccion].Add(VerticesPSphereTemplate[i]);
    }
    for (int i = 0; i < TriangulosSphereTemplate.size(); i++) {
        SeccionTriangles[IdMesh][IdSeccion].Add(TriangulosSphereTemplate[i].IdC + IdSphere * VerticesSphereTemplate.size());
        SeccionTriangles[IdMesh][IdSeccion].Add(TriangulosSphereTemplate[i].IdB + IdSphere * VerticesSphereTemplate.size());
        SeccionTriangles[IdMesh][IdSeccion].Add(TriangulosSphereTemplate[i].IdA + IdSphere * VerticesSphereTemplate.size());
    }
    for (int i = 0; i < NormalsSphereTemplate.size(); i++) {
        SeccionNormals[IdMesh][IdSeccion].Add(NormalsSphereTemplate[i]);
    }
    for (int i = 0; i < UV0SphereTemplate.size(); i++) {
        SeccionUV0[IdMesh][IdSeccion].Add(UV0SphereTemplate[i]);
    }
    for (int i = 0; i < TangentsSphereTemplate.size(); i++) {
        SeccionTangents[IdMesh][IdSeccion].Add(TangentsSphereTemplate[i]);
    }
    for (int i = 0; i < VertexColorsSphereTemplate.size(); i++) {
        SeccionVertexColors[IdMesh][IdSeccion].Add(Color);
    }

    //debo hacer un need recreate
    NeedRecreateSeccion[IdMesh][IdSeccion] = true;

    FDrawElementReferenceStruct Reference;
    Reference.IdLayer = IdMesh;
    Reference.IdSection = IdSeccion;
    Reference.IdElement = IdSphere;
    return Reference;
}

void ASpheresDrawer::RemoveSphereInMeshSection(int IdMesh, int IdSeccion, int IdSphere) {
    SeccionVertices[IdMesh][IdSeccion].RemoveAt(IdSphere*VerticesSphereTemplate.size(),  VerticesSphereTemplate.size());//posicion y tama��
    SeccionVerticesP[IdMesh][IdSeccion].RemoveAt(IdSphere*VerticesPSphereTemplate.size(),  VerticesPSphereTemplate.size());//posicion y tama��
    SeccionTriangles[IdMesh][IdSeccion].RemoveAt(SeccionTriangles[IdMesh][IdSeccion].Num() - TriangulosSphereTemplate.size()*3,  TriangulosSphereTemplate.size()*3);//posicion y tama��
    SeccionNormals[IdMesh][IdSeccion].RemoveAt(IdSphere*NormalsSphereTemplate.size(), NormalsSphereTemplate.size());
    SeccionUV0[IdMesh][IdSeccion].RemoveAt(IdSphere*UV0SphereTemplate.size(), UV0SphereTemplate.size());
    SeccionTangents[IdMesh][IdSeccion].RemoveAt(IdSphere*TangentsSphereTemplate.size(), TangentsSphereTemplate.size());
    SeccionVertexColors[IdMesh][IdSeccion].RemoveAt(IdSphere*VertexColorsSphereTemplate.size(), VertexColorsSphereTemplate.size());
    NeedRecreateSeccion[IdMesh][IdSeccion] = true;
}

void ASpheresDrawer::RemoveSphereInMeshSection(FDrawElementReferenceStruct SphereReference) {
    RemoveSphereInMeshSection(SphereReference.IdLayer, SphereReference.IdSection, SphereReference.IdElement);
}

void ASpheresDrawer::UpdatePosicionAndRadioSphereInMeshSeccion(int IdMesh, int IdSeccion, int IdSphere, FVector NewPosition, float NewRadio) {
    for (int i = 0; i < VerticesSphereTemplate.size(); i++) {
        SeccionVertices[IdMesh][IdSeccion][i + IdSphere * VerticesSphereTemplate.size()] = VerticesSphereTemplate[i] * NewRadio + NewPosition;//esta no es la forma mas optima
    }
    NeedUpdateSeccion[IdMesh][IdSeccion] = true;
}

void ASpheresDrawer::UpdatePosicionAndRadioSphereInMeshSeccion(FDrawElementReferenceStruct SphereReference, FVector NewPosition, float NewRadio) {
    UpdatePosicionAndRadioSphereInMeshSeccion(SphereReference.IdLayer, SphereReference.IdSection, SphereReference.IdElement, NewPosition, NewRadio);
}

void ASpheresDrawer::UpdateColorSphereInMeshSeccion(int IdMesh, int IdSeccion, int IdSphere, FLinearColor NewColor) {
    for (int i = 0; i < VertexColorsSphereTemplate.size(); i++) {
        SeccionVertexColors[IdMesh][IdSeccion][i + IdSphere * VertexColorsSphereTemplate.size()] = NewColor;//esta no es la forma mas optima
    }
    NeedUpdateSeccion[IdMesh][IdSeccion] = true;
}

void ASpheresDrawer::UpdateColorSphereInMeshSeccion(FDrawElementReferenceStruct SphereReference, FLinearColor NewColor) {
    UpdateColorSphereInMeshSeccion(SphereReference.IdLayer, SphereReference.IdSection, SphereReference.IdElement, NewColor);
}

bool ASpheresDrawer::IsValidReference(const FDrawElementReferenceStruct & SphereReference) {
    return false;
}

void ASpheresDrawer::ActivateDepthStencilOnLayer(int IdMesh) {
    if (IdMesh < Meshes.Num()) {
        Meshes[IdMesh]->bRenderCustomDepth = true;
    }
}

void ASpheresDrawer::DeactivateDepthStencilOnLayer(int IdMesh) {
    if (IdMesh < Meshes.Num()) {
        Meshes[IdMesh]->bRenderCustomDepth = false;
    }
}

void ASpheresDrawer::SetDepthStencil(int IdMesh, int NewStencil) {
    if (IdMesh < Meshes.Num()) {
        Meshes[IdMesh]->SetCustomDepthStencilValue(NewStencil);
    }
}
