// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "../Structures/Triangulo.h"
#include "../Structures/DrawElementReferenceStruct.h"
#include <vector>
#include "SpheresDrawer.generated.h"

UCLASS()
class NJVR_API ASpheresDrawer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpheresDrawer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    int UmbralSizeElementsProceduralMesh;//el umbral para mi laptop sera de 1000 nodos

    //para el template de la esfera
    std::vector<Triangulo> TriangulosSphereTemplate;

    std::vector<FVector> VerticesSphereTemplate;

    std::vector<FVector> VerticesPSphereTemplate;// phi, theta, radio

	std::vector<FVector> NormalsSphereTemplate;

	std::vector<FVector2D> UV0SphereTemplate;

	std::vector<FProcMeshTangent> TangentsSphereTemplate;

	std::vector<FLinearColor> VertexColorsSphereTemplate;

    void CreateSphereTemplate(int Precision);// crea esferas de radio 1

    FVector PuntoTFromAToB(FVector a, FVector b, float t);

    FVector PuntoTFromAToBEsferico(FVector a, FVector b, float t);

    void DividirTriangulo(Triangulo t);

    void DividirTriangulos();

    //Procedural mesh para los nodos resaltados 
	UPROPERTY(VisibleAnywhere, Category = "Visualization - Parametros")
	TArray<UProceduralMeshComponent *> Meshes;//layer

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial * DefaultMeshesMaterial;//para cada seccion

	TArray<TArray<UMaterial *>> MeshesMaterials;//para cada seccion

    TArray<TArray<bool>> NeedUpdateSeccion;

    TArray<TArray<bool>> NeedRecreateSeccion;

	TArray<TArray<TArray<FVector>>> SeccionVertices;

	TArray<TArray<TArray<FVector>>> SeccionVerticesP;

	TArray<TArray<TArray<int32>>> SeccionTriangles;

	TArray<TArray<TArray<FVector>>> SeccionNormals;

	TArray<TArray<TArray<FVector2D>>> SeccionUV0;

	TArray<TArray<TArray<FProcMeshTangent>>> SeccionTangents;

	TArray<TArray<TArray<FLinearColor>>> SeccionVertexColors;

    void Update();

    int CreateMesh();//o layer

    int CreateSeccionInMesh(int IdMesh);

    void SetMaterialSeccionMesh(int IdMesh, int IdSeccion, UMaterial * NewMaterial);

    void DeleteMesh(int IdMesh);
    
    int DeleteSeccionInMesh(int IdMesh, int IdSeccion);

    FDrawElementReferenceStruct AddSphereToMeshSeccion(int IdMesh, int IdSeccion, int IdSphere, FVector Posicion, float Radio, FLinearColor Color);//idSphere era NumNodo

    void RemoveSphereInMeshSection(int IdMesh, int IdSeccion, int IdSphere);//de que seccion y cual elimino

    void RemoveSphereInMeshSection(FDrawElementReferenceStruct SphereReference);//de que seccion y cual elimino

    void UpdatePosicionAndRadioSphereInMeshSeccion(int IdMesh, int IdSeccion, int IdNodo, FVector NewPosition, float NewRadio);//aun que en teoria la seccio esta en el propio ndo, no necestio recibirlo

    void UpdatePosicionAndRadioSphereInMeshSeccion(FDrawElementReferenceStruct SphereReference, FVector NewPosition, float NewRadio);//aun que en teoria la seccio esta en el propio ndo, no necestio recibirlo

    void UpdateColorSphereInMeshSeccion(int IdMesh, int IdSeccion, int IdNodo, FLinearColor NewColor);

    void UpdateColorSphereInMeshSeccion(FDrawElementReferenceStruct SphereReference, FLinearColor NewColor);

    bool IsValidReference(const FDrawElementReferenceStruct & SphereReference);

    void ActivateDepthStencilOnLayer(int IdMesh);

    void DeactivateDepthStencilOnLayer(int IdMesh);

    void SetDepthStencil(int IdMesh, int NewStencil);

};
