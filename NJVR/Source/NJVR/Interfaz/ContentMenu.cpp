// Fill out your copyright notice in the Description page of Project Settings.


#include "ContentMenu.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/ScrollBoxSlot.h"
#include "../Actors/PanelBotones.h"


UContentMenu::UContentMenu(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    static ConstructorHelpers::FClassFinder<UContentNodo> ContentNodoWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/NJVR/Blueprints/Interfaz/ContentNodoBP.ContentNodoBP_C'"));
    if (ContentNodoWidgetClass.Succeeded()) {
        TypeContenido = ContentNodoWidgetClass.Class;
    }
}

UContentNodo * UContentMenu::CrearConentido() {
    //debo crear un contenido, este estara vacio pero ubicado
    UContentNodo * contenido = CreateWidget<UContentNodo>(UGameplayStatics::GetGameInstance(GetWorld()), TypeContenido);
    Contenidos.Add(contenido);
    if (ContentScrollBox) {
        UPanelSlot * ChildSlot = ContentScrollBox->AddChild(contenido);
        UScrollBoxSlot * ScrollBoxSlot = Cast<UScrollBoxSlot>(ChildSlot);
        ScrollBoxSlot->SetPadding(FMargin(5.0f, 5.0f));
    }
	contenido->ContentMenu = this;
    return contenido;
}

void UContentMenu::BorrarContenido(UContentNodo * Content) {//como se borra?
	if (!Content->Node) {//es label de una seleccion
		//le debo decir al panel de botones, o alguien que mande eliminar el resaltado
		//necesito mi id;o por el color mejor
		PanelBotones->EliminarLabel(Content->OutlineColor);
	}
	Contenidos.Remove(Content);
	Content->RemoveFromParent();
	Content->Destruct();
}

void UContentMenu::BuscarContenido(Nodo * Node) {
}
