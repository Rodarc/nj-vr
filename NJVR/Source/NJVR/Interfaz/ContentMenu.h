// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/ScrollBox.h"
#include "ContentNodo.h"
#include "../NJ/Nodo.h"
#include "ContentMenu.generated.h"

/**
 * 
 */
UCLASS()
class NJVR_API UContentMenu : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UContentMenu(const FObjectInitializer & ObjectInitializer);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Contenido")
	class APanelBotones * PanelBotones;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Contenido")
    UScrollBox * ContentScrollBox;

    //deberia tener fucionaes para crear un nuevo contenido, y que lo devuelva, y yo le doy los datos, o que tenga una funciond e agregar contendio, y la ivuslizacion le pase ya el widget creado
    //tipode contenido
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Contenido")
    TSubclassOf<UContentNodo> TypeContenido;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Contenido")
    TArray<UContentNodo *> Contenidos;

    UFUNCTION(BlueprintCallable, Category = "Contenido")
    UContentNodo * CrearConentido();//podria recibir tambine nodo

    UFUNCTION(BlueprintCallable, Category = "Contenido")
    void BorrarContenido(UContentNodo * Content);

    void BuscarContenido(Nodo * Node);////para ver si lo tengo o no, o si ya hay un nodo con eso, aun que tambien el nodo deberia saber si ya estoy visualizando su contenido


};
