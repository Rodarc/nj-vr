// Fill out your copyright notice in the Description page of Project Settings.


#include "ContentNodo.h"
#include "UObject/ConstructorHelpers.h"
#include "../NJ/Nodo.h"

UContentNodo::UContentNodo(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {

    ColorFondoTitle = FLinearColor(0.010f, 0.010f, 0.010f, 0.97);
    ColorFondoContent = FLinearColor(0.005f, 0.005f, 0.005f, 1.0);
    OutlineColor = FLinearColor(1.0f, 1.0f, 1.0f, 0.9);

    /*static ConstructorHelpers::FObjectFinder<USlateBrushAsset> FondoLabelBrushFinder(TEXT("SlateBrushAsset'/Game/GeoVisualization/UMG/FondoGraficaBrush.FondoGraficaBrush'"));
    //deberia buscar una grafica
    if (FondoLabelBrushFinder.Succeeded()) {
        BrushFondoLabel = FondoLabelBrushFinder.Object;
    }*/
	Node = nullptr;
}

void UContentNodo::SetNodo(Nodo * NewNode) {
    Node = NewNode;
    Title = FString(Node->Labels[0]);
}

void UContentNodo::SetContent(FString NewContent) {
    Content = NewContent;
}

void UContentNodo::ActualizarContent_Implementation() {
}
