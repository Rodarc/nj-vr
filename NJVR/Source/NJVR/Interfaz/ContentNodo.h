// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "ContentNodo.generated.h"

/**
 * 
 */
UCLASS()
class NJVR_API UContentNodo : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UContentNodo(const FObjectInitializer & ObjectInitializer);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Contenido")
	class UContentMenu * ContentMenu;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Label - Visual")
    FLinearColor ColorFondoTitle;//no se si lo use

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Label - Visual")
    FLinearColor ColorFondoContent;//no se si lo use

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Contenido")
    FLinearColor OutlineColor;

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    //USlateBrushAsset * BrushFondoLabel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Label - Data")
    FString Title;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Label - Data")
    FString Content;

    class Nodo * Node;

    void SetNodo(Nodo * NewNode);

    void SetContent(FString NewContent);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Label - Actualizar")
    void ActualizarContent();
    virtual void ActualizarContent_Implementation();
};
