// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "LabelNodo.generated.h"

/**
 * 
 */
UCLASS()
class NJVR_API ULabelNodo : public UUserWidget
{
	GENERATED_BODY()
	
public:
    ULabelNodo(const FObjectInitializer & ObjectInitializer);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Label - Visual")
    FLinearColor ColorFondoLabel;//no se si lo use

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    //USlateBrushAsset * BrushFondoLabel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Label - Data")
    FString NameNodo;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Label - Data")
    FString Numero;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Label - Actualizar")
    void ActualizarLabel();
    virtual void ActualizarLabel_Implementation();
};
