// Fill out your copyright notice in the Description page of Project Settings.


#include "DatasetLibrary.h"
#include "Misc/FileHelper.h"
#include <fstream>
#include <iostream>
#include <math.h>

FXmlFile * UDatasetLibrary::LoadXml(FString DatasetFile) {
    return nullptr;
}

TArray<Nodo*> UDatasetLibrary::CreateNodosFromDatasetXml(FString DatasetFile) {//solo llenara con informacion relevante al data set, cosas visuales se llenan afuera
    TArray<Nodo *> Nodos;
    FXmlFile XmlSource;
    bool cargado = XmlSource.LoadFile(DatasetFile, EConstructMethod::ConstructFromFile);
    if (cargado) {
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Cargado."));
    }
    else {
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("No cargado."));
    }
    FXmlNode * rootnode = XmlSource.GetRootNode();
    //FXmlNode * rootnode = XmlSourceP->GetRootNode();
    //if (GEngine) {
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, rootnode->GetTag());
    //}
    TArray<FXmlNode *> XMLnodes = rootnode->GetChildrenNodes();
    TArray<FXmlNode*> XMLvertexs;
    for (int i = 0; i < XMLnodes.Num(); ++i) {
        if (XMLnodes[i]->GetTag() == "vertex") {
            XMLvertexs.Add(XMLnodes[i]);
        }
    }
    //obtenienod la unicaion del ulitmo nodo para centrar todo el arbol
    FXmlNode * nodexorigen = XMLvertexs[XMLvertexs.Num()-1]->FindChildNode(FString("x-coordinate"));
    FString xorigen = nodexorigen->GetAttribute("value");
    float OrigenX = FCString::Atof(*xorigen);
    FXmlNode * nodeyorigen = XMLvertexs[XMLvertexs.Num()-1]->FindChildNode(FString("y-coordinate"));
    FString yorigen = nodeyorigen->GetAttribute("value");
    float OrigenY = FCString::Atof(*yorigen) * -1;
    //tengo todos los vertices en ese array
    TArray<int> numerocolores;
    for (int i = 0; i < XMLvertexs.Num(); ++i) {
        //obteniendo el id
        FString id = XMLvertexs[i]->GetAttribute(FString("id"));//devuelve el valor del atributo que le doy, hay otra funocin que me devuelve todos los atributos en un arrya de un obejto especial//quiza deba esto guardarlo como int cuando genere la clase Vertex
        
        //obteniendo el valid
        FXmlNode * nodevalid = XMLvertexs[i]->FindChildNode(FString("valid"));
        FString valid = nodevalid->GetAttribute("value");

        //obteniendo la x-coordinate
        FXmlNode * nodex = XMLvertexs[i]->FindChildNode(FString("x-coordinate"));
        FString xcoordinate = nodex->GetAttribute("value");

        //obteniendo la y-coordinate
        FXmlNode * nodey = XMLvertexs[i]->FindChildNode(FString("y-coordinate"));
        FString ycoordinate = nodey->GetAttribute("value");

        //obteniendo url
        FXmlNode * nodeurl = XMLvertexs[i]->FindChildNode(FString("url"));
        FString url = nodeurl->GetAttribute("value");

        //obteniendo parent 
        FXmlNode * nodeparent = XMLvertexs[i]->FindChildNode(FString("parent"));//quiza no sean necesario usar FString
        FString parent = nodeparent->GetAttribute("value");

        //los hijos no estan dentro de un array por lo tanto es necesario reccorrer todos los child nose, es decir lo de aqui arruba fue por las puras, jeje
        TArray<FString> sons;
        TArray<FXmlNode*> childs = XMLvertexs[i]->GetChildrenNodes();
        for (int j = 0; j < childs.Num(); j++) {
            if (childs[j]->GetTag() == "son") {
                sons.Add(childs[j]->GetAttribute("value"));
            }
        }

        FXmlNode * nodelabels = XMLvertexs[i]->FindChildNode(FString("labels"));//quiza no sean necesario usar FString
        TArray<FXmlNode*> labelschilds = nodelabels->GetChildrenNodes();
        TArray<FString> labels;
        for (int j = 0; j < labelschilds.Num(); j++) {
            labels.Add(labelschilds[j]->GetAttribute("value"));
            //aqui faltaria definir que label es cada uno, para poder ponerlo en la variable que corresponda en el la calse vertex que creare
        }
        FXmlNode * nodescalars = XMLvertexs[i]->FindChildNode(FString("scalars"));//quiza no sean necesario usar FString
        TArray<FXmlNode*> scalarschilds = nodescalars->GetChildrenNodes();
        FString colorcdata;
        for (int j = 0; j < scalarschilds.Num(); j++) {
            if (scalarschilds[j]->GetAttribute("name") == "cdata") {
                colorcdata = scalarschilds[j]->GetAttribute("value");
            }
        }
        //el contenido de los nodos, es lo que hay en trexto plano dentro del tag de apertura y de cierre

        //TArray<FXmlNode*> childs = vertexs[i]->GetChildrenNodes();//para el caso de los vertexs sus hijos son unicos o son un array por lo tanto podria usar la funcion findchildren, para encontrar los que necesito

        //creando un objeto nodo, instanciando y llenando sus datos
        //creo los nodos, pero estos ya no se instancian

        Nodo * NodoInstanciado = new Nodo;
        NodoInstanciado->Id = FCString::Atoi(*id);
        NodoInstanciado->Valido = FCString::ToBool(*valid);
        NodoInstanciado->X = FCString::Atof(*xcoordinate);
        NodoInstanciado->Y = FCString::Atof(*ycoordinate);
        NodoInstanciado->Url = url;
        for (int j = 0; j < labels.Num(); j++) {
            NodoInstanciado->Labels.Add(labels[j]);
        }
        NodoInstanciado->PadreId = FCString::Atoi(*parent);
        for (int j = 0; j < sons.Num(); j++) {
            NodoInstanciado->HijosId[j] = FCString::Atoi(*sons[j]);//quiza deberia reemplzar por vector al de los hijos//que pasa si tuviera mas?
            NodoInstanciado->Hijos[j] = Nodos[NodoInstanciado->HijosId[j]];//para agregar la referencia, esto o se peude con el padre, por que en toeria aun no existe, habria que realizar una segunda pasada, alli podiasmos incluir esto para evtar algun fallo
        }
        NodoInstanciado->Selected = false;
        if (NodoInstanciado->Valido) {//aqui a los nodos reales se le debe asiganar algun colo de acerud a algun criterio, por ahora dejar asi
            NodoInstanciado->Color = FLinearColor::Black;
            NodoInstanciado->ColorNum = FCString::Atoi(*colorcdata);
            //UE_LOG(LogClass, Log, TEXT("Color = %d"), NodoInstanciado->ColorNum);
        }
        else {
            //NodoInstanciado->Color = ColorVirtual;//tambien debo cambiarle el tama�o
            NodoInstanciado->ColorNum = FCString::Atoi(*colorcdata);
            //NodoInstanciado->SetRadio(RadioNodosVirtuales);
            NodoInstanciado->SetRadio(0.01f);
        }
        //actualizar nodo, para cambiar el color o el tama�o si es necesario
        //NodoInstanciado->AttachRootComponentToActor(this);
        Nodos.Add(NodoInstanciado);
        //NodoInstanciado->GetRootComponent()->SetupAttachment(RootComponent);// para hacerlo hioj de la visualizaci�n, aunque parece que esto no es suficient
    }

    //a�andiendo nodos al procedular mesh
    for (int i = 0; i < Nodos.Num(); i++) {
        Nodos[i]->Padre = Nodos[Nodos[i]->PadreId];//para agregar la referencia, esto o se peude con el padre, por que en toeria aun no existe, habria que realizar una segunda pasada, alli podiasmos incluir esto para evtar algun fallo
    }
    return Nodos;
}

TArray<FElement*> UDatasetLibrary::CreateElementsFromDatasetXml(FString DatasetFile) {
    return TArray<FElement *>();
}

TArray<FElement*> UDatasetLibrary::CreateElementsFromDatasetXml(FString DatasetFile, EDataType DatasetType) {
    TArray<FElement *> Elements;
    FXmlFile XmlSource;
    bool cargado = XmlSource.LoadFile(DatasetFile, EConstructMethod::ConstructFromFile);
    if (cargado) {
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Cargado."));
    }
    else {
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("No cargado."));
    }
    FXmlNode * rootnode = XmlSource.GetRootNode();
    //FXmlNode * rootnode = XmlSourceP->GetRootNode();
    //if (GEngine) {
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, rootnode->GetTag());
    //}
    TArray<FXmlNode *> XMLnodes = rootnode->GetChildrenNodes();
    TArray<FXmlNode*> XMLvertexs;
    for (int i = 0; i < XMLnodes.Num(); ++i) {
        if (XMLnodes[i]->GetTag() == "vertex") {
            XMLvertexs.Add(XMLnodes[i]);
        }
    }
    //obtenienod la unicaion del ulitmo nodo para centrar todo el arbol
    FXmlNode * nodexorigen = XMLvertexs[XMLvertexs.Num()-1]->FindChildNode(FString("x-coordinate"));
    FString xorigen = nodexorigen->GetAttribute("value");
    float OrigenX = FCString::Atof(*xorigen);
    FXmlNode * nodeyorigen = XMLvertexs[XMLvertexs.Num()-1]->FindChildNode(FString("y-coordinate"));
    FString yorigen = nodeyorigen->GetAttribute("value");
    float OrigenY = FCString::Atof(*yorigen) * -1;
    //tengo todos los vertices en ese array
    TArray<int> numerocolores;
    for (int i = 0; i < XMLvertexs.Num(); ++i) {
        //obteniendo el id
        FString id = XMLvertexs[i]->GetAttribute(FString("id"));//devuelve el valor del atributo que le doy, hay otra funocin que me devuelve todos los atributos en un arrya de un obejto especial//quiza deba esto guardarlo como int cuando genere la clase Vertex
        
        //obteniendo el valid
        FXmlNode * nodevalid = XMLvertexs[i]->FindChildNode(FString("valid"));
        FString valid = nodevalid->GetAttribute("value");

        //obteniendo la x-coordinate
        FXmlNode * nodex = XMLvertexs[i]->FindChildNode(FString("x-coordinate"));
        FString xcoordinate = nodex->GetAttribute("value");

        //obteniendo la y-coordinate
        FXmlNode * nodey = XMLvertexs[i]->FindChildNode(FString("y-coordinate"));
        FString ycoordinate = nodey->GetAttribute("value");

        //obteniendo url
        FXmlNode * nodeurl = XMLvertexs[i]->FindChildNode(FString("url"));
        FString url = nodeurl->GetAttribute("value");

        //obteniendo parent 
        //FXmlNode * nodeparent = XMLvertexs[i]->FindChildNode(FString("parent"));//quiza no sean necesario usar FString
        //FString parent = nodeparent->GetAttribute("value");

        //los hijos no estan dentro de un array por lo tanto es necesario reccorrer todos los child nose, es decir lo de aqui arruba fue por las puras, jeje
        TArray<FString> sons;
        TArray<FXmlNode*> childs = XMLvertexs[i]->GetChildrenNodes();
        for (int j = 0; j < childs.Num(); j++) {
            if (childs[j]->GetTag() == "son") {
                sons.Add(childs[j]->GetAttribute("value"));
            }
        }

        FXmlNode * nodelabels = XMLvertexs[i]->FindChildNode(FString("labels"));//quiza no sean necesario usar FString
        TArray<FXmlNode*> labelschilds = nodelabels->GetChildrenNodes();
        TArray<FString> labels;
        for (int j = 0; j < labelschilds.Num(); j++) {
            labels.Add(labelschilds[j]->GetAttribute("value"));
            //aqui faltaria definir que label es cada uno, para poder ponerlo en la variable que corresponda en el la calse vertex que creare
        }
        FXmlNode * nodescalars = XMLvertexs[i]->FindChildNode(FString("scalars"));//quiza no sean necesario usar FString
        TArray<FXmlNode*> scalarschilds = nodescalars->GetChildrenNodes();
        FString colorcdata;
        for (int j = 0; j < scalarschilds.Num(); j++) {
            if (scalarschilds[j]->GetAttribute("name") == "cdata") {
                colorcdata = scalarschilds[j]->GetAttribute("value");
            }
        }
        //el contenido de los nodos, es lo que hay en trexto plano dentro del tag de apertura y de cierre

        //TArray<FXmlNode*> childs = vertexs[i]->GetChildrenNodes();//para el caso de los vertexs sus hijos son unicos o son un array por lo tanto podria usar la funcion findchildren, para encontrar los que necesito

        //creando un objeto nodo, instanciando y llenando sus datos
        //creo los nodos, pero estos ya no se instancian

        if (valid == "1") {
            FElement * NewElement = new FElement;
            NewElement->Id = FCString::Atoi(*id);
            NewElement->FileName = url;
            NewElement->TipoElemento = DatasetType;
            NewElement->Path = DatasetFile;//deberia ser la carpeta donde estan todos los documentos o al menos deberia tenerlo asi
            NewElement->Cluster = FCString::Atoi(*colorcdata);
            if (labels.Num()) {
                NewElement->Name = labels[0];
            }
            Elements.Add(NewElement);
        }
        //NodoInstanciado->GetRootComponent()->SetupAttachment(RootComponent);// para hacerlo hioj de la visualizaci�n, aunque parece que esto no es suficient
    }

    //a�andiendo nodos al procedular mesh
    return Elements;
}


FString UDatasetLibrary::ObtenerContenidoDocument(FString ContentFile) {
    FString contenido;
    //FString archivo("D:/UnrealProjects/NJVR/Content/Resources/cbr-ilp-ir-son/");
    //archivo += Node->Url;
    FFileHelper::LoadFileToString(contenido, *ContentFile);
    return contenido;
}

TArray<string> UDatasetLibrary::Separar(string Cadena, string Separador) {
    TArray<string> res;
    int ianterior = 0;
    for(int i = 0; i < Cadena.length(); i++){
        i = Cadena.find(Separador, i);
        if(i != -1){
            //cout << Cadena.substr(ianterior, i - ianterior) << endl;
            res.Add(Cadena.substr(ianterior, i - ianterior));
            ianterior = i+1;
        }
        else{
            //cout << Cadena.substr(ianterior) << endl;
            res.Add(Cadena.substr(ianterior));
            i = Cadena.length();
        }
    }
    return res;
}

bool UDatasetLibrary::CreateVectorsFromFile(FString DatasetFile, float ** & Vectores, TArray<FString> & NombreDimensiones) {//necesito tambien devolver las dimension
    //como debo leer el archivo?
    UE_LOG(LogClass, Log, TEXT("DatasetFile: %s"), *DatasetFile);
    fstream FileVectors;
    FileVectors.open(*DatasetFile, fstream::in);
	bool Open = FileVectors.is_open();
	if (Open) {
		string algo;
		FileVectors >> algo;
		int NumeroElements;
		FileVectors >> NumeroElements;
		int NumeroDimensiones;
		FileVectors >> NumeroDimensiones;
		string namedimensions;
		FileVectors >> namedimensions;
		TArray<string> StringDimensiones = Separar(namedimensions, ";");
		for (int i = 0; i < StringDimensiones.Num(); i++) {
			NombreDimensiones.Add(FString(StringDimensiones[i].c_str()));
		}
		Vectores = new float *[NumeroElements];
		for (int i = 0; i < NumeroElements; i++) {
			Vectores[i] = new float[NumeroDimensiones];
			for (int j = 0; j < NumeroDimensiones; j++) {
				Vectores[i][j] = 0.0f;
			}
		}
		for (int i = 0; i < NumeroElements; i++) {
			string vectorelement;
			FileVectors >> vectorelement;
			TArray<string> Data = Separar(vectorelement, ";");
			for (int j = 1; j < Data.Num()-1; j++) {
				//TArray<string> Dimension = Separar(Data[j], ":");
				//int IdDimension = stoi(Dimension[0]);
				//float Value = stof(Dimension[1]);
				//Vectores[i][IdDimension] = Value;
				float Value = stof(Data[j]);
				Vectores[i][j-1] = Value;
				//convertir el primero en numero, para saber el id de la dimension, el segunto es el valore de la dimensiones
			}
			//haver getlines, y separar por puntos y comas
			//el primero siempre sera el nombre del archivo, el resto sera dimensiones, crear todas las dimensiones inicialmente con 0
		}
		FileVectors.close();
	}
    return Open;
}

bool UDatasetLibrary::CreateVectorsAndElementsFromFile(FString DatasetFile, float **& Vectores, TArray<FString>& NombreDimensiones, TArray<FElement*>& Elements) {
    //como debo leer el archivo?
    UE_LOG(LogClass, Log, TEXT("DatasetFile: %s"), *DatasetFile);
    fstream FileVectors;
    FileVectors.open(*DatasetFile, fstream::in);
	bool Open = FileVectors.is_open();
	if (Open) {
		string algo;
		FileVectors >> algo;
		int NumeroElements;
		FileVectors >> NumeroElements;
		int NumeroDimensiones;
		FileVectors >> NumeroDimensiones;
		//string namedimensions;
		//FileVectors >> namedimensions;
		//TArray<string> StringDimensiones = Separar(namedimensions, ";");
		for (int i = 0; i < NumeroDimensiones; i++) {
			//NombreDimensiones.Add(FString(StringDimensiones[i].c_str()));
			NombreDimensiones.Add(FString(to_string(i).c_str()));
		}
		Vectores = new float *[NumeroElements];
		for (int i = 0; i < NumeroElements; i++) {
			Vectores[i] = new float[NumeroDimensiones];
			for (int j = 0; j < NumeroDimensiones; j++) {
				Vectores[i][j] = 0.0f;
			}
		}
		for (int i = 0; i < NumeroElements; i++) {
			string vectorelement;
			FileVectors >> vectorelement;
			TArray<string> Data = Separar(vectorelement, ";");
			FElement * Element = new FElement;
			Element->Id = i;
			Element->FileName = FString(Data[0].c_str());
			for (int j = 1; j < Data.Num()-1; j++) {
				//TArray<string> Dimension = Separar(Data[j], ":");
				//int IdDimension = stoi(Dimension[0]);
				//float Value = stof(Dimension[1]);
				//Vectores[i][IdDimension] = Value;
				float Value = stof(Data[j]);
				Vectores[i][j-1] = Value;
				//convertir el primero en numero, para saber el id de la dimension, el segunto es el valore de la dimensiones
			}
			Element->Cluster = int(stof(Data[Data.Num() - 1]));
			Elements.Add(Element);
		}
		FileVectors.close();
	}
    return Open;
}

bool UDatasetLibrary::CreateDistanceMatrixFromFile(FString DatasetFile, float ** & Matriz) {
    fstream FileMatrix;
    FileMatrix.open(*DatasetFile, fstream::in);
	bool Open = FileMatrix.is_open();
	if (Open) {
		int n;
		FileMatrix >> n;
		Matriz = new float * [n];
		string * nombres = new string [n];
		string * clase = new string [n];
		for(int i = 0; i < n; i++){
			FileMatrix >> nombres[i];
			Matriz[i] = new float [n];
		}
		for(int i = 0; i < n; i++){//no lo uso
			FileMatrix >> clase[i];
		}
		for(int i = 1; i < n; i++){
			for(int j = 0; j < i; j++){
				FileMatrix >> Matriz[i][j];
				Matriz[j][i] = Matriz[i][j];//esto se queita si solo quiero almacenar la matriz inferior
			}
		}
		//devuelvo matrices cuadradas, con los datos reflejados
		delete [] clase;
		delete [] nombres;
		FileMatrix.close();
	}
    return Open;
}

float ** UDatasetLibrary::ConstruirMatrizDeDistancias(float ** VectoresCaracteristicas, int NumVectores, int TamVectores) {
    int n = NumVectores;
    float ** m = new float * [n];
    for (int i = 0; i < n; i++) {
        m[i] = new float[n];
    }
    for (int i = 1; i < n; i++) {
        for (int j = 0; j < i; j++) {
            m[i][j] = DistanciaEntreVectores(VectoresCaracteristicas[i], VectoresCaracteristicas[j], TamVectores);
            m[j][i] = m[i][j];//esto se queita si solo quiero almacenar la matriz inferior
        }
    }
    return m;
}

float UDatasetLibrary::DistanciaEntreVectores(float * VectorA, float * VectorB, int TamVectores) {
    double acum = 0;
    for (int i = 0; i < TamVectores; i++) {
        acum += pow(VectorB[i] - VectorA[i], 2);
    }
    return sqrt(acum);
}

bool UDatasetLibrary::SaveDistanceMatrixInFile(FString NameFile, TArray<FString> Names, float ** Matriz) {
    fstream FileMatrix;
    FileMatrix.open(*NameFile, fstream::out);
    FileMatrix << Names.Num() << endl;
    for(int i = 0; i < Names.Num()-1; i++){
        FileMatrix << string(TCHAR_TO_UTF8(*Names[i])) << " ";
    }
    FileMatrix << *Names[Names.Num()-1] << endl;
    for(int i = 0; i < Names.Num()-1; i++){
        FileMatrix << 0 << " ";
    }
    FileMatrix << 0 << endl;

    for(int i = 1; i < Names.Num(); i++){
        for(int j = 0; j < i; j++){
            FileMatrix << Matriz[i][j] << " ";
        }
        FileMatrix << endl;
    }
    //devuelvo matrices cuadradas, con los datos reflejados
    FileMatrix.close();
    return true;
}


