// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "XmlParser.h"
#include "../NJ/Nodo.h"
#include <string>
#include <vector>
#include "DatasetLibrary.generated.h"

using namespace std;

UENUM(BlueprintType)
enum class EDataType: uint8 {
    ETextType UMETA(DisplayName = "Texto"),
    EImageType UMETA(DisplayName = "Imagen"),
    EUnknow UMETA(DisplayName = "Desconocido"),
};

USTRUCT(BlueprintType)
struct FElement {
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int Id;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString Name;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString FileName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString Path;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EDataType TipoElemento;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int Cluster;
    //deberia terner su vector de caracteristicas?
    //como asocio su contenido?, neceisto tenerlo cargado?

    //deberia tener funcion obtener contenido?
};

UCLASS()
class NJVR_API UDatasetLibrary: public UObject
{
public:

    GENERATED_BODY()

    static FXmlFile * LoadXml(FString DatasetFile);

    static TArray<Nodo *> CreateNodosFromDatasetXml(FString DatasetFile);//deberia ser

    static TArray<FElement *> CreateElementsFromDatasetXml(FString DatasetFile);//deberia ser

    static TArray<FElement *> CreateElementsFromDatasetXml(FString DatasetFile, EDataType DatasetType);//deberia ser
    //de esta funcion puedo crear documentos y nodos
    //debo tener funciones para crear lo que necesitos
    //si solo necesito crear vectores de caracteristicas, esto crea documento con sus vectores
    //matriz de distancia, solo es matriz, pero siguiendo la numeracion de los documentos o nodos.

    static FString ObtenerContenidoDocument(FString ContentFile);

    //debolvere el array de caracteristicas, de unos documentos que ya estan creados, por lo tanto no estara asociado
    //lo devuelvo como array o como vector?

    static TArray<string> Separar(string Cadena, string Separador);

    static bool CreateVectorsFromFile(FString DatasetFile, float ** & Vectores, TArray<FString> & NombreDimensiones);

    static bool CreateVectorsAndElementsFromFile(FString DatasetFile, float ** & Vectores, TArray<FString> & NombreDimensiones, TArray<FElement *> & Elements);//los elementos podrian estar asociados a su vectore de caracteristicas

    //lo leera en el formato del pex, pero sin los ;, estara separado por espacios para leer solo con cin//no se si sea la mejor opcion
    static bool CreateDistanceMatrixFromFile(FString DatasetFile, float ** & Matriz);

    static float **  ConstruirMatrizDeDistancias(float ** VectoresCaracteristicas, int NumVectores, int TamVectores);//o que devuelva 

    static float DistanciaEntreVectores(float * VectorA, float * VectorB, int TamVectores);

    static bool SaveDistanceMatrixInFile(FString NameFile, TArray<FString> Names, float ** Matriz);//el array ya tiene el tama�o

    
};
