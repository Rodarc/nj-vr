// Fill out your copyright notice in the Description page of Project Settings.


#include "ExportLibrary.h"
#include <iostream>
#include <fstream>
#include <string>

/*
Funcion para exporta en el formato para las metricas de legibilidad.
No se exportará el nombre de las clases ya que estos son enteros que van de 0 a NumGrupos - 1
*/
void UExportLibrary::ExportToMetricasFormat(string NombreArchivo, TArray<Nodo*>& Nodos, float RadioNodos, float RadioNodosVirtuales, int Dimension, int NumGrupos) {
    std::ofstream myfile("D:/AM/Arbolito/DataMetricas/" + NombreArchivo +".txt");
    if (myfile.is_open()) {
        myfile << Dimension << endl;
        myfile << Nodos.Num() << endl;
        myfile << RadioNodos << " " << RadioNodosVirtuales << endl;
        myfile << NumGrupos << endl;
        for (int i = 0; i < Nodos.Num(); i++) {
            myfile << Nodos[i]->Id << " " << Nodos[i]->Valido << " " << Nodos[i] -> ColorNum << " " << Nodos[i]->X << " " << Nodos[i]->Y << " " << Nodos[i]->Z << endl;
        }
        myfile.close();
    }    
}

