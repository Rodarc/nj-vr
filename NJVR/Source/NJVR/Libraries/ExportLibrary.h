// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "../NJ/Nodo.h"
#include "ExportLibrary.generated.h"

/**
 * 
 */

using namespace std;

UCLASS()
class NJVR_API UExportLibrary : public UObject
{
	GENERATED_BODY()
	
public:
	static void ExportToMetricasFormat(string NombreArchivo, TArray<Nodo*> & Nodos, float RadioNodos, float RadioNodosVirtuales, int Dimension, int NumGrupos);
};
