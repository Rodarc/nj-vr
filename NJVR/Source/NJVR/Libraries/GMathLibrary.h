// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GMathLibrary.generated.h"

/**
 * 
 */
UCLASS()
class NJVR_API UGMathLibrary : public UObject
{
	GENERATED_BODY()

public:

    static FMatrix MatrizTraslacion(float x, float y, float z);

    static FMatrix MatrizRotacionX(float angle);

    static FMatrix MatrizRotacionY(float angle);

    static FMatrix MatrizRotacionZ(float angle);

    static FMatrix MultiplicacionMatriz(FMatrix a, FMatrix b);

    static void ImprimirMatriz(FMatrix m);

	
};
