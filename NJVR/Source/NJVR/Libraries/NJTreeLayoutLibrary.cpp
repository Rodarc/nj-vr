// Fill out your copyright notice in the Description page of Project Settings.


#include "NJTreeLayoutLibrary.h"
#include "GMathLibrary.h"
#include "Containers/Queue.h"
#include <stack>


void UNJTreeLayoutLibrary::CalcularRadio(Nodo * V, float RadioFrameHoja, float DeltaDistanciaArista) {//parametro, 
    //rp es el Radio Frame
    if (V->Valido) {
        V->RadioFrame = RadioFrameHoja;//un poco mas del doble del diametro de los nodos (estabe en 6.0f)
        //V->RadioFrame = 1.0f;//un poco mas del doble del diametro de los nodos
        //UE_LOG(LogClass, Log, TEXT("Valid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
    else{
        CalcularRadio(V->Hijos[0], RadioFrameHoja, DeltaDistanciaArista);
        CalcularRadio(V->Hijos[1], RadioFrameHoja, DeltaDistanciaArista);
        //V->RadioFrame = FMath::Max(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 2;//funciona bien apra el conjunto peque�o
        //V->RadioFrame = FMath::Max(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 0.5;//par los otros conjuntos
        V->RadioFrame = FMath::Max(V->Hijos[0]->RadioFrame, V->Hijos[1]->RadioFrame) + DeltaDistanciaArista;//par los otros conjuntos
        //que deberia depender de la altura o numero de ivevels, y la arista mas grande que deseo obtener
        //UE_LOG(LogClass, Log, TEXT("InValid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
}

void UNJTreeLayoutLibrary::CalcularPhi(Nodo * V, float DeltaPhi) {//parametro, 
    if (V->Valido) {
        V->Phi = 0;//un poco mas del doble del diametro de los nodos (estabe en 6.0f)
        //V->RadioFrame = 1.0f;//un poco mas del doble del diametro de los nodos
        //UE_LOG(LogClass, Log, TEXT("Valid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
    else{
        CalcularPhi(V->Hijos[0], DeltaPhi);
        CalcularPhi(V->Hijos[1], DeltaPhi);
        //V->RadioFrame = FMath::Max(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 2;//funciona bien apra el conjunto peque�o
        //V->RadioFrame = FMath::Max(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 0.5;//par los otros conjuntos
        V->Phi = FMath::Max(V->Hijos[0]->Phi, V->Hijos[1]->Phi) + DeltaPhi;//par los otros conjuntos
        //que deberia depender de la altura o numero de ivevels, y la arista mas grande que deseo obtener
        //UE_LOG(LogClass, Log, TEXT("InValid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
}

void UNJTreeLayoutLibrary::CalcularRadioMin(Nodo * V, float RadioFrameHoja, float DeltaDistanciaArista) {
    //rp es el Radio Frame
    if (V->Valido) {
        V->RadioFrame = RadioFrameHoja;//un poco mas del doble del diametro de los nodos (estaba en 6.0f)
        //V->RadioFrame = 1.0f;//un poco mas del doble del diametro de los nodos
        //UE_LOG(LogClass, Log, TEXT("Valid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
    else{
        CalcularRadioMin(V->Hijos[0], RadioFrameHoja, DeltaDistanciaArista);
        CalcularRadioMin(V->Hijos[1], RadioFrameHoja, DeltaDistanciaArista);
        //V->RadioFrame = FMath::Min(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 2;//funciona bien apra el conjunto peque�o
        //V->RadioFrame = FMath::Min(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 4;//funciona bien apra el conjunto peuqe�o
        //V->RadioFrame = FMath::Min(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 0.5;
        V->RadioFrame = FMath::Min(V->Hijos[0]->RadioFrame, V->Hijos[1]->RadioFrame) + DeltaDistanciaArista;
        //UE_LOG(LogClass, Log, TEXT("InValid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
}

void UNJTreeLayoutLibrary::CalcularRadioPromedio(Nodo * V, float RadioFrameHoja, float DeltaDistanciaArista) {
    //rp es el Radio Frame
    if (V->Valido) {
        V->RadioFrame = RadioFrameHoja;//un poco mas del doble del diametro de los nodos (estaba en 6.0f)
        //V->RadioFrame = 1.0f;//un poco mas del doble del diametro de los nodos
        //UE_LOG(LogClass, Log, TEXT("Valid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
    else{
        CalcularRadioMin(V->Hijos[0], RadioFrameHoja, DeltaDistanciaArista);
        CalcularRadioMin(V->Hijos[1], RadioFrameHoja, DeltaDistanciaArista);
        //V->RadioFrame = FMath::Min(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 2;//funciona bien apra el conjunto peque�o
        //V->RadioFrame = FMath::Min(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 4;//funciona bien apra el conjunto peuqe�o
        //V->RadioFrame = FMath::Min(V->Sons[0]->RadioFrame, V->Sons[1]->RadioFrame) + 0.5;
        V->RadioFrame = (V->Hijos[0]->RadioFrame + V->Hijos[1]->RadioFrame)/2 + DeltaDistanciaArista;
        //UE_LOG(LogClass, Log, TEXT("InValid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
}


void UNJTreeLayoutLibrary::CalcularRadioHemiesfera(Nodo * V, float RadioHoja) {
    //rp es el Radio Frame
    if (V->Valido) {
        V->RadioFrame = RadioHoja;
        //UE_LOG(LogClass, Log, TEXT("Valid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
    else{
        CalcularRadioHemiesfera(V->Hijos[0], RadioHoja);
        CalcularRadioHemiesfera(V->Hijos[1], RadioHoja);
        float HAp = PI*V->Hijos[0]->RadioFrame*V->Hijos[0]->RadioFrame + PI*V->Hijos[1]->RadioFrame*V->Hijos[1]->RadioFrame;
        V->RadioFrame = FMath::Sqrt(HAp/(2*PI));
        //UE_LOG(LogClass, Log, TEXT("InValid Nodo = %d, RadioFrame %f"), V->Id, V->RadioFrame);
    }
}

void UNJTreeLayoutLibrary::LayoutBase(TArray<Nodo *> & Nodos, float RadioHojas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    float DeltaRadio = RadioHojas / Root->Altura + 1;
    CalcularRadioHemiesfera(Root->Hijos[0], RadioHojas);
    CalcularRadioHemiesfera(Root->Hijos[1], RadioHojas);
    CalcularRadioHemiesfera(Root->Padre, RadioHojas);
    float HAp = PI*Root->Hijos[0]->RadioFrame*Root->Hijos[0]->RadioFrame + PI*Root->Hijos[1]->RadioFrame*Root->Hijos[1]->RadioFrame;
    HAp += PI*Root->Padre->RadioFrame*Root->Padre->RadioFrame;
    Root->RadioFrame = FMath::Sqrt(HAp/(2*PI));
    //fin clculo radios

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames

    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = Root->RadioFrame * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionY(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = Root->RadioFrame * FMath::Cos(Root->Hijos[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionY(Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionY(2 * PI - Root->Hijos[i]->Phi);
        }
        //RotacionY = MatrizRotacionY(2 * PI - Root->Sons[i]->Phi);
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = PI / 4;
                PhiTotal += V->Hijos[i]->Phi;
            }
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->Phi = PhiTotal - V->Hijos[i]->Phi;//bastaria con asignar defrente Pi?4
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = V->RadioFrame * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionY(V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionY(2 * PI - V->Hijos[i]->Phi);
                }
                //RotacionY = MatrizRotacionY(2 * PI - V->Sons[i]->Phi);
                //RotacionX = MatrizRotacionX(2 * PI - V->Sons[i]->Phi);
                RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

//probar otros enoque para la asignacion del radio, quiza que vata de abajo hacia arriba, asi todas las hojas, tienen la arista de la misma distancia con su padre.
//corregir el erro que aparece por ejemplo con el conjunto 5, leafshapte que no posee una orientacioncorrecta.
void UNJTreeLayoutLibrary::LayoutDistanciaReducida(TArray<Nodo *> & Nodos, float RadioHojas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    //float DeltaRadio = RadioHoja / (Root->Altura + 1);
    float DeltaRadio = RadioHojas / Root->Altura + 1;
    CalcularRadioHemiesfera(Root->Hijos[0], RadioHojas);
    CalcularRadioHemiesfera(Root->Hijos[1], RadioHojas);
    CalcularRadioHemiesfera(Root->Padre, RadioHojas);
    float HAp = PI*Root->Hijos[0]->RadioFrame*Root->Hijos[0]->RadioFrame + PI*Root->Hijos[1]->RadioFrame*Root->Hijos[1]->RadioFrame;
    HAp += PI*Root->Padre->RadioFrame*Root->Padre->RadioFrame;
    Root->RadioFrame = FMath::Sqrt(HAp/(2*PI));

    Root->RadioFrame = RadioHojas - DeltaRadio*Root->Nivel;
    //fin clculo radios

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames

    Root->Padre->RadioFrame = RadioHojas - DeltaRadio*Root->Padre->Nivel;

    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = Root->RadioFrame * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionY(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {

        Root->Hijos[i]->RadioFrame = RadioHojas - DeltaRadio*Root->Hijos[i]->Nivel;

        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = Root->RadioFrame * FMath::Cos(Root->Hijos[i]->Phi);
        //RotacionY = MatrizRotacionY(2 * PI - Root->Sons[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionY(Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionY(2 * PI - Root->Hijos[i]->Phi);
        }
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = PI / 4;
                PhiTotal += V->Hijos[i]->Phi;
            }
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {

                V->Hijos[i]->RadioFrame = RadioHojas - DeltaRadio*V->Hijos[i]->Nivel;

                V->Hijos[i]->Phi = PhiTotal - V->Hijos[i]->Phi;//bastaria con asignar defrente Pi?4
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = V->RadioFrame * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionY(V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionY(2 * PI - V->Hijos[i]->Phi);
                }
                //RotacionY = MatrizRotacionY(2 * PI - V->Sons[i]->Phi);
                //RotacionX = MatrizRotacionX(2 * PI - V->Sons[i]->Phi);
                RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout con Distancia Aumentada

Layout basado la implementacion de Walrus para casos especiales del H3, sin aplicarle la distorcion del espacio hiperbolico.

En este layout el radio de los frames aumenta respecto al mayor radio de frame de sus hijos en un determinado delta.
*/
void UNJTreeLayoutLibrary::LayoutDistanciaAumentada(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    //float DeltaRadio = RadioHoja / (Root->Altura + 1);
    CalcularRadio(Root->Hijos[0], RadioHojas, DeltaDistanciaAristas);
    CalcularRadio(Root->Hijos[1], RadioHojas, DeltaDistanciaAristas);
    CalcularRadio(Root->Padre, RadioHojas, DeltaDistanciaAristas);
    Root->RadioFrame = FMath::Max3(Root->Hijos[0]->RadioFrame, Root->Hijos[1]->RadioFrame, Root->Padre->RadioFrame) + 2.0f;
    //Root->RadioFrame = FMath::Max3(Root->Sons[0]->RadioFrame, Root->Sons[1]->RadioFrame, Root->Parent->RadioFrame) + DeltaDistanciaArista;
    //fin clculo radios

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames
    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = Root->RadioFrame * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionY(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = Root->RadioFrame * FMath::Cos(Root->Hijos[i]->Phi);
        //RotacionY = MatrizRotacionY(2 * PI - Root->Sons[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionY(Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionY(2 * PI - Root->Hijos[i]->Phi);
        }
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = PI / 4;
                PhiTotal += V->Hijos[i]->Phi;
            }
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->Phi = PhiTotal - V->Hijos[i]->Phi;//bastaria con asignar defrente Pi?4
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = V->RadioFrame * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionY(V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionY(2 * PI - V->Hijos[i]->Phi);
                }
                //RotacionY = MatrizRotacionY(2 * PI - V->Sons[i]->Phi);
                //RotacionX = MatrizRotacionX(2 * PI - V->Sons[i]->Phi);
                RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout con Distancia Aumentada con Angulo reducido

Layout que modifica el Layout con Distancia aumentada.

En este layout el angulo Phi que indica la separacion de los hijos respecto el eje central del padre se reduce mientras mas profundo es el nivel nodo en el arbol.
*/
void UNJTreeLayoutLibrary::LayoutDistanciaAumentadaAnguloReducido(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    float DeltaPhi = PI/4 / (Root->Altura + 1);
    CalcularRadio(Root->Hijos[0], RadioHojas, DeltaDistanciaAristas);
    CalcularRadio(Root->Hijos[1], RadioHojas, DeltaDistanciaAristas);
    CalcularRadio(Root->Padre, RadioHojas, DeltaDistanciaAristas);
    Root->RadioFrame = FMath::Max3(Root->Hijos[0]->RadioFrame, Root->Hijos[1]->RadioFrame, Root->Padre->RadioFrame) + 2.0f;
    //Root->RadioFrame = FMath::Max3(Root->Sons[0]->RadioFrame, Root->Sons[1]->RadioFrame, Root->Parent->RadioFrame) + DeltaDistanciaArista;
    //fin clculo radios

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames
    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = Root->RadioFrame * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionX(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = Root->RadioFrame * FMath::Cos(Root->Hijos[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionX(Root->Hijos[i]->Phi);
        }
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = PI / 4 - DeltaPhi*V->Nivel;
                PhiTotal += V->Hijos[i]->Phi;
            }
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->Phi = PI/4 - DeltaPhi*V->Nivel;//bastaria con asignar defrente Pi?4
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = V->RadioFrame * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionX(V->Hijos[i]->Phi);
                }
                //la matriz de rotacion Y en realidad hace rotar segun el eje X, y la matri de rotacion X hace rotar en el eje Y
                RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                //V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout con Distancia Aumentada Promedio con Angulo reducido por altura

Layout que modifica el Layout con Distancia aumentada Promedio con angulo reducido por altura

En este layout el angulo Phi que indica la separacion de los hijos respecto el eje central del padre se determina seguna la altura del nodo.
Mejorar cual sera el phi minimo, en el nivel mas bajo, para asi abrir un poco las ramitas
*/
void UNJTreeLayoutLibrary::LayoutDistanciaAumentadaPromedioAnguloReducidoPorAltura(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    float DeltaPhi = PI/4 / (Root->Altura + 1);
    CalcularRadioPromedio(Root->Hijos[0], RadioHojas, DeltaDistanciaAristas);
    CalcularRadioPromedio(Root->Hijos[1], RadioHojas, DeltaDistanciaAristas);
    CalcularRadioPromedio(Root->Padre, RadioHojas, DeltaDistanciaAristas);
    Root->RadioFrame = FMath::Max3(Root->Hijos[0]->RadioFrame, Root->Hijos[1]->RadioFrame, Root->Padre->RadioFrame) + 2.0f;
    //Root->RadioFrame = FMath::Max3(Root->Sons[0]->RadioFrame, Root->Sons[1]->RadioFrame, Root->Parent->RadioFrame) + DeltaDistanciaArista;
    //fin clculo radios
    //CalcularPhi(Root->Hijos[0], DeltaPhi);
    //CalcularPhi(Root->Hijos[1], DeltaPhi);
    //CalcularPhi(Root->Padre, DeltaPhi);

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames
    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = Root->RadioFrame * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionX(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = Root->RadioFrame * FMath::Cos(Root->Hijos[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionX(Root->Hijos[i]->Phi);
        }
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = DeltaPhi*V->Altura;
                PhiTotal += V->Hijos[i]->Phi;
            }
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->Phi = DeltaPhi*V->Altura;//bastaria con asignar defrente Pi?4
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = V->RadioFrame * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionX(V->Hijos[i]->Phi);
                }
                //la matriz de rotacion Y en realidad hace rotar segun el eje X, y la matri de rotacion X hace rotar en el eje Y
                RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                //V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout con Distancia Aumentada Promedio con Angulo reducido por altura independiente por hijo

Layout que modifica el Layout con Distancia aumentada Promedio con angulo reducido por altura independiente para cada hijo

En este layout el angulo Phi que indica la separacion de los hijos respecto el eje central del padre se determina seguna la altura del nodo.
La separacino del hijo se calcula de forma independiente para cada uno, en base a la altrua del mismo.
Mejorar cual sera el phi minimo, en el nivel mas bajo, para asi abrir un poco las ramitas
*/
void UNJTreeLayoutLibrary::LayoutDistanciaAumentadaPromedioAnguloReducidoPorAlturaIndependienteHijo(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    float MinPhi = PI / 16; //con valor 0 queda muy bien, pero los nodos quedan muy juntos
    float DeltaPhi = (PI/4 - MinPhi) / (Root->Altura + 1);
    CalcularRadioPromedio(Root->Hijos[0], RadioHojas, DeltaDistanciaAristas);
    CalcularRadioPromedio(Root->Hijos[1], RadioHojas, DeltaDistanciaAristas);
    CalcularRadioPromedio(Root->Padre, RadioHojas, DeltaDistanciaAristas);
    Root->RadioFrame = FMath::Max3(Root->Hijos[0]->RadioFrame, Root->Hijos[1]->RadioFrame, Root->Padre->RadioFrame) + 2.0f;
    //Root->RadioFrame = FMath::Max3(Root->Sons[0]->RadioFrame, Root->Sons[1]->RadioFrame, Root->Parent->RadioFrame) + DeltaDistanciaArista;
    //fin clculo radios
    //CalcularPhi(Root->Hijos[0], DeltaPhi);
    //CalcularPhi(Root->Hijos[1], DeltaPhi);
    //CalcularPhi(Root->Padre, DeltaPhi);

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames
    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = Root->RadioFrame * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionX(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = Root->RadioFrame * FMath::Cos(Root->Hijos[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionX(Root->Hijos[i]->Phi);
        }
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = DeltaPhi*(V->Hijos[i]->Altura + 1) + MinPhi;
                PhiTotal += V->Hijos[i]->Phi;
            }
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->Phi = DeltaPhi*(V->Hijos[i]->Altura + 1) + MinPhi;// + 1 para que las hojas no esten juntas ya que su altura seria 0
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = V->RadioFrame * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionX(V->Hijos[i]->Phi);
                }
                //la matriz de rotacion Y en realidad hace rotar segun el eje X, y la matri de rotacion X hace rotar en el eje Y
                RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                //V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout con Distancia Aumentada Promedio con Angulo reducido

Layout que modifica el Layout con Distancia aumentada con angulo reducido.

Calcula el radio frame usando el primedio entre los hijos, tiene un efecto similar a usar el radio min, pero da un poco mas de espacio en teoria, y se balancea segun la situacion
*/
void UNJTreeLayoutLibrary::LayoutDistanciaAumentadaPromedioAnguloReducido(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    float DeltaPhi = PI/4 / (Root->Altura + 1);
    CalcularRadioPromedio(Root->Hijos[0], RadioHojas, DeltaDistanciaAristas);
    CalcularRadioPromedio(Root->Hijos[1], RadioHojas, DeltaDistanciaAristas);
    CalcularRadioPromedio(Root->Padre, RadioHojas, DeltaDistanciaAristas);
    Root->RadioFrame = FMath::Max3(Root->Hijos[0]->RadioFrame, Root->Hijos[1]->RadioFrame, Root->Padre->RadioFrame) + 2.0f;
    //Root->RadioFrame = FMath::Max3(Root->Sons[0]->RadioFrame, Root->Sons[1]->RadioFrame, Root->Parent->RadioFrame) + DeltaDistanciaArista;
    //fin clculo radios

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames
    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = Root->RadioFrame * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionX(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = Root->RadioFrame * FMath::Cos(Root->Hijos[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionX(Root->Hijos[i]->Phi);
        }
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = PI / 4 - DeltaPhi*V->Nivel;
                PhiTotal += V->Hijos[i]->Phi;
            }
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->Phi = PI/4 - DeltaPhi*V->Nivel;//bastaria con asignar defrente Pi?4
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = V->RadioFrame * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionX(V->Hijos[i]->Phi);
                }
                //la matriz de rotacion Y en realidad hace rotar segun el eje X, y la matri de rotacion X hace rotar en el eje Y
                RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                //V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout con Distancia Aumentada Progresiva con Angulo reducido

Layout que modifica el Layout con Distancia aumentada con angulo reducido.

Este layout pone a los a los hijos de los nodos a la distancia necesaria en funcion de cada radio frame de su hijo, los radio frames siempre son el maxiom de los hijos,
pero cada hijo se pone a la distancia necesaria respecto a su padre. es dencir estan siempre dentro del raio fram del padre, siendo el radio frame el maximo entre los hijos.

la caracteristica progresiva puede ser interesante para que ocupen lo necesario, algo similar a cuando haga el angulo progresivo
esta proresion a ivel de radio se puede usar en otros layouts
*/
void UNJTreeLayoutLibrary::LayoutDistanciaAumentadaProgresivaAnguloReducido(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    float DeltaPhi = PI/4 / (Root->Altura + 1);
    CalcularRadio(Root->Hijos[0], RadioHojas, DeltaDistanciaAristas);
    CalcularRadio(Root->Hijos[1], RadioHojas, DeltaDistanciaAristas);
    CalcularRadio(Root->Padre, RadioHojas, DeltaDistanciaAristas);
    Root->RadioFrame = FMath::Max3(Root->Hijos[0]->RadioFrame, Root->Hijos[1]->RadioFrame, Root->Padre->RadioFrame) + DeltaDistanciaAristas;
    //Root->RadioFrame = FMath::Max3(Root->Sons[0]->RadioFrame, Root->Sons[1]->RadioFrame, Root->Parent->RadioFrame) + DeltaDistanciaArista;
    //fin clculo radios

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames
    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = (Root->Padre->RadioFrame + DeltaDistanciaAristas) * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = (Root->Padre->RadioFrame + DeltaDistanciaAristas) * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = (Root->Padre->RadioFrame + DeltaDistanciaAristas) * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionX(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = (Root->Hijos[i]->RadioFrame + DeltaDistanciaAristas) * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = (Root->Hijos[i]->RadioFrame + DeltaDistanciaAristas) * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = (Root->Hijos[i]->RadioFrame + DeltaDistanciaAristas) * FMath::Cos(Root->Hijos[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionX(Root->Hijos[i]->Phi);
        }
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = PI / 4 - DeltaPhi*V->Nivel;
                PhiTotal += V->Hijos[i]->Phi;
            }
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->Phi = PI/4 - DeltaPhi*V->Nivel;//bastaria con asignar defrente Pi?4
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = (V->Hijos[i]->RadioFrame + DeltaDistanciaAristas) * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = (V->Hijos[i]->RadioFrame + DeltaDistanciaAristas) * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = (V->Hijos[i]->RadioFrame + DeltaDistanciaAristas) * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionX(V->Hijos[i]->Phi);
                }
                //la matriz de rotacion Y en realidad hace rotar segun el eje X, y la matri de rotacion X hace rotar en el eje Y
                RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                //V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout con Distancia Aumentada Hijo con Angulo reducido

Layout que modifica el Layout con Distancia aumentada con angulo reduco.

En este layout se usar el radio minimo entre los hijos.
*/
void UNJTreeLayoutLibrary::LayoutDistanciaAumentadaHijoAnguloReducido(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    float DeltaPhi = PI/4 / (Root->Altura + 1);
    /*CalcularRadio(Root->Sons[0]);
    CalcularRadio(Root->Sons[1]);
    CalcularRadio(Root->Parent);*/
    CalcularRadioMin(Root->Hijos[0], RadioHojas, DeltaDistanciaAristas);
    CalcularRadioMin(Root->Hijos[1], RadioHojas, DeltaDistanciaAristas);
    CalcularRadioMin(Root->Padre, RadioHojas, DeltaDistanciaAristas);
    Root->RadioFrame = FMath::Max3(Root->Hijos[0]->RadioFrame, Root->Hijos[1]->RadioFrame, Root->Padre->RadioFrame) + DeltaDistanciaAristas;
    //Root->RadioFrame = FMath::Max3(Root->Sons[0]->RadioFrame, Root->Sons[1]->RadioFrame, Root->Parent->RadioFrame) + DeltaDistanciaArista;
    //fin clculo radios

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames
    //SI se usa el radio de los hijos en lugar del nodo actual, se producen resultados interesantes
    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = Root->RadioFrame * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionY(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = Root->RadioFrame * FMath::Cos(Root->Hijos[i]->Phi);
        //RotacionY = MatrizRotacionY(2 * PI - Root->Sons[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionY(Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionY(2 * PI - Root->Hijos[i]->Phi);
        }
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = PI / 4 - DeltaPhi*V->Nivel;
                PhiTotal += V->Hijos[i]->Phi;
            }
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->Phi = PI/4 - DeltaPhi*V->Nivel;//bastaria con asignar defrente Pi?4
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = V->RadioFrame * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionY(V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionY(2 * PI - V->Hijos[i]->Phi);
                }
                //RotacionY = MatrizRotacionY(2 * PI - V->Sons[i]->Phi);
                //RotacionX = MatrizRotacionX(2 * PI - V->Sons[i]->Phi);
                RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout con Distancia Aumentada con Angulo reducido sin Intercalamiento de orientacion

Layout que modifica el Layout con Distancia aumentada con angulo reduco.

En este layout no intercala la orientacion entre niveles si es que el uno de los hijos es un nodo real.
*/
void UNJTreeLayoutLibrary::LayoutDistanciaAumentadaAnguloReducidoSinIntercalamiento(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas) {
    TQueue<Nodo *> Cola;
    //Calculos2();
    //Calc();//no estaba antes
    Nodo * Root = Nodos[Nodos.Num() - 1];

    //calculamos los radios
    float DeltaPhi = PI/4 / (Root->Altura + 1);
    CalcularRadio(Root->Hijos[0], RadioHojas, DeltaDistanciaAristas);
    CalcularRadio(Root->Hijos[1], RadioHojas, DeltaDistanciaAristas);
    CalcularRadio(Root->Padre, RadioHojas, DeltaDistanciaAristas);
    Root->RadioFrame = FMath::Max3(Root->Hijos[0]->RadioFrame, Root->Hijos[1]->RadioFrame, Root->Padre->RadioFrame) + 2.0f;
    //Root->RadioFrame = FMath::Max3(Root->Sons[0]->RadioFrame, Root->Sons[1]->RadioFrame, Root->Parent->RadioFrame) + DeltaDistanciaArista;
    //fin clculo radios

    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = 0.0f;//esta es la posicion general dentro de la visualizacion, 
    Root->Y = 0.0f;
    Root->Z = 0.0f;
    Root->XRelative = 0.0f;//esta es la posicion relativa respecto al padre
    Root->YRelative = 0.0f;
    Root->ZRelative = 0.0f;
    UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->X, Root->Y, Root->Z);

    FMatrix RotacionY;
    FMatrix RotacionZ;
    FMatrix TraslacionV;
    //por ahora los tres primeros se dividiran de forma equitativa, pero despues ya no, sera en base a sus proporciones, 
    //sa debe realizar el calculo de los frames
    Root->Padre->Phi = 0;
    Root->Padre->Theta = 0;
    Root->Padre->XRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->YRelative = Root->RadioFrame * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->ZRelative = Root->RadioFrame * FMath::Cos(Root->Padre->Phi);
    RotacionY = UGMathLibrary::MatrizRotacionX(Root->Padre->Phi);//3*PI/2 siempre sera este valor
    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
    TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Padre->XRelative, Root->Padre->YRelative, Root->Padre->ZRelative);
    Root->Padre->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
    Root->Padre->X = Root->Padre->Frame.M[0][3];
    Root->Padre->Y = Root->Padre->Frame.M[1][3];
    Root->Padre->Z = Root->Padre->Frame.M[2][3];
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Root->Hijos[i]->Phi = 2*PI/3;
        Root->Hijos[i]->Theta = (i & 1) * PI;
        Root->Hijos[i]->XRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->YRelative = Root->RadioFrame * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->ZRelative = Root->RadioFrame * FMath::Cos(Root->Hijos[i]->Phi);
        if (i & 1) {
            RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - Root->Hijos[i]->Phi);
        }
        else {
            RotacionY = UGMathLibrary::MatrizRotacionX(Root->Hijos[i]->Phi);
        }
        RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
        TraslacionV = UGMathLibrary::MatrizTraslacion(Root->Hijos[i]->XRelative, Root->Hijos[i]->YRelative, Root->Hijos[i]->ZRelative);
        Root->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ);
        Root->Hijos[i]->X = Root->Hijos[i]->Frame.M[0][3];
        Root->Hijos[i]->Y = Root->Hijos[i]->Frame.M[1][3];
        Root->Hijos[i]->Z = Root->Hijos[i]->Frame.M[2][3];
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo* V;
        Cola.Dequeue(V);
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, RadioFrame: %f"), V->Id, V->RadioFrame);
        float PhiTotal = 0.0f;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                //V->Sons[i]->Phi = FMath::Atan(V->Sons[i]->RadioFrame / V->RadioFrame);
                V->Hijos[i]->Phi = PI / 4 - DeltaPhi * V->Nivel;
                PhiTotal += V->Hijos[i]->Phi;
            }
            bool HasValidSons = V->Hijos[0]->Valido || V->Hijos[1]->Valido;
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->Phi = PI/4 - DeltaPhi*V->Nivel;//bastaria con asignar defrente Pi?4
                V->Hijos[i]->Theta = (i & 1) * PI;// +(V->Nivel & 1) * (PI / 2);//si es el primer hijo, le toca Theta 0, si es el segundo le toca Theta PI, y a ello dependeindo del nivel se le agrega La variacion de theta
                V->Hijos[i]->XRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->YRelative = V->RadioFrame * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->ZRelative = V->RadioFrame * FMath::Cos(V->Hijos[i]->Phi);
                if (i & 1) {
                    RotacionY = UGMathLibrary::MatrizRotacionX(2 * PI - V->Hijos[i]->Phi);
                }
                else {
                    RotacionY = UGMathLibrary::MatrizRotacionX(V->Hijos[i]->Phi);
                }
                //RotacionY = MatrizRotacionY(2 * PI - V->Sons[i]->Phi);
                //RotacionX = MatrizRotacionX(2 * PI - V->Sons[i]->Phi);
                if (HasValidSons) {
                    //RotacionZ = UGMathLibrary::MatrizRotacionZ(0); produde ramas arqueadas.
                    RotacionZ = UGMathLibrary::MatrizRotacionZ(PI);//con PI / 4 Sale algo interesante
                }
                else {
                    RotacionZ = UGMathLibrary::MatrizRotacionZ(2 * PI - PI / 2);
                }
                /*if (V->Nivel & 1) {
                    RotacionZ = MatrizRotacionZ(PI / 2);
                }
                else {
                    RotacionZ = MatrizRotacionZ(2 * PI - PI / 2);
                }*/
                TraslacionV = UGMathLibrary::MatrizTraslacion(V->Hijos[i]->XRelative, V->Hijos[i]->YRelative, V->Hijos[i]->ZRelative);
                /*FMatrix res = TraslacionV;
                res = UGMathLibrary::MultiplicacionMatriz(res, RotacionY);
                if (HasValidSons) {
                    res = UGMathLibrary::MultiplicacionMatriz(res, RotacionZ);
                }
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, res);*/
                //V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionZ), RotacionY));
                V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(TraslacionV, RotacionY), RotacionZ));
                //V->Hijos[i]->Frame = UGMathLibrary::MultiplicacionMatriz(V->Frame, UGMathLibrary::MultiplicacionMatriz(UGMathLibrary::MultiplicacionMatriz(RotacionZ, RotacionY), TraslacionV));
                //V->Sons[i]->Frame = MultiplicacionMatriz(MultiplicacionMatriz(TraslacionV, RotacionX), RotacionZ);
                V->Hijos[i]->X = V->Hijos[i]->Frame.M[0][3];
                V->Hijos[i]->Y = V->Hijos[i]->Frame.M[1][3];
                V->Hijos[i]->Z = V->Hijos[i]->Frame.M[2][3];
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}


/*
Layout Mesa

Layout escogido para distribuuir los nodos sobre la mesa, y que sea el punto de partida para la animacino de construccion del arbol
*/
void UNJTreeLayoutLibrary::LayoutMesa(TArray<Nodo *> & Nodos, float RadioNodos, FVector Offset) {//si offset el layout se calculara con centro en 0, 0, 0
    float MesaRadio = 0;
    //float Circunferencia = 2 * PI * MesaRadio;
    //ubicar un nodo alli

    int NumeroNodosReales = Nodos.Num()/2 + 1;//el primer virtual
    int k = 0;
    //ubicar el primer nodo real

    //FVector Offset = GetActorLocation() * -1;
    //Offset = GetTransform().InverseTransformVector(Offset);
    Nodos[k]->X = Offset.X;
    Nodos[k]->Y = Offset.Y;
    Nodos[k]->Z = Offset.Z + RadioNodos;

    k++;
    while (k < NumeroNodosReales) {//solo los nodos reales, los virtales deberian ir debajo de la mesa, o en el techo o en la espalda, debajo de la mesa, e inhabilitada la seleccino o manipulacon de estos, es decir no debo tenerlos en cunenta en la deteccion de coliciones
        MesaRadio += 2*RadioNodos;
        float Circunferencia = 2 * PI * MesaRadio;
        int CantidadNodos = Circunferencia / (2 * RadioNodos);
        float DeltaAngle = 2 * PI / CantidadNodos;
        for (int i = 0; i < CantidadNodos && k < NumeroNodosReales; i++, k++) {
            //layout para la circunferencia de turno
            //en la posicion 0
            Nodos[k]->X = FMath::Cos(DeltaAngle * i)*MesaRadio + Offset.X;
            Nodos[k]->Y = FMath::Sin(DeltaAngle * i)*MesaRadio + Offset.Y;
            Nodos[k]->Z = Offset.Z + RadioNodos;
        }
    }
    MesaRadio = 0;
    //ubicar el primer nodo virtual
    Nodos[k]->X = Offset.X;
    Nodos[k]->Y = Offset.Y;
    Nodos[k]->Z = Offset.Z - RadioNodos;
    k++;
    while (k < Nodos.Num()) {
        MesaRadio += 2*RadioNodos;
        float Circunferencia = 2 * PI * MesaRadio;
        int CantidadNodos = Circunferencia / (2 * RadioNodos);
        float DeltaAngle = 2 * PI / CantidadNodos;
        for (int i = 0; i < CantidadNodos && k < Nodos.Num(); i++, k++) {
            //layout para la circunferencia de turno
            //en la posicion 0
            Nodos[k]->X = FMath::Cos(DeltaAngle * i)*MesaRadio + Offset.X;
            Nodos[k]->Y = FMath::Sin(DeltaAngle * i)*MesaRadio + Offset.Y;
            Nodos[k]->Z = Offset.Z - RadioNodos;
        }
    }

    //itero y asigno esa cantidad
}

/*
Layout Esf�rico

Layout que acomoda el arbol alrededor sobre la superficie de una esfera, tenieno como raiz el extremo superior de la esfera y el arbol cayendo desde alli hacia la parte inferior de la esfera.
Las ramas se extienden hsta el extremo inferior de la esfera.
Se pueden establecer una franja sobre la que estaran dispuestos los nodos.
*/
void UNJTreeLayoutLibrary::LayoutEsferico(TArray<Nodo *> & Nodos, float RadioEsfera, float PhiMin = 0, float PhiMax = PI) {//en este algoritmo puedo asignar el nivel
    TQueue<Nodo *> Cola;
    //la raiz es el ultimo nodo
    Nodo * Root = Nodos[Nodos.Num() - 1];
    float DeltaPhi = (PhiMax-PhiMin) / (Root->Altura+1);
    UE_LOG(LogClass, Log, TEXT("DeltaPhi = %f"), DeltaPhi);
    //agregado para el nuevo radio
    //int NivelDenso, CantidadNodosNivelDenso;
    //NivelMasDenso(NivelDenso, CantidadNodosNivelDenso);
    //NewRadio = EncontrarRadio1(DeltaPhi * NivelDenso, CantidadNodosNivelDenso); 

    //Radio = EncontrarRadio2(DeltaPhi * Root->Altura); //este es el que estoy usando, no recibo radio por la funcion
    
    Root->Theta = 0;
    Root->Phi = 0;
    Root->WTam = 2*PI;
    Root->WInicio = 0;
    Root->X = RadioEsfera * FMath::Sin(Root->Phi) * FMath::Cos(Root->Theta);
    Root->Y = RadioEsfera * FMath::Sin(Root->Phi) * FMath::Sin(Root->Theta);
    Root->Z = RadioEsfera * FMath::Cos(Root->Phi);
    //UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->Xcoordinate, Root->Ycoordinate, Root->Zcoordinate);
    //float DeltaPhi = PI / Root->Altura;
    float WTemp = Root->WInicio;
    //debo tener en cuenta al padre para hacer los calculos, ya que esto esta como arbol sin raiz

    //Root->Parent->Phi = Root->Phi + DeltaPhi;//estaba dividido /2
    Root->Padre->Phi = PhiMin;//estaba dividido /2
    Root->Padre->WTam = Root->WTam * (float(Root->Padre->Hojas) / Root->Hojas);
    Root->Padre->WInicio = WTemp;
    Root->Padre->Theta = WTemp + Root->Padre->WTam / 2;
    Root->Padre->X = RadioEsfera * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta);
    Root->Padre->Y = RadioEsfera * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta);
    Root->Padre->Z = RadioEsfera * FMath::Cos(Root->Padre->Phi);
    WTemp += Root->Padre->WTam;
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->.Num()*/; i++) {
        //Root->Sons[i]->Phi = Root->Phi + DeltaPhi;//estaba dividido por 2
        Root->Hijos[i]->Phi = PhiMin;
        Root->Hijos[i]->WTam = Root->WTam * (float(Root->Hijos[i]->Hojas) / Root->Hojas);
        Root->Hijos[i]->WInicio = WTemp;
        Root->Hijos[i]->Theta = WTemp + Root->Hijos[i]->WTam / 2;
        Root->Hijos[i]->X = RadioEsfera * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->Y = RadioEsfera * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->Z = RadioEsfera * FMath::Cos(Root->Hijos[i]->Phi);
        WTemp += Root->Hijos[i]->WTam;
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        WTemp = V->WInicio;
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {//debo verificar que tenga hijos o no
                V->Hijos[i]->Phi = V->Phi + DeltaPhi;
                V->Hijos[i]->WTam = V->WTam * (float(V->Hijos[i]->Hojas) / V->Hojas);
                V->Hijos[i]->WInicio = WTemp;
                V->Hijos[i]->Theta = WTemp + V->Hijos[i]->WTam / 2;
                V->Hijos[i]->X = RadioEsfera * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->Y = RadioEsfera * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->Z = RadioEsfera * FMath::Cos(V->Hijos[i]->Phi);
                WTemp += V->Hijos[i]->WTam;
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout Esf�rico de Ladrillos

Similar al Layout esferico, solo que el espacio esta dividio en secciones rectangulares (ladrillos), la cantidad de secciones a nivel horizonatal esta determinado por el numero de nodos reales,
cada nivel esta girado medio ladrillo respecto al anterior nivel.
Los nodos hoja son asigandas a un ladrillo en su determinado nivel, y el nodo padre asignado en ladrillo central entre los ladrillos de sus hijos.
*/
void UNJTreeLayoutLibrary::LayoutEsfericoLadrillos(TArray<Nodo*>& Nodos, float RadioEsfera, float PhiMin, float PhiMax) {
    //Ladrillos
    TQueue<Nodo *> Cola;
    std::stack<Nodo *> pila;
    //la raiz es el ultimo nodo
    Nodo * Root = Nodos[Nodos.Num() - 1];
    int hojas = Root->Hojas;//el numero de hojas, las tiene la raiz
    int nivelMax = Root->Altura;//podria salir de la altura, como nivel maximo, 
    //Calculos2(hojas, nivelMax);//esto me preocupa
    //esos datos creo que se pueden sacar de la raiz del arbol
    Root->Theta = 0;
    Root->Phi = 0;
    Root->X = RadioEsfera * FMath::Sin(Root->Phi) * FMath::Cos(Root->Theta);
    Root->Y = RadioEsfera * FMath::Sin(Root->Phi) * FMath::Sin(Root->Theta);
    Root->Z = RadioEsfera * FMath::Cos(Root->Phi);
    //UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->Xcoordinate, Root->Ycoordinate, Root->Zcoordinate);
    //float DeltaPhi = PI / nivelMax;
    float DeltaPhi = (PhiMax - PhiMin) / nivelMax;
    //float DeltaPhi = PI / nivelMax;
    float DeltaTheta = 2 * PI / hojas;
    UE_LOG(LogClass, Log, TEXT("DeltaPhi = %f"), DeltaPhi);
    Cola.Enqueue(Root->Hijos[0]);
    pila.push(Root->Hijos[0]);
    Cola.Enqueue(Root->Hijos[1]);
    pila.push(Root->Hijos[1]);
    Cola.Enqueue(Root->Padre);
    pila.push(Root->Padre);

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        if (V->Hijos[0]) {//o usar el bool valido
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                Cola.Enqueue(V->Hijos[i]);
                pila.push(V->Hijos[i]);
            }
        }
    }

    while (!pila.empty()) {
        Nodo * V = pila.top();
        pila.pop();
        if (V->Hijos[0]) {
            V->Casilla = (V->Hijos[0]->Casilla + V->Hijos[1]->Casilla) / 2;
            V->Casilla += V->Nivel & 1;
            //V->Phi = V->Nivel * DeltaPhi;
            V->Phi = (V->Nivel-1) * DeltaPhi + PhiMin;
            V->Theta = V->Casilla * DeltaTheta + DeltaTheta / 2 * !(V->Nivel & 1);
        }
        else{
            //V->Phi = V->Nivel * DeltaPhi;
            V->Phi = (V->Nivel-1) * DeltaPhi + PhiMin;
            //V->Phi = nivelMax * DeltaPhi;//para alinear las hojas al ultimo nivel
            V->Theta = V->Casilla * DeltaTheta + DeltaTheta / 2 * !(V->Nivel & 1);
            //V->Theta = V->Casilla * DeltaTheta + DeltaTheta / 2 * !(nivelMax & 1);
        }
        V->X = RadioEsfera * FMath::Sin(V->Phi) * FMath::Cos(V->Theta);
        V->Y = RadioEsfera * FMath::Sin(V->Phi) * FMath::Sin(V->Theta);
        V->Z = RadioEsfera * FMath::Cos(V->Phi);
    }
}


/*
Layout Radial 2D

Layout radial para arboles filogeneticos, el usado por el NJTree.
*/
void UNJTreeLayoutLibrary::LayoutRadial2D(TArray<Nodo*>& Nodos, float DistanciaArista) {
    //parece que solo necesito la cantidad de hojas, pero siempre es mejor tener todos los datos
    TQueue<Nodo *> Cola;
    //la raiz es el ultimo nodo
    Nodo * Root = Nodos[Nodos.Num() - 1];
    //agregado para el nuevo radio
    //int NivelDenso, CantidadNodosNivelDenso;
    //NivelMasDenso(NivelDenso, CantidadNodosNivelDenso);
    //NewRadio = EncontrarRadio1(DeltaPhi * NivelDenso, CantidadNodosNivelDenso); 
    //
    Root->Theta = 0;
    Root->Phi = 0;
    Root->WTam = 2*PI;
    Root->WInicio = 0;
    Root->X = 0;
    Root->Y = 0;
    Root->Z = 0;
    //UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->Xcoordinate, Root->Ycoordinate, Root->Zcoordinate);
    //float DeltaPhi = PI / Root->Altura;
    float WTemp = Root->WInicio;
    //debo tener en cuenta al padre para hacer los calculos, ya que esto esta como arbol sin raiz

    //Root->Parent->Phi = Root->Phi + DeltaPhi;//estaba dividido /2
    Root->Padre->WTam = 2*PI * (float(Root->Padre->Hojas) / Root->Hojas);
    Root->Padre->WInicio = WTemp;
    Root->Padre->Theta = Root->Padre->WInicio + Root->Padre->WTam / 2;
    Root->Padre->X = 0;
    Root->Padre->Y = Root->Y + DistanciaArista * FMath::Cos(Root->Padre->Theta);
    Root->Padre->Z = Root->Z + DistanciaArista * FMath::Sin(Root->Padre->Theta);
    WTemp += Root->Padre->WTam;
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        //Root->Sons[i]->Phi = Root->Phi + DeltaPhi;//estaba dividido por 2
        Root->Hijos[i]->WTam = 2*PI * (float(Root->Hijos[i]->Hojas) / Root->Hojas);
        Root->Hijos[i]->WInicio = WTemp;
        Root->Hijos[i]->Theta =Root->Hijos[i]->WInicio + Root->Hijos[i]->WTam / 2;
        Root->Hijos[i]->X = 0;
        Root->Hijos[i]->Y = Root->Y + DistanciaArista * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->Z = Root->Z + DistanciaArista * FMath::Sin(Root->Hijos[i]->Theta);
        WTemp += Root->Hijos[i]->WTam;
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        WTemp = V->WInicio;
        if (V->Hijos[0]) {
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                V->Hijos[i]->WTam = 2 * PI * (float(V->Hijos[i]->Hojas) / Root->Hojas);
                V->Hijos[i]->WInicio = WTemp;
                V->Hijos[i]->Theta = V->Hijos[i]->WInicio + V->Hijos[i]->WTam / 2;
                V->Hijos[i]->X = 0;
                V->Hijos[i]->Y = V->Y + DistanciaArista * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->Z = V->Z + DistanciaArista * FMath::Sin(V->Hijos[i]->Theta);
                WTemp += V->Hijos[i]->WTam;
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

/*
Layout Radial 3D

Generalizaci�n del layout radial 2D
*/
void UNJTreeLayoutLibrary::LayoutRadial3D(TArray<Nodo*>& Nodos, float DistanciaArista) {
    /*
        Mejorar:
        - algunas ramas estan tomando angulos equivocados, contrarios a la direccion que tenian, tanto horizontal como verticalmente
        - El dispersamiento vertical tambien puede mejorarse, las ramas no estan siguendo un sentido natural o suave en algunos puntos. Tambien podria mejorarse un poco el aprovechamiento de espacio.
        - Al igual que sucede en el vertical, horizontalmente se peude hacer un mejor aprovechamiento del espacio, ya que hay menos nodos ocupando el espacio horizontal (creo que ahora son la itad de nodos en horizontal)
    */
    //parece que solo necesito la cantidad de hojas, pero siempre es mejor tener todos los datos
    TQueue<Nodo *> Cola;
    //la raiz es el ultimo nodo
    Nodo * Root = Nodos[Nodos.Num() - 1];
    //agregado para el nuevo radio
    //int NivelDenso, CantidadNodosNivelDenso;
    //NivelMasDenso(NivelDenso, CantidadNodosNivelDenso);
    //NewRadio = EncontrarRadio1(DeltaPhi * NivelDenso, CantidadNodosNivelDenso); 
    //
    float TamVertical = PI / 2;// PI;
    Root->Theta = 0;
    Root->Phi = 0;
    Root->WTam = 2*PI;
    Root->WInicio = 0;
    Root->HTam = PI;
    Root->HInicio = 0;
    Root->X = 0;
    Root->Y = 0;
    Root->Z = 0;
    //UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->Xcoordinate, Root->Ycoordinate, Root->Zcoordinate);
    //float DeltaPhi = PI / Root->Altura;
    float WTemp = Root->WInicio;
    float HTemp = Root->HInicio;
    //debo tener en cuenta al padre para hacer los calculos, ya que esto esta como arbol sin raiz
    float MaxHojasVertical = FMath::Max3(Root->Padre->Hojas, Root->Hijos[0]->Hojas, Root->Hijos[1]->Hojas);

    //Root->Parent->Phi = Root->Phi + DeltaPhi;//estaba dividido /2
    Root->Padre->WTam = 2*PI * (float(Root->Padre->Hojas) / Root->Hojas);
    Root->Padre->WInicio = WTemp;
    Root->Padre->Theta = Root->Padre->WInicio + Root->Padre->WTam / 2;
    Root->Padre->HTam = TamVertical * (float(Root->Padre->Hojas) / MaxHojasVertical);
    Root->Padre->HInicio = TamVertical/2 + Root->HInicio + (TamVertical - Root->Padre->HTam)/2;//esto se calcula con un ofset de lo restante
    Root->Padre->Phi = Root->Padre->HInicio + Root->Padre->HTam / 2;
    Root->Padre->X = Root->X + DistanciaArista * FMath::Sin(Root->Padre->Phi) * FMath::Cos(Root->Padre->Theta); // * Sin(Phi)
    Root->Padre->Y = Root->Y + DistanciaArista * FMath::Sin(Root->Padre->Phi) * FMath::Sin(Root->Padre->Theta); // * Sin(Phi)
    Root->Padre->Z = Root->Z + DistanciaArista * FMath::Cos(Root->Padre->Phi);
    WTemp += Root->Padre->WTam;
    HTemp += Root->Padre->HTam;
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        //Root->Sons[i]->Phi = Root->Phi + DeltaPhi;//estaba dividido por 2
        Root->Hijos[i]->WTam = 2*PI * (float(Root->Hijos[i]->Hojas) / Root->Hojas);
        Root->Hijos[i]->WInicio = WTemp;
        Root->Hijos[i]->Theta = Root->Hijos[i]->WInicio + Root->Hijos[i]->WTam / 2;
        Root->Hijos[i]->HTam = TamVertical * (float(Root->Hijos[i]->Hojas) / MaxHojasVertical);
        Root->Hijos[i]->HInicio = TamVertical/2 + Root->HInicio + (TamVertical - Root->Hijos[i]->HTam)/2;//esto se calcula con un ofset de lo restante
        Root->Hijos[i]->Phi = Root->Hijos[i]->HInicio + Root->Hijos[i]->HTam / 2;
        Root->Hijos[i]->X = Root->X + DistanciaArista * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->Y = Root->Y + DistanciaArista * FMath::Sin(Root->Hijos[i]->Phi) * FMath::Sin(Root->Hijos[i]->Theta);
        Root->Hijos[i]->Z = Root->Z + DistanciaArista * FMath::Cos(Root->Hijos[i]->Phi);
        WTemp += Root->Hijos[i]->WTam;
        HTemp += Root->Hijos[i]->HTam;
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        WTemp = V->WInicio;
        HTemp = V->HInicio;
        if (V->Hijos[0]) {
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
                if (V->Nivel & 1) {// nivel impar
                    // cuando toca vertical, el usado de la proporcion con el numero de hojas debe ser un poco distitno
                    V->Hijos[i]->WTam = V->WTam;
                    V->Hijos[i]->WInicio = V->WInicio;
                    V->Hijos[i]->Theta = V->Hijos[i]->WInicio + V->Hijos[i]->WTam / 2;
                    V->Hijos[i]->HTam = TamVertical * (float(V->Hijos[i]->Hojas) / MaxHojasVertical);//si se usa root, crece hacia arriba, usando el numero de hojas, de la ramma principal a la que pertenece, sale bien, pero muy disperso
                    V->Hijos[i]->HInicio = HTemp;
                    V->Hijos[i]->Phi = V->Hijos[i]->HInicio + V->Hijos[i]->HTam / 2;
                }
                else {// nivel par, como el Root
                    V->Hijos[i]->WTam = 2*PI * (float(V->Hijos[i]->Hojas) / Root->Hojas);
                    V->Hijos[i]->WInicio = WTemp;
                    V->Hijos[i]->Theta = V->Hijos[i]->WInicio + V->Hijos[i]->WTam / 2;
                    V->Hijos[i]->HTam = V->HTam;
                    V->Hijos[i]->HInicio = V->HInicio;
                    V->Hijos[i]->Phi = V->Hijos[i]->HInicio + V->Hijos[i]->HTam / 2;
                }
                V->Hijos[i]->X = V->X + DistanciaArista * FMath::Sin(V->Hijos[i]->Phi) * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->Y = V->Y + DistanciaArista * FMath::Sin(V->Hijos[i]->Phi) * FMath::Sin(V->Hijos[i]->Theta);
                V->Hijos[i]->Z = V->Z + DistanciaArista * FMath::Cos(V->Hijos[i]->Phi);
                WTemp += V->Hijos[i]->WTam;
                HTemp += V->Hijos[i]->HTam;
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}
