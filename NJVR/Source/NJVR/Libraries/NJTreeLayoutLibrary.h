// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../NJ/Nodo.h"
#include "NJTreeLayoutLibrary.generated.h"

/**
 * Usar despues de que los nodos del arbol ya tiene la informacion de hojas, altura, nivel, etc ya calculada
 */
UCLASS()
class NJVR_API UNJTreeLayoutLibrary : public UObject
{
	GENERATED_BODY()

public:
	
    static void CalcularRadio(Nodo * V, float RadioFrameHoja, float DeltaDistanciaArista);

    static void CalcularPhi(Nodo * V, float DeltaPhi);

    static void CalcularRadioMin(Nodo * V, float RadioFrameHoja, float DeltaDistanciaArista);

    static void CalcularRadioPromedio(Nodo * V, float RadioFrameHoja, float DeltaDistanciaArista);

    static void CalcularRadioHemiesfera(Nodo * V, float RadioHoja);

    static void LayoutBase(TArray<Nodo *> & Nodos, float RadioHojas);

    static void LayoutDistanciaReducida(TArray<Nodo *> & Nodos, float RadioHojas);

    static void LayoutDistanciaAumentada(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas);

    static void LayoutDistanciaAumentadaAnguloReducido(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas);

    static void LayoutDistanciaAumentadaPromedioAnguloReducidoPorAltura(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas);

    static void LayoutDistanciaAumentadaPromedioAnguloReducidoPorAlturaIndependienteHijo(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas);

    static void LayoutDistanciaAumentadaPromedioAnguloReducido(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas);

    static void LayoutDistanciaAumentadaProgresivaAnguloReducido(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas);

    static void LayoutDistanciaAumentadaHijoAnguloReducido(TArray<Nodo *> & Nodos, float RadioHojas, float DeltaDistanciaAristas);

    static void LayoutDistanciaAumentadaAnguloReducidoSinIntercalamiento(TArray<Nodo*>& Nodos, float RadioHojas, float DeltaDistanciaAristas);

    static void LayoutMesa(TArray<Nodo *> & Nodos, float RadioNodos, FVector Offset);

    static void LayoutEsferico(TArray<Nodo*>& Nodos, float RadioEsfera, float PhiMin, float PhiMax);

    static void LayoutEsfericoLadrillos(TArray<Nodo*>& Nodos, float RadioEsfera, float PhiMin, float PhiMax);

    static void LayoutRadial2D(TArray<Nodo*>& Nodos, float DistanciaArista);

    static void LayoutRadial3D(TArray<Nodo*>& Nodos, float DistanciaArista);

};
