// Fill out your copyright notice in the Description page of Project Settings.


#include "Arista.h"
#include "Nodo.h"

void Arista::SetSourceNodo(Nodo * NewSourceNodo) {
    if (NewSourceNodo) {
        SourceId = NewSourceNodo->Id;
        SourceNodo = NewSourceNodo;
    }
}

void Arista::SetTargetNodo(Nodo * NewTargetNodo) {
    if (NewTargetNodo) {
        TargetId = NewTargetNodo->Id;
        TargetNodo = NewTargetNodo;
        ActualTargetPosition = TargetNodo->GetLocation();//para que emipiece del tama�o exacto, si incio animacion este cambiara
    }
}

void Arista::SetSourceNodo(int NewSourceId, Nodo * NewSourceNodo) {
    SourceId = NewSourceId;
    SourceNodo = NewSourceNodo;
}

void Arista::SetTargetNodo(int NewTargetId, Nodo * NewTargetNodo) {
    TargetId = NewTargetId;
    TargetNodo = NewTargetNodo;
    if (TargetNodo) {
        ActualTargetPosition = TargetNodo->GetLocation();//para que emipiece del tama�o exacto, si incio animacion este cambiara
    }
}

FVector Arista::GetTargetPosition() {
    if (bVisible) {
        if (bStreching) {
            return ActualTargetPosition;//quiza deberia estar en un que controle la animacion
        }
        else {
            return TargetNodo->GetLocation();
        }
    }
    else {
        return SourceNodo->GetLocation();
    }
}

FVector Arista::GetSourcePosition() {
    return SourceNodo->GetLocation();
}

void Arista::UpdateAnimation(float DeltaTime) {
    if (ActualDelay >= Delay) {
        if (bStreching) {//cada una maneja sus tiempos
            TiempoActualStreching += DeltaTime;
            ActualTargetPosition = FMath::InterpEaseInOut(SourceNodo->GetLocation(), TargetNodo->GetLocation(), TiempoActualStreching / TiempoStreching, 2);//muy bueno
            if (TiempoActualStreching > TiempoStreching) {
                bStreching = false;
                //como termino la animacion , tambien reseteo los delay
                ActualDelay = 0.0f;
                Delay = 0.0f;
            }
        }
    }
    else {
        ActualDelay += DeltaTime;
    }
    if (bGrowing) {
        TiempoActualGrowing += DeltaTime;
        ActualRadio = FMath::InterpEaseInOut(InitialRadio, FinalRadio, TiempoActualGrowing / TiempoGrowing, 2);//muy bueno
        if (TiempoActualGrowing > TiempoGrowing) {
            bGrowing = false;
        }
    }
    if (!bStreching && !bGrowing) {
        bAnimando = false;
    }
}

void Arista::ConectSourceWithTargetAnimation() {
    bAnimando = true;
    bStreching = true;
    TiempoActualStreching = 0.0f;
    TiempoStreching = 1.0f;
    ActualDelay = 0.0f;
    Delay = 0.0f;
}

void Arista::ConectSourceWithTargetAnimation(float StartDelay) {
    bAnimando = true;
    bStreching = true;
    TiempoActualStreching = 0.0f;
    TiempoStreching = 1.0f;
    ActualDelay = 0.0f;
    Delay = StartDelay;
    ActualTargetPosition = SourceNodo->GetLocation();
}

float Arista::GetRadio() {
    if (bVisible) {
        return ActualRadio;
    }
    else {
        return 0.001f;
    }
}

void Arista::SetRadio(float NewRadio) {
    ActualRadio = NewRadio;
}

void Arista::GrowToRadio(float NewRadio) {
    InitialRadio = ActualRadio;
    FinalRadio = NewRadio;
    bAnimando = true;
    bGrowing = true;
    TiempoActualGrowing = 0.0f;
    TiempoGrowing = 1.0f;
}

bool Arista::NeedUpdate() {
	return SourceNodo->Selected || TargetNodo->Selected || SourceNodo->bAnimando || TargetNodo->bAnimando;
}

Arista::Arista() {
    SourceNodo = nullptr;
    TargetNodo = nullptr;
    bAnimando = false;
    bStreching = false;
    bGrowing = false;
    bVisible = false;
}

Arista::~Arista() {
}
