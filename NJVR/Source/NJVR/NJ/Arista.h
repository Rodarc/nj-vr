// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Structures/DrawElementReferenceStruct.h"

/**
 * 
 */
class NJVR_API Arista
{
public:
    int Id;
    int SourceId; //id del nodo del que sale esta arista, es decir del padre
    int TargetId; //id del nodo al que se dirige la arista, es decir el hijo
    class Nodo * SourceNodo; //referencia al nodo padre
    class Nodo * TargetNodo; //referencia al nodo hijo
    void SetSourceNodo(class Nodo * NewSourceNodo); //referencia al nodo padre
    void SetTargetNodo(class Nodo * NewTargetNodo); //referencia al nodo hijo
    void SetSourceNodo(int NewSourceId, class Nodo * NewSourceNodo); //referencia al nodo padre
    void SetTargetNodo(int NewTargetId, class Nodo * NewTargetNodo); //referencia al nodo hijo

    //para la visualizacion
    float Escala;
    float Distancia;//la distancia no puede ser menor que el radio, esta es por la capsula la cual sera el cllider
    float Radio;

    bool bAnimando;
    bool bStreching;

    float TiempoStreching;
    float TiempoActualStreching;

    FVector ActualTargetPosition;
    //deberia tener funciones que me devuelvan las posiciones en funcion de la animacion que este ejcutando
    //si no tengo animacon devuelvo
    FVector GetTargetPosition();
    FVector GetSourcePosition();

    bool bGrowing;
    float ActualRadio;
    float InitialRadio;
    float FinalRadio;

    float TiempoGrowing;
    float TiempoActualGrowing;

    void UpdateAnimation(float DeltaTime); 

    void ConectSourceWithTargetAnimation();

    float ActualDelay;
    float Delay;
    void ConectSourceWithTargetAnimation(float StartDelay);

    float GetRadio();
    void SetRadio(float NewRadio);
    void GrowToRadio(float NewRadio);


	bool NeedUpdate();
    bool bVisible;


    FDrawElementReferenceStruct DrawNormal;
	Arista();
	~Arista();
};
