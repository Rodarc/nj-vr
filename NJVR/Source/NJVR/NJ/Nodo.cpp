// Fill out your copyright notice in the Description page of Project Settings.


#include "Nodo.h"

void Nodo::UpdateAnimation(float DeltaTime) {
    //dos tipos de animaciones, si me muevo o me agrando
    //TiempoActualAnimacion += DeltaTime;
    //en realidad aqui podria haber un if de bAnimation, pero este se esta preguntando fuera, si es verdadero recien se llama a esta funcion
    if (bMoving) {//cada una maneja sus tiempos
        TiempoActualMoving += DeltaTime;
        ActualLocation = FMath::InterpEaseInOut(InitialLocation, FinalLocation, TiempoActualMoving / TiempoMoving, 2);//muy bueno
        if (TiempoActualMoving > TiempoMoving) {
            bMoving = false;
        }
    }
    if (bGrowing) {
        TiempoActualGrowing += DeltaTime;
        ActualRadio = FMath::InterpEaseInOut(InitialRadio, FinalRadio, TiempoActualGrowing / TiempoGrowing, 2);//muy bueno
        if (TiempoActualGrowing > TiempoGrowing) {
            bGrowing = false;
        }
    }
    if (!bMoving && !bGrowing) {
        bAnimando = false;
    }
}

FVector Nodo::GetLocation() {
    //return FVector(X, Y, Z);
    return ActualLocation;
}

void Nodo::SetLocation(FVector NewLocation) {
    //X = NewLocation.X;//ya no actualizo la posicion del layout calculada
    //Y = NewLocation.Y;
    //Z = NewLocation.Z;

    ActualLocation = NewLocation;
}

void Nodo::MoveToLocation(FVector NewLocation) {
    //establesco la posision inicial que es la que tengo ahora
    InitialLocation = ActualLocation;
    //establesco la posicion final, que es new location
    FinalLocation = NewLocation;
    //establesco el bAnimando en true;, este valor sera leido por Visulaization
    bAnimando = true;
    bMoving = true;
    TiempoActualMoving = 0.0f;
    TiempoMoving = 0.5f;
    //quiza tambien deba establecer el tiempo, que vaya en funcion de la distancia que se va a recorrer
    //Visualization se entcargara de calcular la posicion actual
}

float Nodo::GetRadio() {
    return ActualRadio;
}

void Nodo::SetRadio(float NewRadio) {
    ActualRadio = NewRadio;
}

void Nodo::GrowToRadio(float NewRadio) {
    InitialRadio = ActualRadio;
    FinalRadio = NewRadio;
    bAnimando = true;
    bGrowing = true;
    TiempoActualGrowing = 0.0f;
    TiempoGrowing = 0.5f;
}

//tal vez deba tener mi funcion que reciba el avance del tiempo para actualizar mi posicion, en lugar de que visualizacion la actualice

Nodo::Nodo() {
    Id = 0;
    Nombre = "";
    Orden = -1;
    Padre = nullptr;
    PadreId = -1;
    HijosId = new int [2];
    Hijos = new Nodo * [2];
    DistanciasHijos = new float [2];
    HijosId[0] = -1;
    HijosId[1] = -1;
    Hijos[0] = nullptr;
    Hijos[1] = nullptr;
    HojasId = nullptr;
    NumeroHojas = 0;//en teoria este siempre deberia tener tamano 1 ya que el nodo real deberia ser ese represante, los viruales represntanc a 2 o mas
    X = 0.0f;
    Y = 0.0f;
    Z = 0.0f;

    bAnimando = false;
    bGrowing = false;
    bMoving = false;
}

Nodo::~Nodo() {
    delete [] HojasId;
    delete [] Hijos;
    delete [] HijosId;
    delete [] DistanciasHijos;
}

void Nodo::AgregarHoja(int n){
    int * temp = HojasId;
    HojasId = new int [NumeroHojas + 1];
    for(int i = 0; i < NumeroHojas; i++){
        *(HojasId + i) = *(temp + i);
    }//abra una mejor forma de realizar esta copia
    HojasId[NumeroHojas] = n;
    NumeroHojas++;
    delete [] temp;
}

FVector Nodo::GetLayoutLocation() {
    return FVector(X, Y, Z);
}
