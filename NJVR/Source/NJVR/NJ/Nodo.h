// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <string>
#include "../Structures/DrawElementReferenceStruct.h"

using namespace std;

/**
 * 
 */
class NJVR_API Nodo {
public:
    // Atributos para el NJ
    int Id;
    bool Valido;
    string Nombre;//nombre del elemento o dato
    int Orden;
    int * HijosId;
    Nodo ** Hijos;
    float * DistanciasHijos;//deberianser vectores
    int PadreId;
    Nodo * Padre;
    float DistanciaPadre;
    int * HojasId;
    int NumeroHojas;//solo para saber a quienes representa
    void AgregarHoja(int n);

    //para el layout
    float Theta;
    float Phi;
    int Nivel;
    int Altura;
    float WInicio;
    float WTam;
    float HInicio;
    float HTam;
    int Hojas;//lo usa Numero de hojas, por lo visto ya lo tengo calculado
    int Casilla;//para el layout2
    FMatrix Frame;//para el H3
    float RadioFrame;//para el H3, rp


    FDrawElementReferenceStruct DrawNormal;
    FDrawElementReferenceStruct DrawSeleccionado;
    FDrawElementReferenceStruct DrawResaltado;
    // para la visualizacion, esta se copia o se pone como Final para que en algun memnto la actual la alcance
    int SeccionArbol;
    int IdSeccionArbol;
    int SeccionSeleccion;
    int IdSeccionSeleccion;
    int MeshResaltado;
    int IdMeshResaltado;
    int TamRama;
    float X;
    float Y;
    float Z;
    FVector GetLayoutLocation();
    //para el calculo relativo por ejmplo en el H3
    float XRelative;
    float YRelative;
    float ZRelative;

    //para las animaciones
    bool bAnimando;//por la forma del calculo este buoleando sirve para el traslado y para la escala del radio
    bool bMoving;
    FVector ActualLocation;
    FVector InitialLocation;
    FVector FinalLocation;

    bool bGrowing;
    float ActualRadio;
    float InitialRadio;
    float FinalRadio;

    float TiempoAnimacion;
    float TiempoActualAnimacion;

    float TiempoGrowing;
    float TiempoActualGrowing;

    float TiempoMoving;
    float TiempoActualMoving;

    void UpdateAnimation(float DeltaTime); 

    //para las tareas de analisis
    bool Selected;
    int ColorNum;
    FLinearColor Color;//este podria cambiar a un arreglo de colores si tengo diferentes formas de clasificar
    float Radio;
	bool Resaltado;

    //para la compatibilidad con el formato del pex
    FString FileName;
    FString Url;
    TArray<FString> Labels;

    //para trabajar con la visualizacion
    FVector GetLocation();
    void SetLocation(FVector NewLocation);
    void MoveToLocation(FVector NewLocation);

    float GetRadio();
    void SetRadio(float NewRadio);
    void GrowToRadio(float NewRadio);

	Nodo();
	~Nodo();
};

//estos nodos deben tener la informacion pertinente, como el contenido, la informacion para visualizar, y tal vez algo mas