// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "HeadMountedDisplay.h"
#include "Components/WidgetComponent.h"
#include "Components/WidgetInteractionComponent.h"
#include "MotionControllerComponent.h"
#include "Components/CapsuleComponent.h"
#include "VRPawn.generated.h"

UCLASS()
class NJVR_API AVRPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AVRPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    //analizar si estos parametos deben ser posibles verlosd esde el blueprint
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    class AVRVisualization * Visualization;//esto no es practio llenarlo en el cosntructor, cuando esta clase pase a bluprint sera mejor

    //Root component
    UPROPERTY(VisibleAnywhere, Category = "Visualization")
    USceneComponent * DefaultSceneRoot;
    //Camara del HMD
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UCameraComponent * VRCamera;
    
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    class UMotionControllerComponent * MotionControllerLeft;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    class UMotionControllerComponent * MotionControllerRight;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UStaticMeshComponent * ViveControllerLeft;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UStaticMeshComponent * ViveControllerRight;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "VRPawn")
	UCapsuleComponent * ColisionControllerLeft;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "VRPawn")
	UCapsuleComponent * ColisionControllerRight;

    //UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    //UWidgetComponent * Menu;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float DistanciaMenuCamaraToCamara;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float AlturaMenuCamara;

    /*UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * MenuCamara;*/

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * MenuContenido;

    //UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    //UWidgetComponent * Document;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * DataSets;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * MenuColores;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetInteractionComponent * Interaction;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UParticleSystemComponent * Laser;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UParticleSystemComponent * EfectoImpacto;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UFloatingPawnMovement * Movimiento;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    TArray<UParticleSystem *> Lasers;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    bool bPadDerecho;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float Velocidad;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float LaserIndice;

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void CambiarLaser(int Indice);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    int LaserActual();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void CambiarPuntoFinal(FVector PuntFinal);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void PadDerechoPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void PadDerechoReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void SelectPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void SelectReleased();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bClick;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bHold;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bDoubleClick;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float UmbralActualDoubleClick;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float UmbralDoubleClick;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float UmbralActualHold;

    //umbral para ejecutar las acciones de hold, esto sera para que no se inici una traslacion en caso de que no haya sido intencionarl
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float UmbralHold;


};
