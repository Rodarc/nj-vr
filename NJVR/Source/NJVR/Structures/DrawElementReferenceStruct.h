#pragma once

#include "CoreMinimal.h"
#include "DrawElementReferenceStruct.generated.h"

USTRUCT(BlueprintType)
struct FDrawElementReferenceStruct {
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int IdLayer;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int IdSection;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int IdElement;

    
};
