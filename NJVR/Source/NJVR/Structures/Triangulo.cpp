// Fill out your copyright notice in the Description page of Project Settings.


#include "Triangulo.h"

Triangulo::Triangulo() {
}

Triangulo::~Triangulo() {
}

Triangulo::Triangulo(int a, int b, int c, int niv, bool ori): IdA(a), IdB(b), IdC(c), Nivel(niv), Orientacion(ori) {
}

Triangulo::Triangulo(FVector va, FVector vb, FVector  vc, int niv, bool ori): A(va), B(vb), C(vc), Nivel(niv), Orientacion(ori) {
}

Triangulo::Triangulo(FVector va, FVector vb, FVector  vc, int a, int b, int c, int niv, bool ori): A(va), B(vb), C(vc), IdA(a), IdB(b), IdC(c), Nivel(niv), Orientacion(ori) {
}
