// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class NJVR_API Triangulo {
public:
    FVector A;
    FVector B;
    FVector C;
    int IdA;
    int IdB;
    int IdC;
    int Nivel;
    bool Orientacion;// 0 ahcia arriga, 1 hacia abajo, esto no se si lo use
    FVector Centro;
    float Phi;
    float Theta;
    Triangulo(int a, int b, int c, int niv, bool ori);
    Triangulo(FVector va, FVector vb, FVector  vc, int niv, bool ori);
    Triangulo(FVector va, FVector vb, FVector  vc, int a, int b, int c, int niv, bool ori);
	Triangulo();
	~Triangulo();
};
