// Fill out your copyright notice in the Description page of Project Settings.


#include "NJVRVisualization.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Materials/Material.h"
#include "../NJ/Nodo.h"
#include "../NJ/Arista.h"
#include <stack>
#include "MotionControllerComponent.h"
#include "NJVRVisualization.h"
#include "../Pawns/VRPawn.h"
#include "Particles/ParticleSystemComponent.h"
#include "../Libraries/GMathLibrary.h"
#include "../Libraries/NJTreeLayoutLibrary.h"



ANJVRVisualization::ANJVRVisualization() {
    RadioHoja = 6.0f;
    DeltaDistanciaArista = 2.0f;
    //DeltaDistanciaArista = 0.5f;
    PrecisionAristas = 8;
    PrecisionNodos = 1;
    //necesito asignar la presion a los actores dibujadores

    static ConstructorHelpers::FObjectFinder<UMaterial> NodosMaterialAsset(TEXT("Material'/Game/Visualization/Materials/NodosMeshMaterial.NodosMeshMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (NodosMaterialAsset.Succeeded()) {
        NodosMeshMaterial = NodosMaterialAsset.Object;
        NodosSelectedMeshMaterial = NodosMaterialAsset.Object;
    }

    static ConstructorHelpers::FObjectFinder<UMaterial> NodosSelectedMaterialAsset(TEXT("Material'/Game/Visualization/Materials/NodosSelectedMeshMaterial.NodosSelectedMeshMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (NodosSelectedMaterialAsset.Succeeded()) {
        //NodosSelectedMeshMaterial = NodosSelectedMaterialAsset.Object;
    }

    static ConstructorHelpers::FObjectFinder<UMaterial> AristasMaterialAsset(TEXT("Material'/Game/Visualization/Materials/AristasMeshMaterial.AristasMeshMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (AristasMaterialAsset.Succeeded()) {
        AristasMeshMaterial = AristasMaterialAsset.Object;
    }

    NeedUpdateAristas = false;
    NeedUpdateNodos = false;

    UmbralSizeNodosProceduralMesh = 1000;//el umbral para mi laptop sera de 1000 nodos
}

void ANJVRVisualization::BeginPlay() {
    Super::BeginPlay();

    /*UWorld * const World = GetWorld();
    if (World) {
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = Instigator;

        NodosVisuales = World->SpawnActor<ASpheresDrawer>(ASpheresDrawer::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
        NodosVisuales->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
    }*/

	CreateMeshesResaltados();
    CreateSeccionNodoSelected();//creo una seccion para poder hacer seleccionase, de ser necesarias luego se crearan mas
    //aun que podria hacerlo en la porpia funcion selec
    //LayoutBase();
    //LayoutDistanciaReducida();
    //LayoutDistanciaAumentada();//el intermedio, funcona bien con los de radio grandes recuciendo el delta para los radios

    LayoutDistanciaAumentadaAnguloReducido();//acoplado bonito, el mas equilibrado, la mejor opcion

    //LayoutDistanciaAumentadaAnguloReducidoSinIntercalamiento();
    //LayoutDistanciaAumentadaHijoAnguloReducido();
    //LayoutDistanciaAumentadaProgresivaAnguloReducido();
    //LayoutDistanciaAumentadaPromedioAnguloReducido();
    //LayoutDistanciaAumentadaPromedioAnguloReducidoPorAltura();

    //LayoutDistanciaAumentadaPromedioAnguloReducidoPorAlturaIndependienteHijo(); // muy bueno, aun que hay algo de cruzamiento

    /*Layouts adicionales*/
    //LayoutMesa();
    //LayoutEsferico();
    //LayoutEsfericoLadrillos();
    //LayoutRadial2D();
    //LayoutRadial3D();
    //ActualizarLayout();

    //LayoutDistanciaAumentadaHijoAnguloReducido();// mas pequeno que el anterior

    ActualizarLayout();
}

void ANJVRVisualization::Tick(float DeltaTime) {
    Super::Tick(DeltaTime);
    if (bAnimatingConstruction) {
        ActualTimeStepConstruction += DeltaTime;
        if (ActualTimeStepConstruction >= TimeStepConstruction) {
            //itero al siguiente nodo, inicio su animacion
            CurrentNodeConstruction++;
            if (CurrentNodeConstruction < Nodos.Num()) {
                //ejecuto animacion para este nodo virtual
                ActualTimeStepConstruction = 0.0f;
                Nodos[CurrentNodeConstruction]->GrowToRadio(RadioNodosVirtuales);//el debe estar ubicado exactamente donde debe
                Nodos[CurrentNodeConstruction]->SetLocation(Nodos[CurrentNodeConstruction]->GetLayoutLocation());//el debe estar ubicado exactamente donde debe
                if (Nodos[CurrentNodeConstruction]->Hijos[0]->Valido) {
                    Nodos[CurrentNodeConstruction]->Hijos[0]->MoveToLocation(Nodos[CurrentNodeConstruction]->Hijos[0]->GetLayoutLocation());
                }
                if (Nodos[CurrentNodeConstruction]->Hijos[1]->Valido) {
                    Nodos[CurrentNodeConstruction]->Hijos[1]->MoveToLocation(Nodos[CurrentNodeConstruction]->Hijos[1]->GetLayoutLocation());
                }
                int IdAritasReferenica = CurrentNodeConstruction - (Nodos.Num() / 2 + 1);
                Aristas[2*IdAritasReferenica]->bVisible = true;
                Aristas[2*IdAritasReferenica + 1]->bVisible = true;
                Aristas[2*IdAritasReferenica]->ConectSourceWithTargetAnimation(0.5f);
                Aristas[2*IdAritasReferenica + 1]->ConectSourceWithTargetAnimation(0.5f);
                Aristas[2*IdAritasReferenica]->GrowToRadio(RadioAristas);
                Aristas[2*IdAritasReferenica + 1]->GrowToRadio(RadioAristas);

                if (CurrentNodeConstruction == Nodos.Num() - 1) {
                    Aristas[Aristas.Num() - 1]->bVisible = true;
                    Aristas[Aristas.Num() - 1]->ConectSourceWithTargetAnimation();
                    Aristas[Aristas.Num() - 1]->GrowToRadio(RadioAristas);
                }
            }
            else {
                //debo seguir ejecutnado animaciones, pero debo dejar de lanzar nueva, por lo tanto, animatingconstruct temrno
                bAnimatingConstruction = false;
                //falta coordinar bien cuando termina la nimacion y cuando terminar de inciar animaciones
            }
        }
    }
    EjecutarAnimaciones(DeltaTime);
}

void ANJVRVisualization::EjecutarAnimaciones(float DeltaTime) {
    //bool NeedUpdate = false;//comoesto se llamara en cada frame, ya no necesito el update mesh en cada funcion que lo requeria, ahora solo debo poner este bool en verdadero y se llamara a actualizar, asi no encuentre un animating;
    //normalmente estara en falso, estara en verdadero si alguien lo puso asi, o despues de este blucle se pone en true
    for (int i = 0; i < Nodos.Num(); i++) {
        if (Nodos[i]->bAnimando) {
            NeedUpdateNodos = true;
            //calcular la posicion actual de esta animacion
            //hacer update nodo con esta nueva posicion
            Nodos[i]->UpdateAnimation(DeltaTime);
            NodosVisuales->UpdatePosicionAndRadioSphereInMeshSeccion(Nodos[i]->DrawNormal, Nodos[i]->GetLocation(), Nodos[i]->GetRadio());
        }
    }
    for (int i = 0; i < Aristas.Num(); i++) {//este puede ser un poco pensado
        if (Aristas[i]->NeedUpdate()) {
            NeedUpdateAristas = true;
            Aristas[i]->UpdateAnimation(DeltaTime);
            AristasVisuales->UpdatePosicionAndRadioCylinderInMeshSeccion(Aristas[i]->DrawNormal, Aristas[i]->GetSourcePosition(), Aristas[i]->GetTargetPosition(), Aristas[i]->ActualRadio);
        }
        else if (Aristas[i]->bAnimando) {
            NeedUpdateAristas = true;
            Aristas[i]->UpdateAnimation(DeltaTime);
            AristasVisuales->UpdatePosicionAndRadioCylinderInMeshSeccion(Aristas[i]->DrawNormal, Aristas[i]->GetSourcePosition(), Aristas[i]->GetTargetPosition(), Aristas[i]->ActualRadio);
        }
    }

    //estos ifs no seran necesarios
    if (NeedUpdateNodos) {
        UpdateNodosMesh();
    }
    if (NeedUpdateAristas) {
        UpdateAristasMesh();
    }

    NeedUpdateNodos = false;
    NeedUpdateAristas = false;
}

void ANJVRVisualization::TestAnimation() {
    //Nodos[Nodos.Num() - 1]->MoveToLocation(FVector(0.0f, 0.0f, 200.0f));
    //Nodos[Nodos.Num() - 1]->GrowToRadio(10.0f);
    //Aristas[Aristas.Num() - 1]->ConectSourceWithTargetAnimation();
    //Aristas[Aristas.Num() - 1]->GrowToRadio(8.0f);
    EjecutarAnimacionCreacionArbol();
}

void ANJVRVisualization::CreateNodos() {
    FXmlNode * rootnode = XmlSource.GetRootNode();
    //FXmlNode * rootnode = XmlSourceP->GetRootNode();
    //if (GEngine) {
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, rootnode->GetTag());
    //}
    TArray<FXmlNode *> XMLnodes = rootnode->GetChildrenNodes();
    TArray<FXmlNode*> XMLvertexs;
    TArray<FXmlNode*> XMLedges;
    for (int i = 0; i < XMLnodes.Num(); ++i) {
        if (XMLnodes[i]->GetTag() == "vertex") {
            XMLvertexs.Add(XMLnodes[i]);
        }
        /*if (XMLnodes[i]->GetTag() == "edges" && (XMLnodes[i]->GetAttribute("name") == "NJ" || XMLnodes[i]->GetAttribute("name") == "N-J")) {
            TArray<FXmlNode *> XMLnodesed = XMLnodes[i]->GetChildrenNodes();
            for (int j = 0; j < XMLnodesed.Num(); j++) {
                XMLedges.Add(XMLnodesed[j]);
            }
        }*/
    }
    //obtenienod la unicaion del ulitmo nodo para centrar todo el arbol
    FXmlNode * nodexorigen = XMLvertexs[XMLvertexs.Num()-1]->FindChildNode(FString("x-coordinate"));
    FString xorigen = nodexorigen->GetAttribute("value");
    float OrigenX = FCString::Atof(*xorigen);
    FXmlNode * nodeyorigen = XMLvertexs[XMLvertexs.Num()-1]->FindChildNode(FString("y-coordinate"));
    FString yorigen = nodeyorigen->GetAttribute("value");
    float OrigenY = FCString::Atof(*yorigen) * -1;
    //tengo todos los vertices en ese array
    TArray<int> numerocolores;
    for (int i = 0; i < XMLvertexs.Num(); ++i) {
        //obteniendo el id
        FString id = XMLvertexs[i]->GetAttribute(FString("id"));//devuelve el valor del atributo que le doy, hay otra funocin que me devuelve todos los atributos en un arrya de un obejto especial//quiza deba esto guardarlo como int cuando genere la clase Vertex
        
        //obteniendo el valid
        FXmlNode * nodevalid = XMLvertexs[i]->FindChildNode(FString("valid"));
        FString valid = nodevalid->GetAttribute("value");

        //obteniendo la x-coordinate
        FXmlNode * nodex = XMLvertexs[i]->FindChildNode(FString("x-coordinate"));
        FString xcoordinate = nodex->GetAttribute("value");

        //obteniendo la y-coordinate
        FXmlNode * nodey = XMLvertexs[i]->FindChildNode(FString("y-coordinate"));
        FString ycoordinate = nodey->GetAttribute("value");

        //obteniendo url
        FXmlNode * nodeurl = XMLvertexs[i]->FindChildNode(FString("url"));
        FString url = nodeurl->GetAttribute("value");

        //obteniendo parent 
        FXmlNode * nodeparent = XMLvertexs[i]->FindChildNode(FString("parent"));//quiza no sean necesario usar FString
        FString parent = nodeparent->GetAttribute("value");

        //los hijos no estan dentro de un array por lo tanto es necesario reccorrer todos los child nose, es decir lo de aqui arruba fue por las puras, jeje
        TArray<FString> sons;
        TArray<FXmlNode*> childs = XMLvertexs[i]->GetChildrenNodes();
        for (int j = 0; j < childs.Num(); j++) {
            if (childs[j]->GetTag() == "son") {
                sons.Add(childs[j]->GetAttribute("value"));
            }
        }

        FXmlNode * nodelabels = XMLvertexs[i]->FindChildNode(FString("labels"));//quiza no sean necesario usar FString
        TArray<FXmlNode*> labelschilds = nodelabels->GetChildrenNodes();
        TArray<FString> labels;
        for (int j = 0; j < labelschilds.Num(); j++) {
            labels.Add(labelschilds[j]->GetAttribute("value"));
            //aqui faltaria definir que label es cada uno, para poder ponerlo en la variable que corresponda en el la calse vertex que creare
        }
        FXmlNode * nodescalars = XMLvertexs[i]->FindChildNode(FString("scalars"));//quiza no sean necesario usar FString
        TArray<FXmlNode*> scalarschilds = nodescalars->GetChildrenNodes();
        FString colorcdata;
        for (int j = 0; j < scalarschilds.Num(); j++) {
            if (scalarschilds[j]->GetAttribute("name") == "cdata") {
                colorcdata = scalarschilds[j]->GetAttribute("value");
            }
        }
        //el contenido de los nodos, es lo que hay en trexto plano dentro del tag de apertura y de cierre

        //TArray<FXmlNode*> childs = vertexs[i]->GetChildrenNodes();//para el caso de los vertexs sus hijos son unicos o son un array por lo tanto podria usar la funcion findchildren, para encontrar los que necesito

        //creando un objeto nodo, instanciando y llenando sus datos
        //creo los nodos, pero estos ya no se instancian

        Nodo * NodoInstanciado = new Nodo;
        NodoInstanciado->Id = FCString::Atoi(*id);
        NodoInstanciado->Valido = FCString::ToBool(*valid);
        NodoInstanciado->X = FCString::Atof(*xcoordinate);
        NodoInstanciado->Y = FCString::Atof(*ycoordinate);
        NodoInstanciado->Url = url;
        for (int j = 0; j < labels.Num(); j++) {
            NodoInstanciado->Labels.Add(labels[j]);
        }
        NodoInstanciado->PadreId = FCString::Atoi(*parent);
        for (int j = 0; j < sons.Num(); j++) {
            NodoInstanciado->HijosId[j] = FCString::Atoi(*sons[j]);//quiza deberia reemplzar por vector al de los hijos//que pasa si tuviera mas?
            NodoInstanciado->Hijos[j] = Nodos[NodoInstanciado->HijosId[j]];//para agregar la referencia, esto o se peude con el padre, por que en toeria aun no existe, habria que realizar una segunda pasada, alli podiasmos incluir esto para evtar algun fallo
        }
        NodoInstanciado->Selected = false;
        if (NodoInstanciado->Valido) {//aqui a los nodos reales se le debe asiganar algun colo de acerud a algun criterio, por ahora dejar asi
            NodoInstanciado->Color = FLinearColor::Black;
            NodoInstanciado->ColorNum = FCString::Atoi(*colorcdata);
            NodoInstanciado->Radio = RadioNodos;//esto es visual, hacerlo aquia, pero depues
            NodoInstanciado->SetRadio(RadioNodos);
            numerocolores.AddUnique(NodoInstanciado->ColorNum);
            //UE_LOG(LogClass, Log, TEXT("Color = %d"), NodoInstanciado->ColorNum);
        }
        else {
            NodoInstanciado->Color = ColorVirtual;//tambien debo cambiarle el tama�o
            NodoInstanciado->ColorNum = FCString::Atoi(*colorcdata);
            NodoInstanciado->Radio = RadioNodosVirtuales;
            //NodoInstanciado->SetRadio(RadioNodosVirtuales);
            NodoInstanciado->SetRadio(0.01f);
        }
        //actualizar nodo, para cambiar el color o el tama�o si es necesario
        //NodoInstanciado->AttachRootComponentToActor(this);
        Nodos.Add(NodoInstanciado);
        //NodoInstanciado->GetRootComponent()->SetupAttachment(RootComponent);// para hacerlo hioj de la visualizaci�n, aunque parece que esto no es suficient
    }
    //UE_LOG(LogClass, Log, TEXT("NumeroColor = %d"), numerocolores.Num());
    if (DataSetSeleccionado == 21) {
        numerocolores.Sort();//para el segundo conjunto
    }
    //int variacion = 240 / (numerocolores.Num()-1);//produce erro si solo hay un color,ver como asifnar mejor los colores
    int variacion = 240 / numerocolores.Num();
    for (int k = 0; k < numerocolores.Num(); k++) {
        //int h = (k*variacion) % 360;
        int h = (k*variacion);// % 240;
        Colores.Add(UKismetMathLibrary::HSVToRGB(h, 1.0f, IntensidadColor, 1.0f));
    }
    /*for (int i = 0; i < XMLedges.Num(); i++) {
        //obteniendo el id
        FString source = XMLedges[i]->GetAttribute(FString("source"));//devuelve el valor del atributo que le doy, hay otra funocin que me devuelve todos los atributos en un arrya de un obejto especial//quiza deba esto guardarlo como int cuando genere la clase Vertex
        FString target = XMLedges[i]->GetAttribute(FString("target"));//devuelve el valor del atributo que le doy, hay otra funocin que me devuelve todos los atributos en un arrya de un obejto especial//quiza deba esto guardarlo como int cuando genere la clase Vertex
        int SourceId = FCString::Atoi(*source);
        int TargetId = FCString::Atoi(*target);
        if (SourceId == Nodos.Num() - 1 && TargetId == Nodos.Num() - 2) {
            Nodos[Nodos.Num() - 1]->Padre = Nodos[Nodos.Num() - 2];
            Nodos[Nodos.Num() - 1]->PadreId = Nodos[Nodos.Num() - 2]->Id;
            Nodos[Nodos.Num() - 2]->Padre = Nodos[Nodos.Num() - 1];
            Nodos[Nodos.Num() - 2]->PadreId = Nodos[Nodos.Num() - 1]->Id;
        }
        else {
            if (Nodos[SourceId]->Hijos[0] == nullptr) {
                Nodos[SourceId]->Hijos[0] = Nodos[TargetId];
                Nodos[SourceId]->HijosId[0] = Nodos[TargetId]->Id;
                Nodos[TargetId]->Padre = Nodos[SourceId];
                Nodos[TargetId]->PadreId = Nodos[SourceId]->Id;
            }
            else if (Nodos[SourceId]->Hijos[1] == nullptr) {
                Nodos[SourceId]->Hijos[1] = Nodos[TargetId];
                Nodos[SourceId]->HijosId[1] = Nodos[TargetId]->Id;
                Nodos[TargetId]->Padre = Nodos[SourceId];
                Nodos[TargetId]->PadreId = Nodos[SourceId]->Id;
            }
            else{
                //no inserto
            }
        }
    }*/

    //a�andiendo nodos al procedular mesh
    for (int i = 0; i < Nodos.Num(); i++) {
        Nodos[i]->Padre = Nodos[Nodos[i]->PadreId];//para agregar la referencia, esto o se peude con el padre, por que en toeria aun no existe, habria que realizar una segunda pasada, alli podiasmos incluir esto para evtar algun fallo
        //asignar los respectivos padres.
        

        if (Nodos[i]->Valido) {//tiene hijos
            int ind;
            numerocolores.Find(Nodos[i]->ColorNum, ind);
            if (DataSetSeleccionado == 21) {
                ind = numerocolores.Num() - 1 - ind;//para el segundo conjunto
            }
            Nodos[i]->ColorNum = ind;
            Nodos[i]->Color = Colores[ind];//eliminar para darle un color uniforme a los nodos al inicio (negro)
            //Nodos[i]->Color = Colores[Nodos[i]->ColorNum];//eliminar para darle un color uniforme a los nodos al inicio (negro)
            //AddNodoToMesh(FVector(Nodos[i]->X, Nodos[i]->Y, Nodos[i]->Z), RadioNodos, Nodos[i]->Color, i);
        }
        /*else {
            //no tiene hijos es virtual
            Nodos[i]->Hijos[0]->Padre = Nodos[i];
            Nodos[i]->Hijos[0]->PadreId = Nodos[i]->Id;
            Nodos[i]->Hijos[1]->Padre = Nodos[i];
            Nodos[i]->Hijos[1]->PadreId = Nodos[i]->Id;
            //AddNodoToMesh(FVector(Nodos[i]->X, Nodos[i]->Y, Nodos[i]->Z), RadioNodosVirtuales, Nodos[i]->Color, i);
        }*/
        //AddNodoToMesh(FVector(Nodos[i]->X, Nodos[i]->Y, Nodos[i]->Z), Nodos[i]->Radio, Nodos[i]->Color, i);
    }
    //Nodos[Nodos.Num() - 1]->Padre = Nodos[Nodos.Num() - 2];
    //Nodos[Nodos.Num() - 1]->PadreId = Nodos[Nodos.Num() - 2]->Id;
    //Nodos[Nodos.Num() - 2]->Padre = Nodos[Nodos.Num() - 1];
    //Nodos[Nodos.Num() - 2]->PadreId = Nodos[Nodos.Num() - 1]->Id;
    //CreateNodosMesh();
    Calculos2();
    Calc();//no estaba antes
    //para calcular la informacon pertinente del arbol
    //y ya no calcularla al momento de calcular los layouts
}

void ANJVRVisualization::CreateVisualNodos() {
    for (int i = 0; i < Nodos.Num(); i++) {
        if (Nodos[i]->Valido) {//aqui a los nodos reales se le debe asiganar algun colo de acerud a algun criterio, por ahora dejar asi
            Nodos[i]->Radio = RadioNodos;//esto es visual, hacerlo aquia, pero depues
            Nodos[i]->SetRadio(RadioNodos);
        }
        else {
            Nodos[i]->Radio = RadioNodosVirtuales;
            Nodos[i]->SetRadio(RadioNodosVirtuales);
            //Nodos[i]->SetRadio(0.01f);
        }
    }
    //debo crear y asignar las secciones, para luego agregar nodos a cada seccion
    Calculos2();
    Calc();//no estaba antes

    UWorld * const World = GetWorld();
    if (World) {
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = GetInstigator();

        NodosVisuales = World->SpawnActor<ASpheresDrawer>(ASpheresDrawer::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
        NodosVisuales->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
    }

    AsignarSeccionesNodos();//aqui ya esty agregand los nodos, a los arrays
    //usar update nodos mesh
    NodosVisuales->Update();
    //CreateNodosMesh();
}

void ANJVRVisualization::CreateAristas() {
    int count = 0;
    for (int i = 0; i < Nodos.Num(); i++) {
        if (!Nodos[i]->Valido) {
            for (int j = 0; j < 2 /*Nodos[i]->Sons.Num()*/; j++) {
                int padre = i;
                int hijo = Nodos[i]->HijosId[j];


                FVector Diferencia = Nodos[hijo]->GetLocation() - Nodos[padre]->GetLocation();
                FVector Direccion = Diferencia.GetClampedToSize(1.0f, 1.0f);
                //FVector SpawnLocation(Diferencia/2 + Nodos[padre]->GetActorLocation());//ejes invertidos a los recibidos, esto tal vez deba ser la posicion relaitva, no la general
                FVector SpawnLocation(Diferencia/2 + Nodos[padre]->GetLocation() - GetActorLocation());//ejes invertidos a los recibidos, esto tal vez deba ser la posicion relaitva, no la general
                //necesito las posiones de los nodos relativas a esta clase
                SpawnLocation = GetTransform().TransformPosition(SpawnLocation);
                FRotator SpawnRotation(0.0f, 0.0f, 0.0f);

                Arista * AristaInstanciado = new Arista ();
                AristaInstanciado->Id = count;
                AristaInstanciado->SetSourceNodo(padre, Nodos[padre]);
                AristaInstanciado->SetTargetNodo(hijo, Nodos[hijo]);
                AristaInstanciado->Escala = Escala;
                AristaInstanciado->Radio = RadioAristas;
                AristaInstanciado->ActualRadio = RadioAristas;
                AristaInstanciado->bVisible = true;
                //AristaInstanciado->ActualRadio = 0.01f;

                Aristas.Add(AristaInstanciado);
                count++;
            }
        }
    }
    //instancia de la ultima arista
    int padre = Nodos.Num() - 1;
    int hijo = Nodos[Nodos.Num() - 1]->PadreId;
    FVector Diferencia = Nodos[hijo]->GetLocation() - Nodos[padre]->GetLocation();
    FVector Direccion = Diferencia.GetClampedToSize(1.0f, 1.0f);
    FVector SpawnLocation(Diferencia/2 + Nodos[padre]->GetLocation());//ejes invertidos a los recibidos
    //FRotator r(angle, 0.0f, 0.0f);
    //FRotator r(0.0f, angle, 0.0f);//rotacion con el eje up
    //UE_LOG(LogClass, Log, TEXT("Arista id = %d, angle= %f, sing = %f"), count, angle, sing);
    float angle = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(FVector::UpVector, Direccion)));
    float sing = FVector::CrossProduct(FVector::UpVector, Direccion).X;//esto es por que el signo es impotante para saber si fue un angulo mayor de 180 o no
    if (sing >= 0) {
        angle = 360-angle;
    }
    FRotator SpawnRotation(0.0f, 0.0f, angle);

    Arista * AristaInstanciado = new Arista();
    AristaInstanciado->Id = count;
    AristaInstanciado->SetSourceNodo(padre, Nodos[padre]);
    AristaInstanciado->SetTargetNodo(hijo, Nodos[hijo]);
    AristaInstanciado->Escala = Escala;
    AristaInstanciado->Radio = RadioAristas;
    AristaInstanciado->ActualRadio = RadioAristas;
    AristaInstanciado->bVisible = true;
    //AristaInstanciado->ActualRadio = 0.01f;

    Aristas.Add(AristaInstanciado);
    count++;

    //a�andiendo nodos al procedular mesh
    /*UWorld * const World = GetWorld();
    if (World) {
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = Instigator;

        AristasVisuales = World->SpawnActor<ACylindersDrawer>(ACylindersDrawer::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
        AristasVisuales->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
    }
    int AristasLayerNormal = AristasVisuales->CreateMesh();
    int AristasSeccion = AristasVisuales->CreateSeccionInMesh(AristasLayerNormal);
    for (int i = 0; i < Aristas.Num(); i++) {*/
        //Aristas[i]->DrawNormal = AristasVisuales->AddCylinderToMeshSeccion(AristasLayerNormal, AristasSeccion, i, Aristas[i]->GetSourcePosition(), Aristas[i]->GetTargetPosition(), Aristas[i]->ActualRadio/* RadioAristas*/, FLinearColor(0.75f, 0.75f, 0.75f, 1.0f));
    //}
    //AristasVisuales->Update();
    //CreateAristasMesh();
}

void ANJVRVisualization::CreateVisualAristas() {
    //a�andiendo nodos al procedular mesh
    UWorld * const World = GetWorld();
    if (World) {
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = GetInstigator();

        AristasVisuales = World->SpawnActor<ACylindersDrawer>(ACylindersDrawer::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
        AristasVisuales->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
    }
    AsignarSeccionesAristas();
    AristasVisuales->Update();
    //CreateAristasMesh();
}

void ANJVRVisualization::DestroyNodos() {
	Super::DestroyNodos();
    //Destruir nodos visuales
    //NodosVisuales->CleanAll();
}

void ANJVRVisualization::DestroyVisualNodos() {
}

void ANJVRVisualization::DestroyAristas() {
	Super::DestroyAristas();

	UpdateAristasMesh();

}

void ANJVRVisualization::AplicarLayout() {//aplica el layout de forma instantaneta
    LayoutDistanciaAumentadaAnguloReducido();//acoplado bonito
    ActualizarLayout();
    //los nodos y aristas debe estar en el tam�o adecuado
    //pero tambien debe 
}

void ANJVRVisualization::AplicarLayoutConAnimacion() {
    //aplica el layout ccon animacion
    //va de su posicion actual ahcia el nuevo layout
}

void ANJVRVisualization::EjecutarAnimacionCreacionArbol() {
    bAnimatingConstruction = true;
    //encolar el primer nodo virtual, o el primer indicie en una variable
    CurrentNodeConstruction = Nodos.Num()/2 + 1;//el primer virtual
    TimeStepConstruction = 0.05f;
    ActualTimeStepConstruction = 0.0f;
    //ejecuto la animacion del primero
    //animacino Step
    //agrandar el nodo virtual
    //mover a los hijos a usus posiciones
    //agrandar las aristas//solo que no se que aristas son las que debo mover!!, salvo que las cree tambien en un oden particular, con lo cual serian de 2 en dos
    //Nodos[CurrentNodeConstruction]->GrowToRadio(RadioNodos);//el debe estar ubicado exactamente donde debe
    Nodos[CurrentNodeConstruction]->GrowToRadio(RadioNodosVirtuales);//el debe estar ubicado exactamente donde debe
    Nodos[CurrentNodeConstruction]->SetLocation(Nodos[CurrentNodeConstruction]->GetLayoutLocation());//el debe estar ubicado exactamente donde debe
    if (Nodos[CurrentNodeConstruction]->Hijos[0]->Valido) {
        Nodos[CurrentNodeConstruction]->Hijos[0]->MoveToLocation(Nodos[CurrentNodeConstruction]->Hijos[0]->GetLayoutLocation());
    }
    if (Nodos[CurrentNodeConstruction]->Hijos[1]->Valido) {
        Nodos[CurrentNodeConstruction]->Hijos[1]->MoveToLocation(Nodos[CurrentNodeConstruction]->Hijos[1]->GetLayoutLocation());
    }

    int IdAritasReferenica = CurrentNodeConstruction - (Nodos.Num() / 2 + 1);
    Aristas[IdAritasReferenica]->bVisible = true;
    Aristas[IdAritasReferenica + 1]->bVisible = true;
    Aristas[IdAritasReferenica]->ConectSourceWithTargetAnimation();
    Aristas[IdAritasReferenica + 1]->ConectSourceWithTargetAnimation();
    Aristas[IdAritasReferenica]->GrowToRadio(RadioAristas);
    Aristas[IdAritasReferenica + 1]->GrowToRadio(RadioAristas);

}

void ANJVRVisualization::Calculos(Nodo * V) {//lo uso dentro de claculos 2, por alguna razon
    //calcular hojas, altura,
    //if (V->Hijos[0] == nullptr /*Sons.Num() == 0*/) {//deberia usar si es virtual o no
    if(V->Valido){
        V->Hojas = 1;
        V->Altura = 0;
        V->TamRama = 1;
    }
    else {
        V->Hojas = 0;
        V->Altura = 0;
        V->TamRama = 0;
        for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {
            Calculos(V->Hijos[i]);
            V->Hojas += V->Hijos[i]->Hojas;
            V->Altura = FMath::Max<float>(V->Altura, V->Hijos[i]->Altura);
            V->TamRama += V->Hijos[i]->TamRama;
        }
        V->Altura++;
        V->TamRama++;
    }
    //si el arbol tiene 4 niveles, el valor de altura de la raiz es 3
}

void ANJVRVisualization::Calculos2() {//calcula hojas y altura, de otra forma
    Nodo * Root = Nodos[Nodos.Num() - 1];
    Calculos(Root->Padre);
    Root->Altura = Root->Padre->Altura;
    Root->Hojas = Root->Padre->Hojas;
    Root->TamRama = Root->Padre->TamRama;
    for (int i = 0; i < 2 /*Root->Sons.Num()*/; i++) {
        Calculos(Root->Hijos[i]);
        Root->Hojas += Root->Hijos[i]->Hojas;
        Root->Altura = FMath::Max<float>(Root->Altura, Root->Hijos[i]->Altura);
        Root->TamRama += Root->Hijos[i]->TamRama;
    }
    Root->Altura++;
    Root->TamRama++;
}

void ANJVRVisualization::Calc() {//para hallar niveles
    std::stack<Nodo *> pila;
    //la raiz es el ultimo nodo
    Nodo * Root = Nodos[Nodos.Num() - 1];
    Root->Nivel = 0;
    //pila.push(Root);//no deberia dsencolarlo
    int hojas = 0;
    Root->Padre->Nivel = 1;
    pila.push(Root->Padre);
    Root->Hijos[1]->Nivel = 1;
    pila.push(Root->Hijos[1]);
    Root->Hijos[0]->Nivel = 1;
    pila.push(Root->Hijos[0]);
    while (!pila.empty()) {
        Nodo * V = pila.top();
        pila.pop();
        //if (V->Sons.Num()) {
        if (!V->Valido) {
            for (int i = 1 /*V->Sons.Num()-1*/; i >= 0; i--) {
                V->Hijos[i]->Nivel = V->Nivel + 1;
                pila.push(V->Hijos[i]);
            }
        }
        else {
            V->Casilla = hojas;
            hojas++;
        }
    }
}

void ANJVRVisualization::LayoutBase() {
    UNJTreeLayoutLibrary::LayoutBase(Nodos, RadioHoja);
}

//probar otros enoque para la asignacion del radio, quiza que vata de abajo hacia arriba, asi todas las hojas, tienen la arista de la misma distancia con su padre.
//corregir el erro que aparece por ejemplo con el conjunto 5, leafshapte que no posee una orientacioncorrecta.
void ANJVRVisualization::LayoutDistanciaReducida() {
    UNJTreeLayoutLibrary::LayoutDistanciaReducida(Nodos, RadioHoja);
}

void ANJVRVisualization::LayoutDistanciaAumentada() {
    UNJTreeLayoutLibrary::LayoutDistanciaAumentada(Nodos, RadioHoja, DeltaDistanciaArista);
}

void ANJVRVisualization::LayoutDistanciaAumentadaAnguloReducido() {
    UNJTreeLayoutLibrary::LayoutDistanciaAumentadaAnguloReducido(Nodos, RadioHoja, DeltaDistanciaArista);
}

void ANJVRVisualization::LayoutDistanciaAumentadaPromedioAnguloReducidoPorAltura() {
    UNJTreeLayoutLibrary::LayoutDistanciaAumentadaPromedioAnguloReducidoPorAltura(Nodos, RadioHoja, 6.0);
}

void ANJVRVisualization::LayoutDistanciaAumentadaPromedioAnguloReducidoPorAlturaIndependienteHijo() {
    UNJTreeLayoutLibrary::LayoutDistanciaAumentadaPromedioAnguloReducidoPorAlturaIndependienteHijo(Nodos, RadioHoja, 6.0);
}

void ANJVRVisualization::LayoutDistanciaAumentadaProgresivaAnguloReducido() {
    UNJTreeLayoutLibrary::LayoutDistanciaAumentadaProgresivaAnguloReducido(Nodos, RadioHoja, DeltaDistanciaArista);
}

void ANJVRVisualization::LayoutDistanciaAumentadaPromedioAnguloReducido() {
    UNJTreeLayoutLibrary::LayoutDistanciaAumentadaPromedioAnguloReducido(Nodos, RadioHoja, 6.0);
}

void ANJVRVisualization::LayoutDistanciaAumentadaAnguloReducidoSinIntercalamiento() {
    UNJTreeLayoutLibrary::LayoutDistanciaAumentadaAnguloReducidoSinIntercalamiento(Nodos, RadioHoja, DeltaDistanciaArista);
}

void ANJVRVisualization::LayoutDistanciaAumentadaHijoAnguloReducido() {
    //UNJTreeLayoutLibrary::LayoutDistanciaAumentadaHijoAnguloReducido(Nodos, RadioHoja, DeltaDistanciaArista);
    // con delta distancia de aristas con valor de 4.0 funciona muy bien
    UNJTreeLayoutLibrary::LayoutDistanciaAumentadaHijoAnguloReducido(Nodos, RadioHoja, 6.0f);
}

void ANJVRVisualization::LayoutMesa() {
    FVector Offset = GetActorLocation() * -1;
    Offset = GetTransform().InverseTransformVector(Offset);
    UNJTreeLayoutLibrary::LayoutMesa(Nodos, RadioNodos, Offset);
}

void ANJVRVisualization::LayoutEsferico() {
    UNJTreeLayoutLibrary::LayoutEsferico(Nodos, 300.0f, 0, PI);
}

void ANJVRVisualization::LayoutEsfericoLadrillos() {
    float PhiMax = FMath::DegreesToRadians(150);
    float PhiMin = FMath::DegreesToRadians(40);

    UNJTreeLayoutLibrary::LayoutEsfericoLadrillos(Nodos, 300.0f, PhiMin, PhiMax);
}

void ANJVRVisualization::LayoutRadial2D() {
    UNJTreeLayoutLibrary::LayoutRadial2D(Nodos, 20.0f);
}

void ANJVRVisualization::LayoutRadial3D() {
    UNJTreeLayoutLibrary::LayoutRadial3D(Nodos, 20.0f);
}

void ANJVRVisualization::ActualizarLayout() {//este actulizar deberia ser general
    for (int i = 0; i < Nodos.Num(); i++) {//debo copiar la info a actual
        FVector NuevaPosicion;
        NuevaPosicion.X = Nodos[i]->X;
        NuevaPosicion.Y = Nodos[i]->Y;
        NuevaPosicion.Z = Nodos[i]->Z;
        UE_LOG(LogClass, Log, TEXT("Nodo id = %d, (%f,%f,%f)"), Nodos[i]->Id, NuevaPosicion.X, NuevaPosicion.Y, NuevaPosicion.Z);
        Nodos[i]->SetLocation(NuevaPosicion);
        Nodos[i]->SetRadio(Nodos[i]->Radio);
        NodosVisuales->UpdatePosicionAndRadioSphereInMeshSeccion(Nodos[i]->DrawNormal, NuevaPosicion, Nodos[i]->Radio);
        //aqui debo pasar actualizar la posicion de un nodo, pero al inicio puedo ya crear un mesh
    }
    for (int i = 0; i < Aristas.Num(); i++) {
        //las pondre visibles con su radio maximo

        Aristas[i]->bVisible = true;
        Aristas[i]->ActualRadio = RadioAristas;
        //UE_LOG(LogClass, Log, TEXT("AristaId = %d [Source %d (%f, %f, %f) - Target %d (%f, %f, %f)"), Aristas[i]->Id, Aristas[i]->SourceNodo->Id, Aristas[i]->SourceNodo->Xcoordinate, Aristas[i]->SourceNodo->Ycoordinate, Aristas[i]->SourceNodo->Zcoordinate, Aristas[i]->TargetNodo->Id, Aristas[i]->TargetNodo->Xcoordinate, Aristas[i]->TargetNodo->Ycoordinate, Aristas[i]->TargetNodo->Zcoordinate);

        AristasVisuales->UpdatePosicionAndRadioCylinderInMeshSeccion(Aristas[i]->DrawNormal, Aristas[i]->GetSourcePosition(), Aristas[i]->GetTargetPosition(), Aristas[i]->ActualRadio);
    }
    //NodosVisuales->Update();
    UpdateNodosMesh();//esto contiene la linea anterior
    UpdateAristasMesh();

}

//funciones para el procedural mesh
void ANJVRVisualization::AsignarSeccionesNodos() {
    //debo crear un mesh o layer principal para los nodos normales
    LayerNormal = NodosVisuales->CreateMesh();
    // y alli empezar a crear las secciones
	int IdNewSeccion = SeccionNodos.AddDefaulted();//guarda los nodos que pertenencen a cada seccion
    //estos dos deberian ser iguales siempre
    int RootSeccion = NodosVisuales->CreateSeccionInMesh(LayerNormal);

    //cuando creo una seccion tambien debo crear el array de nodos que esta clase tiene para almancenar los punteros a los nodos pertenecientes, asi relacions esos nodos con los de la visualizacion
    Nodo * Root = Nodos[Nodos.Num() - 1];
    AddNodoToMeshSeccion(LayerNormal, RootSeccion, Root); //agrego al razi a la seccion 0
    //dentro de esta funcion se deberian llenar los id pertinentes
    //Root->SeccionArbol = 0;
    //Root->IdSeccionArbol = 0;
    TQueue<pair<Nodo *, int>> Cola;// para el bfs, necesita el nodo y la seccion a al que se quiere unir, la cual seria la seccion del padre
    //si los nodos ya son asignaods a un mehs, quiere decir que ya tiene una referencia
    if (Root->TamRama <= (UmbralSizeNodosProceduralMesh - SeccionNodos[Root->DrawNormal.IdSection].Num())) {
        //meter todo el arbol en la seccion 0
        //poner todo los hijos aqui
        AddRamaToMeshSeccion(LayerNormal, RootSeccion, Root->Padre);
        AddRamaToMeshSeccion(LayerNormal, RootSeccion, Root->Hijos[0]);
        AddRamaToMeshSeccion(LayerNormal, RootSeccion, Root->Hijos[1]);
    }
    else {//es mas grande que el espacio disponible en la seccion
        //encolamos a sus hijos
        //Cola.Enqueue(pair<Nodo *, int>(Root->Padre, Root->SeccionArbol));
        //Cola.Enqueue(pair<Nodo *, int>(Root->Hijos[0], Root->SeccionArbol));
        //Cola.Enqueue(pair<Nodo *, int>(Root->Hijos[1], Root->SeccionArbol));
        Cola.Enqueue(pair<Nodo *, int>(Root->Padre, Root->DrawNormal.IdSection));
        Cola.Enqueue(pair<Nodo *, int>(Root->Hijos[0], Root->DrawNormal.IdSection));
        Cola.Enqueue(pair<Nodo *, int>(Root->Hijos[1], Root->DrawNormal.IdSection));

    }
    while (!Cola.IsEmpty()) {
        pair<Nodo *, int> P;
        Cola.Dequeue(P);
        if (P.first->TamRama <= (UmbralSizeNodosProceduralMesh - SeccionNodos[P.second].Num())) {
            //meter todo el arbol en la seccion 0
            AddRamaToMeshSeccion(LayerNormal, P.second, P.first);
        }
        else {//es mas grande que el espacio disponible en la seccion
            //encolamos a sus hijos
            if (SeccionNodos[P.second].Num() < UmbralSizeNodosProceduralMesh) {
                //agregar P.first al la seccion P.second
                AddNodoToMeshSeccion(LayerNormal, P.second, P.first);
                //encolar los hijos con la seccion P.second
                if (P.first->Hijos[0]) {
                    Cola.Enqueue(pair<Nodo *, int>(P.first->Hijos[0], P.second));//podria ir la seccion del nodo, es decir, P.first->SeccionArbol, ya que el nodo ya le asigne una seccion en la linea anterior
                    Cola.Enqueue(pair<Nodo *, int>(P.first->Hijos[1], P.second));
                }
            }
            else {
                IdNewSeccion = SeccionNodos.AddDefaulted();//guarda los nodos que pertenencen a cada seccion
                //estos dos deberian ser iguales siempre
                int VNewSeccion = NodosVisuales->CreateSeccionInMesh(LayerNormal);
                //debo aasignar el material a esta nueva seccion
                //crear una nueva seccion, y meto alli al nodo actual
                AddNodoToMeshSeccion(LayerNormal, IdNewSeccion, P.first);
                if (P.first->Hijos[0]) {
                    Cola.Enqueue(pair<Nodo *, int>(P.first->Hijos[0], IdNewSeccion));//podria ir la seccion del nodo, es decir, P.first->SeccionArbol, ya que el nodo ya le asigne una seccion en la linea anterior
                    Cola.Enqueue(pair<Nodo *, int>(P.first->Hijos[1], IdNewSeccion));
                }
                //encolo los dos hijos con la nueva seccion.
            }
            //los encolamientos pueden ir aca afuera
            //Cola.Enqueue(pair<Nodo *, int>(Root->Hijos[0], Root->SeccionArbol));
            //Cola.Enqueue(pair<Nodo *, int>(Root->Hijos[1], Root->SeccionArbol));

        }
    }
    
}

int ANJVRVisualization::CreateSeccion() {
    //puedo conservar esta seccion para encapsular la creacion de secciones en el nodo,y en el array de referencia de nodos
    //crea los arrays enecearios para una nueva seccion, devuelve el indice de esta nueva seccion
	int IdNewSeccion = SeccionNodos.AddDefaulted();//guarda los nodos que pertenencen a cada seccion
    /*NeedUpdateSeccionNodos.Add(false);
    NeedRecreateSeccionNodos.Add(false);
	SeccionVerticesNodos.AddDefaulted();
	SeccionVerticesPNodos.AddDefaulted();
	SeccionTrianglesNodos.AddDefaulted();
	SeccionNormalsNodos.AddDefaulted();
	SeccionUV0Nodos.AddDefaulted();
	SeccionTangentsNodos.AddDefaulted();
	SeccionVertexColorsNodos.AddDefaulted();*/
    //UE_LOG(LogClass, Log, TEXT("Creando seccion: %d"), IdNewSeccion);
    return IdNewSeccion;
}

void ANJVRVisualization::AddNodoToMeshSeccion(int IdMesh, int IdSeccion, Nodo * Node) {//la seccion ya deberia estar creada
    //en esta funcion debo llenar la seccion y el id dentro de la seecion del nodo
    int IdSeccionNodo = SeccionNodos[IdSeccion].Add(Node);
    //Node->SeccionArbol = Seccion;
    //Node->IdSeccionArbol = IdSeccionNodo;
    //hacer lo mismo para el resto de arrays, los cuales deberian ser usando la funcoin addnoto to meshseccion, similar a addnodo to mesh
    Node->DrawNormal = NodosVisuales->AddSphereToMeshSeccion(IdMesh, IdSeccion, IdSeccionNodo, Node->GetLayoutLocation(), Node->Radio, Node->Color);
    //agrego el nodo a los arrays para que luego los pase al procedural mesh
}

void ANJVRVisualization::AddRamaToMeshSeccion(int IdMesh, int IdSeccion, Nodo * Node) {
    //podria usar la funcon addnodotoseccion
    TQueue<Nodo *> Cola;
    Cola.Enqueue(Node);
    while (!Cola.IsEmpty()) {
        Nodo * N;
        Cola.Dequeue(N);
        AddNodoToMeshSeccion(IdMesh, IdSeccion, N);
        if (N->Hijos[0]) {//tiene un puntero a un hijo
            Cola.Enqueue(N->Hijos[0]);
            Cola.Enqueue(N->Hijos[1]);
        }
    }
}

void ANJVRVisualization::UpdateNodosMesh() {//ahora debo hacer update solo de las secciones que han sufrido modificacion, quiza deberia llamar a esta funcion en todo los frames, asi me olvido de estar pensando en que momento llamarla y en cual no
    NodosVisuales->Update();
}

int ANJVRVisualization::CreateSeccionNodoSelected() {
    LayerSelected = NodosVisuales->CreateMesh();
    NodosVisuales->CreateSeccionInMesh(LayerSelected);
    NodosVisuales->ActivateDepthStencilOnLayer(LayerSelected);
    NodosVisuales->SetDepthStencil(LayerSelected, 255);
    //crear seccion en el procedural mesh tambien 
	int IdNewSeccion = SeccionNodosSelected.AddDefaulted();//guarda los nodos que pertenencen a cada seccion
    NodosVisuales->SetMaterialSeccionMesh(LayerSelected, IdNewSeccion, NodosSelectedMeshMaterial);
    return IdNewSeccion;
}

void ANJVRVisualization::CreateMeshesResaltados() {
    for (int i = 0; i < 8; i++) {
        int LayerResaltado = NodosVisuales->CreateMesh();
        NodosVisuales->ActivateDepthStencilOnLayer(LayerResaltado);
		NodosVisuales->SetDepthStencil(LayerResaltado, 255 - (8-i));
        LayersResaltado.Add(LayerResaltado);
		int IdNewSeccion = SeccionNodosResaltados.AddDefaulted();//guarda los nodos que pertenencen a cada seccion
        NodosVisuales->CreateSeccionInMesh(LayerResaltado);//solo tengo una seccion en cada capa de estas
    }
}

void ANJVRVisualization::AddAristaToMesh(FVector Source, FVector Target, int Radio, int NumArista) {
    //precision debe ser mayor a 3, represante el numero de lados del cilindro
    //la precision deberia ser de forma general,asi lo puedo usar para determinar la cantidad de vertices a�adidos
    //NECESITARE UNA FUNCION ADD ARISTA TO MESH SECCION, IGUAL QUE LA DE NODOS
}

void ANJVRVisualization::AsignarSeccionesAristas() {
    // creara arrays igual que los arrays de los nodos
    // iterar por las aristas, exlorar los nodos target, estos marcan a que array pertenece
    SeccionAristas.SetNum(SeccionNodos.Num());//no se si esto sea necesario

    //crear un mser principar para las aristas normales
    int AristasLayerNormal = AristasVisuales->CreateMesh();
    for (int i = 0; i < SeccionAristas.Num(); i++) {//crea todas las secciones, igual que los nodos
        int AristasSeccion = AristasVisuales->CreateSeccionInMesh(AristasLayerNormal);
    }
    for (int i = 0; i < Aristas.Num(); i++) {
        int AristaSeccion = Aristas[i]->TargetNodo->DrawNormal.IdSection;
        int idAristaSeccion = SeccionAristas[AristaSeccion].Add(Aristas[i]);
        Aristas[i]->DrawNormal = AristasVisuales->AddCylinderToMeshSeccion(AristasLayerNormal, AristaSeccion, idAristaSeccion, Aristas[i]->GetSourcePosition(), Aristas[i]->GetTargetPosition(), Aristas[i]->ActualRadio/* RadioAristas*/, FLinearColor(0.75f, 0.75f, 0.75f, 1.0f));
    }
}

void ANJVRVisualization::UpdateAristasMesh() {
	//AristasMesh->UpdateMeshSection_LinearColor(0, VerticesAristas, NormalsAristas, UV0Aristas, VertexColorsAristas, TangentsAristas);
    AristasVisuales->Update();
}

void ANJVRVisualization::ColorearNodo(Nodo * Node, FLinearColor NuevoColor) {
	NodosVisuales->UpdateColorSphereInMeshSeccion(Node->DrawNormal, NuevoColor);
    /*for (int i = 0; i < VertexColorsNodoTemplate.size(); i++) {
        SeccionVertexColorsNodos[Node->SeccionArbol][i + Node->IdSeccionArbol * VertexColorsNodoTemplate.size()] = NuevoColor;
        //quiza deberia guardar el color tambien en el nodo, que tenga un color actual tal vez
    }*/
    //UpdateNodosMesh();//despues de colorear deberia hacer los updates, el problema es colorear rama, que hace un varios cmbios de color, y no deberia hacer un update tantes veces, cuando podria hacer solo uno al final de todo
}

void ANJVRVisualization::ColorearRama(Nodo * NodoSeleccionado, FLinearColor NuevoColor) {
    Super::ColorearRama(NodoSeleccionado, NuevoColor);
    UpdateNodosMesh();
}

void ANJVRVisualization::TrasladarNodo(Nodo * Node, FVector VectorTraslacion) {//esta traslacion la debe aplicar en funcion de si esta seleccionado o no
	Node->SetLocation(Node->GetLocation() + VectorTraslacion);
	//UpdatePosicionNodoMesh(Node->Id, Node->GetLocation());
    if (Node->Selected) {
        //UpdatePosicionNodoSeccionSeleccionMesh(Node->SeccionSeleccion, Node->IdSeccionSeleccion, Node->GetLocation());
        NodosVisuales->UpdatePosicionAndRadioSphereInMeshSeccion(Node->DrawSeleccionado, Node->GetLocation(), Node->Radio);
    }
    else {
        //UpdatePosicionNodoSeccionMesh(Node->SeccionArbol, Node->IdSeccionArbol, Node->GetLocation());
        NodosVisuales->UpdatePosicionAndRadioSphereInMeshSeccion(Node->DrawNormal, Node->GetLocation(), Node->Radio);
    }
    //for (int i = 0; i < VerticesNodoTemplate.size(); i++) {
        //VerticesNodos[i + Node->Id * VerticesNodoTemplate.size()] += VectorTraslacion;
    //}
}

void ANJVRVisualization::SetLocationNodo(Nodo * Node, FVector NewLocation) {//no se esta usando ya que la posicion del nodo se esta estableciendo internamente en Nodo, ya no necesito usar set location
	Node->SetLocation(NewLocation);
    NodosVisuales->UpdatePosicionAndRadioSphereInMeshSeccion(Node->DrawNormal, Node->GetLocation(), Node->GetRadio());
	//UpdatePosicionNodoMesh(Node->Id, NewLocation);
}

/*FVector ANJVRVisualization::BuscarNodo(int & IdNodoEncontrado) {
    FCollisionQueryParams NodoTraceParams = FCollisionQueryParams(FName(TEXT("TraceNodo")), true, nullptr);
    NodoTraceParams.bTraceComplex = true;
    NodoTraceParams.bReturnFaceIndex = true;
    FVector PuntoInicial = RightController->GetComponentLocation();//lo mismo que en teorioa, GetComponentTransfor().GetLocation();
    FVector Vec = RightController->GetForwardVector();
    FVector PuntoFinal = PuntoInicial + Vec*DistanciaLaserMaxima;
    //PuntoInical = PuntoInicial + Vec * 10;//para que no se choque con lo que quiero, aun que no deberia importar
    TArray<TEnumAsByte<EObjectTypeQuery> > TiposObjetos;
    //TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery7);//Nodo
    TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery1);//World dynamic, separado esta funcionando bien, supongo que tendre que hacer oto trace par saber si me estoy chocando con la interfaz, y no tener encuenta esta busqueda
    TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery2);//World dynamic, separado esta funcionando bien, supongo que tendre que hacer oto trace par saber si me estoy chocando con la interfaz, y no tener encuenta esta busqueda
    //podria agregar los world static y dynamic, para asi avitar siempre encontrar algun nodo que este destrar de algun menu, y que por seleccionar en el menu tambien le de click a el
    TArray<AActor*> vacio;
    FHitResult Hit;
    //bool trace = UKismetSystemLibrary::LineTraceSingleForObjects(GetWorld(), PuntoInicial, PuntoFinal, TiposObjetos, true, vacio, EDrawDebugTrace::ForOneFrame, Hit, false, FLinearColor::Blue);//el none es para que no se dibuje nada
    bool trace = NodosMesh->LineTraceComponent(Hit, PuntoInicial, PuntoFinal, NodoTraceParams);
    //hit se supone que devovera al actor y el punto de impacto si encontr� algo, castearlo a nodo, y listo
    if (trace) {
        //tengo que comprobar que sea una visualizacion, ya que el mesh es de una visualizacion
        //solo que al agregar el worldynamic ,tengo que castear y verificar
        //ahora debo ver que triangulo es, dividiendo el id del triangulo entre el numero de triangulos del template
        //IdNodoEncontrado = Hit.FaceIndex / TriangulosNodoTemplate.size();
        IdNodoEncontrado = Hit.FaceIndex;
        /*if (NodoEncontrado) {//no estaba
            //en que momento debo incluir la seccion del label? despues de todo esto, en otra funcion, o en este mismo codigo?
            return Hit.ImpactPoint;
        }* /
        //DrawDebugSphere(GetWorld(), Hit.ImpactPoint, 1.5f, 6, FColor::Black, false, 0.0f, 0, 0.25f);
        
        return Hit.ImpactPoint;//si quito toto el if anterior debo activar esta linea, y liesto
    }
    //y si esta funcion es solo para esto, y luego ya verifico si es tal o cual cosa, en otra parte del codigo?

    //DrawDebugLine(GetWorld(), SourceNodo->GetActorLocation(), TargetNodo->GetActorLocation(), FColor::Black, false, -1.0f, 0, Radio*Escala);
    IdNodoEncontrado = -1;
    return FVector::ZeroVector;// los casos manejarlos afuera
}*/

FVector ANJVRVisualization::BuscarNodo(int & IdNodoEncontrado) {
    //guardo todos y me quedo con el mas cercano al usuario
    // o paso por todos y busco el minimo
    IdNodoEncontrado = -1;
    FVector IntereseccionMinima = FVector::ZeroVector;
    float TMin;
    //necesito quedarme con el que tenga menos distancia con el control, lo que podria ser el que tenga el menor t, entonces en lugar de la interseccion podria retornar el t, para evitar hacer calculos
    for (int i = 0; i < Nodos.Num(); i++) {
        FVector Interseccion;
        float T;
        if (InterseccionConNodo(Nodos[i], Interseccion, T)) {
            if (IdNodoEncontrado == -1 || T < TMin) {
                TMin = T;
                IdNodoEncontrado = i;
                IntereseccionMinima = Interseccion;
            }
        }
    }
    //IdNodoEncontrado = -1;
    return IntereseccionMinima;
}

bool ANJVRVisualization::InterseccionConNodo(Nodo * Node, FVector & Interseccion, float & T) {//distancia de laser infinita
    //no neceistaria retornar el bool, si t es negatibo es que no hubo interseccion, si es positibo hubo interseccion en ese T
    float Radio = Node->ActualRadio;
    /*if (Node->Valido) {
        Radio = RadioNodos;
    }
    else {
        Radio = RadioNodosVirtuales;
    }*/

    FVector Centro = Node->GetLocation();

    FVector Punto = RightController->GetComponentTransform().GetLocation();
    Punto = GetTransform().InverseTransformPosition(Punto);
    FVector Vector = RightController->GetForwardVector();
    Vector = GetTransform().InverseTransformVector(Vector);

    FVector S = Punto - Centro;

    float A = (Vector.X*Vector.X + Vector.Y*Vector.Y + Vector.Z*Vector.Z);//a
    float B = 2*(S.X *Vector.X + S.Y*Vector.Y + S.Z*Vector.Z);//2*b
    float C = (S.X*S.X + S.Y*S.Y + S.Z*S.Z)-(Radio*Radio);//c-R^2;, en realidad solo a este se le resta R^2
    float Discriminante = B*B - 4*A*C;
    //UE_LOG(LogClass, Log, TEXT("Punto = %f, %f, %f"), Punto.X, Punto.Y, Punto.Z);
    //UE_LOG(LogClass, Log, TEXT("Vector = %f, %f, %f"), Vector.X, Vector.Y, Vector.Z);
    //UE_LOG(LogClass, Log, TEXT("(A,B,C) = %f, %f, %f"), A, B, C);
    //UE_LOG(LogClass, Log, TEXT("Determinante = %f"), Discriminante);
    if (Discriminante < 0) {
        T = -1.0f;
        Interseccion = FVector::ZeroVector;
        return false;
        //DrawDebugLine(GetWorld(), Punto, Punto + DistanciaLaser*Vector, FColor::Red, false, -1.0f, 0, 1.0f);// los calculos estan perfectos
        //dibujar en rojo, el maximo alcance
    }
    else if (Discriminante > 0) {
        float raiz = FMath::Sqrt(Discriminante);
        float T1 = (-B + raiz) / (2 * A);
        float T2 = (-B - raiz) / (2 * A);
        //UE_LOG(LogClass, Log, TEXT("(T1,T2) = %f, %f"), T1, T2);
        FVector P1 = Punto + T1*Vector;
        FVector P2 = Punto + T2*Vector;
        if (T1 >= 0 && T2 >= 0) {
            if ((P1 - Punto).Size() < (P2 - Punto).Size()) {
                //DrawDebugLine(GetWorld(), Punto, P1, FColor::Blue, false, -1.0f, 0, 1.0f);// los calculos estan perfectos
                T = T1;
                Interseccion = P1;
                return true;
                //return GetTransform().InverseTransformPosition(P1);
            }
            else {
                T = T2;
                Interseccion = P2;
                return true;
                //DrawDebugLine(GetWorld(), Punto, P2, FColor::Blue, false, -1.0f, 0, 1.0f);// los calculos estan perfectos
                //return GetTransform().InverseTransformPosition(P2);
            }
        }
        else if (T1 >= 0) {
            T = T1;
            Interseccion = P1;
            return true;
            //return GetTransform().InverseTransformPosition(P1);
            //DrawDebugLine(GetWorld(), Punto, P1, FColor::Blue, false, -1.0f, 0, 1.0f);// los calculos estan perfectos
        }
        else if (T2 >= 0) {
            T = T2;
            Interseccion =  P2;
            return true;
            //return GetTransform().InverseTransformPosition(P2);
            //DrawDebugLine(GetWorld(), Punto, P2, FColor::Blue, false, -1.0f, 0, 1.0f);// los calculos estan perfectos
        }
    }
    else{
        T = (-B + FMath::Sqrt(Discriminante)) / (2 * A);
        //DrawDebugLine(GetWorld(), Punto, Punto + T*Vector, FColor::Green, false, -1.0f, 0, 1.0f);// los calculos estan perfectos
        //return GetTransform().InverseTransformPosition(Punto + T*Vector);
        Interseccion = Punto + T*Vector;
        return true;
    } 
    T = -1.0f;
    Interseccion = FVector::ZeroVector;
    return false;
}

void ANJVRVisualization::TraslacionConNodoGuia() {
    FVector PuntoInicial = GetTransform().InverseTransformPosition(RightController->GetComponentLocation());//lo mismo que en teorioa, GetComponentTransfor().GetLocation();
	//debo el punto del contrl y el vector a relativo de la visualizacion, ya no estoy trabajando con actores
    FVector Vec =  GetTransform().InverseTransformVector(RightController->GetForwardVector()).GetSafeNormal();
    FVector PuntoFinal = PuntoInicial + Vec*DistanciaLaser;
    if(Usuario->LaserActual() != 6){
        Usuario->CambiarLaser(6);
    }
    //Usuario->CambiarPuntoFinal(PuntoFinal);
	Usuario->CambiarPuntoFinal(GetTransform().TransformPosition(PuntoFinal));
	Usuario->EfectoImpacto->SetWorldLocation(GetTransform().TransformPosition(NodoGuia->GetLocation()));
	if (WidgetCompLabelNodoActual->IsVisible()) {
        WidgetCompLabelNodoActual->SetRelativeLocation(NodoGuia->GetLocation() + FVector(0.0f, 0.0f, RadioNodos));
	}

    AplicarTraslacion(PuntoFinal - NodoGuia->GetLocation());
	//ActualizarCambiosVisuales();//no siempre actualizo aristas, por lo tanto deben estar separadas, debo llamar solo lo que necesito, esta funcoin no sera necesaria
    UpdateNodosMesh();
	UpdateAristasMesh();
	//lamar en su lugar a update nodos mesh
	//si el nodo guia es valido, tiene label, entonces debo trasladarlo.
	
}

//void ANJVRVisualization::TrasladarRamaPressed() {
//}

void ANJVRVisualization::AplicarTraslacion(FVector Traslacion) {
	for (int i = 0; i < NodosSeleccionados.Num(); i++) {
		TrasladarNodo(NodosSeleccionados[i], Traslacion);
    }
	for (int i = 0; i < Aristas.Num(); i++) {//este puede ser un poco pensado
		if(Aristas[i]->NeedUpdate()){
            AristasVisuales->UpdatePosicionAndRadioCylinderInMeshSeccion(Aristas[i]->DrawNormal, Aristas[i]->SourceNodo->GetLocation(), Aristas[i]->TargetNodo->GetLocation(), Aristas[i]->ActualRadio);
		}
	}
	//despues de esto deberia hacer el updat
	//ActualizarCambiosVisuales();
}

void ANJVRVisualization::FeedbackSeleccionarNodo(Nodo * NodoSeleccionado) {//por ahora tratar como si solo tuviera una unica secion de nodos seleccionados, no se si valga la pena tener mas, ya que solo voy a hacer acciones sobre todos a lavez
    //copiar el nodo de la seccion normal a la seccion de seleccionadso, verificar si tengo que crear una nueva seccion de nodos seleccionados
    //esto copia su informacon de array a array
    int SeccionSeleccion = 0;
    int IdSeccionNodo = SeccionNodosSelected[SeccionSeleccion].Add(NodoSeleccionado);//agregando a la seccion
    NodoSeleccionado->DrawSeleccionado = NodosVisuales->AddSphereToMeshSeccion(LayerSelected, SeccionSeleccion, IdSeccionNodo, NodoSeleccionado->GetLocation(), NodoSeleccionado->Radio, NodoSeleccionado->Color);

    NodosVisuales->RemoveSphereInMeshSection(NodoSeleccionado->DrawNormal);
    for (int i = NodoSeleccionado->DrawNormal.IdElement + 1; i < SeccionNodos[NodoSeleccionado->DrawNormal.IdSection].Num(); i++) {
        SeccionNodos[NodoSeleccionado->DrawNormal.IdSection][i]->DrawNormal.IdElement--;
    }
    SeccionNodos[NodoSeleccionado->DrawNormal.IdSection].RemoveAt(NodoSeleccionado->DrawNormal.IdElement);//deberian conicnidir
    //actualizar los id de todos los nodos de la seccion normal
}

void ANJVRVisualization::FeedbackDeseleccionarNodo(Nodo * NodoSeleccionado) {
    //copiar el nodo de la seccion seleccionado a la seccion normal que tiene establecida, debo verificar que si todos los nodos entran en una seccion reducir las innecesarias
    int IdSeccionNodo = SeccionNodos[NodoSeleccionado->DrawNormal.IdSection].Add(NodoSeleccionado);//tenog un nuevo id en esta seccion
    NodoSeleccionado->DrawNormal = NodosVisuales->AddSphereToMeshSeccion(NodoSeleccionado->DrawNormal.IdLayer, NodoSeleccionado->DrawNormal.IdSection, IdSeccionNodo, NodoSeleccionado->GetLocation(), NodoSeleccionado->Radio, NodoSeleccionado->Color);
    NodosVisuales->RemoveSphereInMeshSection(NodoSeleccionado->DrawSeleccionado);

    for (int i = NodoSeleccionado->DrawSeleccionado.IdElement + 1; i < SeccionNodosSelected[NodoSeleccionado->DrawSeleccionado.IdSection].Num(); i++) {
        SeccionNodosSelected[NodoSeleccionado->DrawSeleccionado.IdSection][i]->DrawSeleccionado.IdElement--;
    }
    SeccionNodosSelected[NodoSeleccionado->DrawSeleccionado.IdSection].RemoveAt(NodoSeleccionado->DrawSeleccionado.IdElement);
}

void ANJVRVisualization::FeedbackResaltarNodo(Nodo * NodoResaltado, int ColorResaltado) {
	//si esta seleccionado lo remuevo de la seleccion, si no esta lo remuevo de los seleccionados
    int MeshResaltado = ColorResaltado;
    int IdResaltadoNodo = SeccionNodosResaltados[MeshResaltado].Add(NodoResaltado);//agregando a la seccion
    NodoResaltado->DrawResaltado = NodosVisuales->AddSphereToMeshSeccion(LayersResaltado[MeshResaltado], 0, IdResaltadoNodo, NodoResaltado->GetLocation(), NodoResaltado->Radio, NodoResaltado->Color);

	if (NodoResaltado->Selected) {
        NodosVisuales->RemoveSphereInMeshSection(NodoResaltado->DrawSeleccionado);
		for (int i = NodoResaltado->DrawSeleccionado.IdElement + 1; i < SeccionNodosSelected[NodoResaltado->DrawSeleccionado.IdSection].Num(); i++) {
			SeccionNodosSelected[NodoResaltado->DrawSeleccionado.IdSection][i]->DrawSeleccionado.IdElement--;
		}
		SeccionNodosSelected[NodoResaltado->DrawSeleccionado.IdSection].RemoveAt(NodoResaltado->DrawSeleccionado.IdElement);
		//actualizar los ids de todos los nodos array de nodos seleccionados de la seccion
	}
	else {
        NodosVisuales->RemoveSphereInMeshSection(NodoResaltado->DrawNormal);
		//asignar el id de seleccion al nodo
		//borrar del array de nodos normales
		for (int i = NodoResaltado->DrawNormal.IdElement + 1; i < SeccionNodos[NodoResaltado->DrawNormal.IdSection].Num(); i++) {
			SeccionNodos[NodoResaltado->DrawNormal.IdSection][i]->DrawNormal.IdElement--;
		}
		SeccionNodos[NodoResaltado->DrawNormal.IdSection].RemoveAt(NodoResaltado->DrawNormal.IdElement);
		//actualizar los id de todos los nodos de la seccion normal
	}
	//tambien deberia estar el caso en que este resaltado ya, con otro colore
}

void ANJVRVisualization::FeedbackDesresaltarNodo(Nodo * NodoResaltado, int ColorResaltado) {//hacia donde lo mando? seleccion o normal
	if(NodoResaltado->Selected){
		int SeccionSeleccion = 0;
		int IdSeccionNodo = SeccionNodosSelected[SeccionSeleccion].Add(NodoResaltado);//agregando a la seccion
		NodoResaltado->DrawSeleccionado = NodosVisuales->AddSphereToMeshSeccion(LayerSelected, SeccionSeleccion, IdSeccionNodo, NodoResaltado->GetLocation(), NodoResaltado->Radio, NodoResaltado->Color);
	}
	else {
		int IdSeccionNodo = SeccionNodos[NodoResaltado->DrawNormal.IdSection].Add(NodoResaltado);//tenog un nuevo id en esta seccion
		NodosVisuales->AddSphereToMeshSeccion(NodoResaltado->DrawNormal.IdLayer, NodoResaltado->DrawNormal.IdSection, IdSeccionNodo, NodoResaltado->GetLocation(), NodoResaltado->Radio, NodoResaltado->Color);
	}
    //asignar el el nuevo id del nodo en la seccion que tneia
    NodosVisuales->RemoveSphereInMeshSection(NodoResaltado->DrawResaltado);
    int IdMeshResaltado = LayersResaltado.Find(NodoResaltado->DrawResaltado.IdLayer);
    if (IdMeshResaltado != -1) {
        for (int i = NodoResaltado->DrawResaltado.IdElement + 1; i < SeccionNodosResaltados[IdMeshResaltado].Num(); i++) {//en realidad esl el id del array de los layer de resaltado, debo buscar alli en que layer estoy
            SeccionNodosResaltados[IdMeshResaltado][i]->DrawResaltado.IdElement--;
        }
        SeccionNodosResaltados[IdMeshResaltado].RemoveAt(NodoResaltado->DrawResaltado.IdElement);
    }
    //actualizar los ids de todos los nodos array de nodos seleccionados de la seccion
}

void ANJVRVisualization::RightPressed() {
}

void ANJVRVisualization::RightReleased() {
    bRightHold = false;
    if (CurrentVisualizationTask == EVRVisualizationTask::ESelectionTask) {
        TrasladarReleased();//aqui ya viene cambios, no peude ser esta funcion o si?
    }
    else if (CurrentVisualizationTask == EVRVisualizationTask::EVisualizationTask) {
    }
    else if (CurrentVisualizationTask == EVRVisualizationTask::EColorTask) {
    }
}

void ANJVRVisualization::RightHolding() {
    //trasladar pressed,
    bRightHold = true;
    //holdin en true, aun que es lo msmo que el nodo guia
    //en el releaseestara el trasladar released
    if (CurrentVisualizationTask == EVRVisualizationTask::ESelectionTask) {
        TrasladarPressed();//aqui ya viene cambios, no peude ser esta funcion o si?
    }
    else if (CurrentVisualizationTask == EVRVisualizationTask::EVisualizationTask) {
    }
    else if (CurrentVisualizationTask == EVRVisualizationTask::EColorTask) {
    }
}

void ANJVRVisualization::RightClick() {
    //si no estoy en una tarea en especial solo es seleccionar, en caso de que sea alguna, significa pintar, o visulizar contenido
    if (CurrentVisualizationTask == EVRVisualizationTask::ESelectionTask) {
        //selecciono
        SeleccionarNodoPressed();
    }
    else if (CurrentVisualizationTask == EVRVisualizationTask::EVisualizationTask) {
        VisualizarNodoPressed();
    }
    else if (CurrentVisualizationTask == EVRVisualizationTask::EColorTask) {//colorear rama o solo colorear nodo?
        ColorearRamaPressed();
    }
}

void ANJVRVisualization::LeftPressed() {
}

void ANJVRVisualization::LeftReleased() {
}

void ANJVRVisualization::LeftHolding() {
}

void ANJVRVisualization::LeftClick() {
}

