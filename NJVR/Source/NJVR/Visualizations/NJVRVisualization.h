// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Visualizations/VRVisualization.h"
#include "../Structures/Triangulo.h"
#include "ProceduralMeshComponent.h"
#include "../Drawers/SpheresDrawer.h"
#include "../Drawers/CylindersDrawer.h"
#include <vector>
#include "NJVRVisualization.generated.h"

/**
 * 
 */
UCLASS()
class NJVR_API ANJVRVisualization : public AVRVisualization
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

    virtual void EjecutarAnimaciones(float DeltaTime) override;

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void TestAnimation();

    bool NeedUpdateNodos;
    bool NeedUpdateAristas;

    ANJVRVisualization();

    ASpheresDrawer * NodosVisuales;

    ACylindersDrawer * AristasVisuales;

    int LayerNormal;

    TArray<int> LayersResaltado;

    int LayerSelected;

    virtual void CreateNodos() override;

    virtual void CreateVisualNodos() override;

    virtual void CreateAristas() override;

    virtual void CreateVisualAristas() override;

    virtual void DestroyNodos() override;

    virtual void DestroyVisualNodos() override;

    virtual void DestroyAristas() override;

    virtual void AplicarLayout() override;

    virtual void AplicarLayoutConAnimacion() override;
	
    virtual void EjecutarAnimacionCreacionArbol() override;


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    float RadioHoja;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    float DeltaDistanciaArista;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    float AreaHoja;

    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    void Calculos(Nodo * V);

    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    void Calculos2();

    void Calc();

    //conservare esto, ya que me permite llamar desde blueprint a las librerias, esto para tener acceso desde la aplicacion
    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutBase();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutDistanciaReducida();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutDistanciaAumentada();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutDistanciaAumentadaAnguloReducido();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutDistanciaAumentadaPromedioAnguloReducidoPorAltura();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutDistanciaAumentadaPromedioAnguloReducidoPorAlturaIndependienteHijo();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutDistanciaAumentadaProgresivaAnguloReducido();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutDistanciaAumentadaPromedioAnguloReducido();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutDistanciaAumentadaAnguloReducidoSinIntercalamiento();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutDistanciaAumentadaHijoAnguloReducido();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutMesa();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutEsferico();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutEsfericoLadrillos();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutRadial2D();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void LayoutRadial3D();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void ActualizarLayout();

    UPROPERTY(EditAnywhere, Category = "Visualization - Parametros")
    int PrecisionNodos; //NumeroLadosArista;//este tambien indica el tamano de los arreglos

    UPROPERTY(EditAnywhere, Category = "Visualization - Parametros")
    int UmbralSizeNodosProceduralMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial * NodosMeshMaterial;

	TArray<TArray<Nodo *>> SeccionNodos;//guarda los nodos que pertenencen a cada seccion

    void AsignarSeccionesNodos();

    int CreateSeccion();

    void AddNodoToMeshSeccion(int IdMesh, int IdSeccion, Nodo * Node);

    void AddRamaToMeshSeccion(int IdMesh, int IdSeccion, Nodo * Node);

    virtual void UpdateNodosMesh() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial * NodosSelectedMeshMaterial;

	TArray<TArray<Nodo *>> SeccionNodosSelected;//guarda los nodos que pertenencen a cada seccion

    int CreateSeccionNodoSelected();

	TArray<TArray<Nodo *>> SeccionNodosResaltados;//guarda los nodos que pertenencen a cada seccion

	void CreateMeshesResaltados();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial * AristasMeshMaterial;

	TArray<TArray<Arista *>> SeccionAristas;//guarda los nodos que pertenencen a cada seccion
	
    UPROPERTY(EditAnywhere, Category = "Visualization - Parametros")
    int PrecisionAristas; //NumeroLadosArista;//este tambien indica el tamano de los arreglos

    void AddAristaToMesh(FVector Source, FVector Target, int Radio, int NumArista);// precision es cuanto lados tendra el cilindo, minimo 3, radio, sera el radio del cilindro, este template no es tan adaptable como el de la esfera, por eso necesai dos parametros

    void CreateAristasMesh();

    void AsignarSeccionesAristas();//cra las seccines para las airstas y la asigna, siguinedo la estrucutura de lo asignado en elos nodos

    virtual void UpdateAristasMesh() override;

    virtual void ColorearNodo(Nodo * Node, FLinearColor NuevoColor) override;

    virtual void ColorearRama(Nodo * NodoSeleccionado, FLinearColor NuevoColor) override;

    virtual void TrasladarNodo(Nodo * Node, FVector VectorTraslacion) override;//podria tener una funcion cambiar posicion 

    virtual void SetLocationNodo(Nodo * Node, FVector NewLocation) override;//en el espacio de la visualizacion

    virtual FVector BuscarNodo(int &IdNodoEncontrado) override;

    virtual bool InterseccionConNodo(Nodo * Node, FVector & Interseccion, float & T) override;

    virtual void TraslacionConNodoGuia() override;

    //virtual void TrasladarRamaPressed() override;

    virtual void AplicarTraslacion(FVector Traslacion) override;
	
    //virtual void AplicarRotacionRelativaANodo(Nodo* NodoReferencia, FVector PuntoReferencia) override;
    virtual void FeedbackSeleccionarNodo(Nodo * NodoSeleccionado) override;

    virtual void FeedbackDeseleccionarNodo(Nodo * NodoSeleccionado) override;

    virtual void FeedbackResaltarNodo(Nodo * NodoSeleccionado, int ColorResaltado) override;

    virtual void FeedbackDesresaltarNodo(Nodo * NodoSeleccionado, int ColorResaltado) override;

    virtual void RightPressed() override;

    virtual void RightReleased() override;

    virtual void RightHolding() override;

    virtual void RightClick() override;

    virtual void LeftPressed() override;

    virtual void LeftReleased() override;

    virtual void LeftHolding() override;

    virtual void LeftClick() override;
};
