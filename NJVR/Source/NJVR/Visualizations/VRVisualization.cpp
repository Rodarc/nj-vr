// Fill out your copyright notice in the Description page of Project Settings.


#include "VRVisualization.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "../NJ/Arista.h"
#include "../NJ/Nodo.h"
#include "../NJ/NJ.h"
#include "../Pawns/VRPawn.h"
#include "Particles/ParticleSystemComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Misc/FileHelper.h"
#include <queue>
#include <vector>
#include <utility>
#include <algorithm>
#include "../Interfaz/ContentMenu.h"
#include "../Interfaz/ContentNodo.h"
#include "../Libraries/DatasetLibrary.h"
#include "../Libraries/ExportLibrary.h"

// Sets default values
AVRVisualization::AVRVisualization()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));

    bHitNodo = false;
    bNodoGuia = false;
    bInstanciarAristas = true;
    bColorear = true;

    Escala = 1.0f;
    RadioNodos = 3.0f;
    RadioNodosVirtuales = 2.0f;
    RadioAristas = 1.0f;
    DistanciaLaserMaxima = 500.0f;
    DistanciaLaser = DistanciaLaserMaxima;
    IntensidadColor = 0.4f;
    ColorSeleccion = FLinearColor::Green;
    //ColorVirtual = FLinearColor::White;
    ColorVirtual = FLinearColor(0.515625, 0.515625, 0.515625, 1.000000);
    //ColorReal = FLinearColor(0.0, 0.014444, 0.104616, 1.000000);
    //ColorReal = FLinearColor(0.0, 0.024449, 0.177083, 1.000000);
    ColorReal = FLinearColor(0.0, 0.027326, 0.197917, 1.000000);
    ColorPincel = ColorReal;


    /*PaletaColores.Add(UKismetMathLibrary::HSVToRGB(231.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(0.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(56.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(35.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(125.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(190.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(283.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(149.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(90.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color*/

    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(42.4362488f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(118.1415939f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(230.5003967f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(0.0f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(258.572937f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(9.6480713f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(155.224884f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color
    PaletaColores.Add(UKismetMathLibrary::HSVToRGB(313.3397522f, 1.0f, IntensidadColor, 1.0f));//el primero indica el color


    //static ConstructorHelpers::FClassFinder<UUserWidget> LabelNodoClass(TEXT("WidgetBlueprintGeneratedClass'/Game/NJVR/Blueprints/Interfaz/LabelNodoBP.LabelNodoBP_C'"));
    static ConstructorHelpers::FClassFinder<UUserWidget> LabelNodoClass(TEXT("WidgetBlueprintGeneratedClass'/Game/NJVR/Blueprints/Interfaz/MultiLineLabelNodoBP.MultiLineLabelNodoBP_C'"));
    if (LabelNodoClass.Succeeded()) {
        TypeLabelNodo = LabelNodoClass.Class;
    }
    WidgetCompLabelNodoActual = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget Component Label Nodo Actual"));
    WidgetCompLabelNodoActual->SetWidgetSpace(EWidgetSpace::World);
    WidgetCompLabelNodoActual->SetupAttachment(RootComponent);
    WidgetCompLabelNodoActual->SetDrawSize(FVector2D(300.0f, 250.0f));
    WidgetCompLabelNodoActual->SetPivot(FVector2D(0.5f, 1.0f));
    //WidgetCompLabelNodoActual->SetRelativeLocation(FVector(7.6604f, 0.0f, 6.42788f));
    //WidgetCompLabelNodoActual->SetRelativeRotation(FRotator(50.0f, 180.0f, 0.0f));
    WidgetCompLabelNodoActual->SetWorldScale3D(FVector(0.05f, 0.05f, 0.05f));
    WidgetCompLabelNodoActual->SetDrawAtDesiredSize(true);
    WidgetCompLabelNodoActual->SetVisibility(false);
    if (LabelNodoClass.Succeeded()) {
        WidgetCompLabelNodoActual->SetWidgetClass(LabelNodoClass.Class);
    }

//Blueprint'/Game/NJVR/Blueprints/Actors/ContentNodoActorBP.ContentNodoActorBP'
    static ConstructorHelpers::FClassFinder<AContentNodoActor> ContentActorClass(TEXT("BlueprintGeneratedClass'/Game/NJVR/Blueprints/Actors/ContentNodoActorBP.ContentNodoActorBP_C'"));
    //static ConstructorHelpers::FClassFinder<AStation> StationClass(TEXT("BlueprintGeneratedClass'/Game/GeoVisualization/Blueprints/Station_BP.Station_BP_C'"));
    if (ContentActorClass.Succeeded()) {
        //if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
            //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Station type founded."));
        TypeContentNodoActor = ContentActorClass.Class;
    }


    SetVisualizationMode(EVRVisualizationMode::ENoMode);// como inicia en ste mdo deberia parecer marcado, el boton correspondiente,
    //SetVisualizationTask(EVRVisualizationTask::ENoTask);//esta bien que empiece en ninguno, asi ningun boton tarea estara marcado
    SetVisualizationTask(EVRVisualizationTask::ESelectionTask);//esta bien que empiece en ninguno, asi ningun boton tarea estara marcado
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/cbr-ilp-ir-son.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/Sincbr-ilp-ir-son.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/Sincbr-ilp-ir.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/imagensCorel.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/SinimagensCorel.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/AMTesis.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/LeafShapes.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/BrocadoL8.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/pendigits-orig.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/cbr-ilp-ir-son-int.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/medicas12clases.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/medicas12classes.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/medicas12classescos.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/messages4.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/rssnewsfeed.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/rss_661.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/medicas12classesimage.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/mammals-4000.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/pendigits-orig-subset.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/Medical12Classes_cosine_part.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/Medical12Classes_cosine.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/Medical12Classes_euclidean.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/PB_images_corel_radial.xml"));
    DataSets.Add(FString("D:/Unreal Projects/NJVR/Content/Resources/PB_images_corel_force.xml"));
    DataSetSeleccionado = 0;
    //DataSetSeleccionado = 13;

    PathDataSets = FString("D:/Unreal Projects/NJVR/Content/Resources/");

    DataSetsNames.Add(FString("cbr-ilp-ir-son"));
    DataSetsNames.Add(FString("Sincbr-ilp-ir-son"));
    DataSetsNames.Add(FString("Sincbr-ilp-ir"));
    DataSetsNames.Add(FString("imagensCorel"));
    DataSetsNames.Add(FString("SinimagensCorel"));
    DataSetsNames.Add(FString("AMTesis"));
    DataSetsNames.Add(FString("LeafShapes"));
    DataSetsNames.Add(FString("BrocadoL8"));
    DataSetsNames.Add(FString("pendigits-orig"));
    DataSetsNames.Add(FString("cbr-ilp-ir-son-int"));
    DataSetsNames.Add(FString("medicas12clases"));
    DataSetsNames.Add(FString("medicas12classes"));
    DataSetsNames.Add(FString("medicas12classescos"));
    DataSetsNames.Add(FString("messages4"));
    DataSetsNames.Add(FString("rssnewsfeed"));
    DataSetsNames.Add(FString("rss_661"));
    DataSetsNames.Add(FString("medicas12classesimage"));
    DataSetsNames.Add(FString("mammals-4000"));
    DataSetsNames.Add(FString("pendigits-orig-subset"));
    DataSetsNames.Add(FString("Medical12Classes_cosine_part"));
    DataSetsNames.Add(FString("Medical12Classes_cosine"));
    DataSetsNames.Add(FString("Medical12Classes_euclidean"));
    DataSetsNames.Add(FString("PB_images_corel_force"));
    DataSetsNames.Add(FString("PB_images_corel_force"));

    XmlSourceP = new FXmlFile;
}

// Called when the game starts or when spawned
void AVRVisualization::BeginPlay()
{
	Super::BeginPlay();
    WidgetCompLabelNodoActual->SetWorldScale3D(FVector(0.05));

	for (int i = 0; i < 8; i++) {
		NodosResaltados.AddDefaulted();
	}
	
    AVRPawn * MyVRPawn = Cast<AVRPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
    if (MyVRPawn) {
        RightController = MyVRPawn->MotionControllerRight;
        LeftController = MyVRPawn->MotionControllerLeft;
        MyVRPawn->Visualization = this;
        Interaction = MyVRPawn->Interaction;
        //Document = MyVRPawn->Document;//no se si deba estar esto aqui
        Usuario = MyVRPawn;
    }

	UWorld * const World = GetWorld();
    if (World) {
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = GetInstigator();

        PanelBotones = World->SpawnActor<APanelBotones>(APanelBotones::StaticClass(), GetActorLocation() + FVector(70.0f, 0.0f, -90.0f), FRotator(0.0f, 180.0f, 0.0f), SpawnParams);
        //PanelBotones->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
    }

    bool cargado = XmlSource.LoadFile(DataSets[DataSetSeleccionado], EConstructMethod::ConstructFromFile);//para construirlo como archivo
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Probando."));
        if(cargado)
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Cargado."));
        else
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("No cargado."));
    }
    //if (cargado) {
        //TArray<FElement *> Elementos = UDatasetLibrary::CreateElementsFromDatasetXml(PathDataSets + DataSetsNames[DataSetSeleccionado] + "/" + DataSetsNames[DataSetSeleccionado] + "fd.xml", EElementType::EImageType);
        UDatasetLibrary::CreateVectorsFromFile(PathDataSets + DataSetsNames[DataSetSeleccionado] + "/" + DataSetsNames[DataSetSeleccionado] + ".data", VectoresCaracteristicas, NombreDimensiones);
        /*float ** Matriz = UDatasetLibrary::ConstruirMatrizDeDistancias(VectoresCaracteristicas, Elementos.Num(), NombreDimensiones.Num());//para construir la matriz de distancias
        TArray<FString> NombresElementos;
        for (int i = 0; i < Elementos.Num(); i++) {
            NombresElementos.Add(Elementos[i]->FileName);
        }
        UDatasetLibrary::SaveDistanceMatrixInFile(PathDataSets + DataSetsNames[DataSetSeleccionado] + "/" + DataSetsNames[DataSetSeleccionado] + ".dmat", NombresElementos, Matriz);*/
        //LoadNodos();
        CreateNodos();
        //TArray<FElement *> Elementos;
        //TArray<int> numerocolores;
        //UDatasetLibrary::CreateVectorsAndElementsFromFile(PathDataSets + DataSetsNames[DataSetSeleccionado] + "/" + DataSetsNames[DataSetSeleccionado] + ".data", VectoresCaracteristicas, NombreDimensiones, Elementos);
        //float ** Matriz = UDatasetLibrary::ConstruirMatrizDeDistancias(VectoresCaracteristicas, Elementos.Num(), NombreDimensiones.Num());//para construir la matriz de distancias

		//copiando la informacion de clusters de los elementos de leidos a los nodos creados
		/*if (Elementos.Num()) {
			for(int i = 0; i < Elementos.Num(); i++){
				Nodos[i]->ColorNum = Elementos[i]->Cluster;
				Nodos[i]->Color = Colores[Nodos[i]->ColorNum];
			}
		}*/
        //float ** VectoresC = nullptr;
        //TArray<FString> NombreDimensiones;
        //siempre y cuando exita el archivo
        /*UE_LOG(LogClass, Log, TEXT("Numero dimensiones %d"), NombreDimensiones.Num());
        if (VectoresCaracteristicas) {
            if (VectoresCaracteristicas[0]) {
                for (int i = 0; i < NombreDimensiones.Num(); i++) {
                    UE_LOG(LogClass, Log, TEXT("dimension %d = %f"), i, VectoresCaracteristicas[0][i]);
                }
            }
        }*/
        //CalcularNJ();
        CreateVisualNodos();
        if (bInstanciarAristas) {
            CreateAristas();
            CreateVisualAristas();
        }
    //}
}

// Called every frame
void AVRVisualization::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

void AVRVisualization::EjecutarAnimaciones(float DeltaTime) {
}

void AVRVisualization::EjecutarAnimacionCreacionArbol() {
}

void AVRVisualization::LoadNodos() {
    FXmlNode * rootnode = XmlSource.GetRootNode();
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, rootnode->GetTag());
    }
    TArray<FXmlNode *> XMLnodos = rootnode->GetChildrenNodes();
    TArray<FXmlNode*> XMLvertexs;
    //for(FXmlNode* nodo : nodos){
        //if (nodo->GetTag() == "vertex") {
            //vertexs.Add(nodo);
        //}
    for (int i = 0; i < XMLnodos.Num(); ++i) {
        if (XMLnodos[i]->GetTag() == "vertex") {
            XMLvertexs.Add(XMLnodos[i]);
        }
    }
    //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::FromInt(vertexs.Num()));
    //tengo todos los vertices en ese array
    for (int i = 0; i < XMLvertexs.Num(); ++i) {
        //obteniendo el id
        FString id = XMLvertexs[i]->GetAttribute(FString("id"));//devuelve el valor del atributo que le doy, hay otra funocin que me devuelve todos los atributos en un arrya de un obejto especial//quiza deba esto guardarlo como int cuando genere la clase Vertex
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, id);
        UE_LOG(LogClass, Log, TEXT("Vertex id = %s"), *id);
        
        //obteniendo el valid
        FXmlNode * nodevalid = XMLvertexs[i]->FindChildNode(FString("valid"));
        FString valid = nodevalid->GetAttribute("value");
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, valid);
        UE_LOG(LogClass, Log, TEXT("valid = %s"), *valid);

        //obteniendo la x-coordinate
        FXmlNode * nodex = XMLvertexs[i]->FindChildNode(FString("x-coordinate"));
        FString xcoordinate = nodex->GetAttribute("value");
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, xcoordinate);
        UE_LOG(LogClass, Log, TEXT("x-coordinate = %s"), *xcoordinate);

        //obteniendo la y-coordinate
        FXmlNode * nodey = XMLvertexs[i]->FindChildNode(FString("y-coordinate"));
        FString ycoordinate = nodey->GetAttribute("value");
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, ycoordinate);
        UE_LOG(LogClass, Log, TEXT("y-coordinate = %s"), *ycoordinate);

        //obteniendo url
        FXmlNode * nodeurl = XMLvertexs[i]->FindChildNode(FString("url"));
        FString url = nodeurl->GetAttribute("value");
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, url);
        UE_LOG(LogClass, Log, TEXT("url = %s"), *url);

        //obteniendo parent 
        FXmlNode * nodeparent = XMLvertexs[i]->FindChildNode(FString("parent"));//quiza no sean necesario usar FString
        FString parent = nodeparent->GetAttribute("value");
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, parent);
        UE_LOG(LogClass, Log, TEXT("parent = %s"), *parent);

        //los hijos no estan dentro de un array por lo tanto es necesario reccorrer todos los child nose, es decir lo de aqui arruba fue por las puras, jeje
        TArray<FString> sons;
        TArray<FXmlNode*> childs = XMLvertexs[i]->GetChildrenNodes();
        for (int j = 0; j < childs.Num(); j++) {
            if (childs[j]->GetTag() == "son") {
                sons.Add(childs[j]->GetAttribute("value"));
            }
        }
        for (int j = 0; j < sons.Num(); j++) {
            //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, sons[j]);
            UE_LOG(LogClass, Log, TEXT("son = %s"), *sons[j]);
        }

        FXmlNode * nodelabels = XMLvertexs[i]->FindChildNode(FString("labels"));//quiza no sean necesario usar FString
        TArray<FXmlNode*> labelschilds = nodelabels->GetChildrenNodes();
        TArray<FString> labels;
        for (int j = 0; j < labelschilds.Num(); j++) {
            labels.Add(labelschilds[j]->GetAttribute("value"));
            //aqui faltaria definir que label es cada uno, para poder ponerlo en la variable que corresponda en el la calse vertex que creare
        }
        for (int j = 0; j < labels.Num(); j++) {
            //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, labels[j]);
            UE_LOG(LogClass, Log, TEXT("label = %s"), *labels[j]);
        }
        //el contenido de los nodos, es lo que hay en trexto plano dentro del tag de apertura y de cierre

        //TArray<FXmlNode*> childs = vertexs[i]->GetChildrenNodes();//para el caso de los vertexs sus hijos son unicos o son un array por lo tanto podria usar la funcion findchildren, para encontrar los que necesito

    }
}

void AVRVisualization::CreateNodos() {
}

void AVRVisualization::CreateVisualNodos() {
}

void AVRVisualization::CreateAristas() {
}

void AVRVisualization::CreateVisualAristas() {
}

void AVRVisualization::DestroyNodos() {
    for (int i = 0; i < Nodos.Num(); i++) {
		delete Nodos[i];
    }
    Nodos.Empty();
}

void AVRVisualization::DestroyVisualNodos() {
}

void AVRVisualization::DestroyAristas() {
    for (int i = 0; i < Aristas.Num(); i++) {//debo eliminarlos, y tambien debo 
		delete Aristas[i];
    }
    Aristas.Empty();
	//debo eliminar los array y hacer update
}

void AVRVisualization::AplicarLayout() {
}

void AVRVisualization::AplicarLayoutConAnimacion() {
}

void AVRVisualization::CargarDataSet(int indice) {
    DataSetSeleccionado = indice;
    //XmlSourceP = new FXmlFile;
    //XmlSource.Clear();
    //bool cargado = XmlSourceP->LoadFile(DataSets[DataSetSeleccionado], EConstructMethod::ConstructFromFile);//para construirlo como archivo
    bool cargado = XmlSource.LoadFile(DataSets[DataSetSeleccionado], EConstructMethod::ConstructFromFile);//para construirlo como archivo
    if (GEngine) {
        //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Dataset %d", indice));
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Probando."));
        if(cargado)
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Cargado."));
        else
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("No cargado."));
    }
    //DestroyAristas();
    //DestroyNodos();
    if (cargado) {
        //LoadNodos();
        CreateNodos();
        if(bInstanciarAristas)
            CreateAristas();
        AplicarLayout();
    }
}

void AVRVisualization::LimpiarVisualizacion() {
    //XmlSourceP->~FXmlFile();
	//debo destruir todos los contenidos que hayan, y ocultar el label actual
	for (int i = 0; i < Contenidos.Num(); i++) {
		Contenidos[i]->Destroy();
	}
	Contenidos.Empty();
    DestroyAristas();
    DestroyNodos();
	//tambien debo destruir el arbol o lo que tenga como arbol
}

int AVRVisualization::NivelMasDenso() {//de todo el arbol
    int NivelDensoMaximo = 0;
    int CantidadNodosMaximo = 0;
    int CantidadNodosNivelActual = 0;
    int NivelActual = 1;
    std::queue<Nodo *> Cola;
    //la raiz es el ultimo nodo
    Nodo * Root = Nodos[Nodos.Num() - 1];
    //UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->Xcoordinate, Root->Ycoordinate, Root->Zcoordinate);
    Cola.push(Root->Padre);
    for (int i = 0; i < 2 /*Root->Hijos.Num()*/; i++) {//este deberia ser menor que 2, o usar vector para evitar errores
        if (Root->Hijos[i]) {
            Cola.push(Root->Hijos[i]);
        }
    }
    CantidadNodosNivelActual = Cola.size();
    if (CantidadNodosNivelActual >= CantidadNodosMaximo) {// deberia camniar si estoy ma abajo o arriba??
        CantidadNodosMaximo = CantidadNodosNivelActual;
        NivelDensoMaximo = NivelActual;
    }
    NivelActual++;//iniciaria el bucle con 2, pero los nodos ubicados en la cola son de nivel 1
    while(!Cola.empty()){
        while (!Cola.empty() && Cola.front()->Nivel < NivelActual) {
            Nodo * V;
            V = Cola.front();
            Cola.pop();
            for (int i = 0; i < 2 /*V->Sons.Num()*/; i++) {//el problema es que no se si son nulos o no
                if (V->Hijos[i]) {
                    Cola.push(V->Hijos[i]);
                }
            }
        }
        CantidadNodosNivelActual = Cola.size();
        if (CantidadNodosNivelActual >= CantidadNodosMaximo) {// deberia camniar si estoy ma abajo o arriba??
            CantidadNodosMaximo = CantidadNodosNivelActual;
            NivelDensoMaximo = NivelActual;
        }
        NivelActual++;
    }
    return NivelDensoMaximo;
}

void AVRVisualization::NivelMasDenso(int & NivelDenso, int & CantidadNodos) {
    int NivelDensoMaximo = 0;
    int CantidadNodosMaximo = 0;
    int CantidadNodosNivelActual = 0;
    int NivelActual = 1;
    std::queue<Nodo *> Cola;
    //la raiz es el ultimo nodo
    Nodo * Root = Nodos[Nodos.Num() - 1];
    //UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->Xcoordinate, Root->Ycoordinate, Root->Zcoordinate);
    Cola.push(Root->Padre);
    for (int i = 0; i < 2; i++) {
        if (Root->Hijos[i]) {
            Cola.push(Root->Hijos[i]);
        }
    }
    CantidadNodosNivelActual = Cola.size();
    if (CantidadNodosNivelActual >= CantidadNodosMaximo) {// deberia camniar si estoy ma abajo o arriba??
        CantidadNodosMaximo = CantidadNodosNivelActual;
        NivelDensoMaximo = NivelActual;
    }
    NivelActual++;//iniciaria el bucle con 2, pero los nodos ubicados en la cola son de nivel 1
    while(!Cola.empty()){
        while (!Cola.empty() && Cola.front()->Nivel < NivelActual) {
            Nodo * V;
            V = Cola.front();
            Cola.pop();
            for (int i = 0; i < 2; i++) {
                if (V->Hijos[i]) {
                    Cola.push(V->Hijos[i]);
                }
            }
        }
        CantidadNodosNivelActual = Cola.size();
        if (CantidadNodosNivelActual >= CantidadNodosMaximo) {// deberia camniar si estoy ma abajo o arriba??
            CantidadNodosMaximo = CantidadNodosNivelActual;
            NivelDensoMaximo = NivelActual;
        }
        NivelActual++;
    }
    NivelDenso = NivelDensoMaximo;
    CantidadNodos = CantidadNodosMaximo;
}

int AVRVisualization::NivelMasDensoRama(Nodo * Node) {//de todo el arbol, deeria tomarme en cuenta, osea el nodo inicial,
    int NivelDensoMaximo = Node->Nivel;
    int CantidadNodosMaximo = 1;
    int CantidadNodosNivelActual = 1;
    int NivelActual = Node->Nivel + 1;//estaba solo 1
    std::queue<Nodo *> Cola;
    Cola.push(Node);
    while(!Cola.empty()){
        while (!Cola.empty() && Cola.front()->Nivel < NivelActual) {
            Nodo * V;
            V = Cola.front();
            Cola.pop();
            for (int i = 0; i < 2; i++) {
                if (V->Hijos[i]) {
                    Cola.push(V->Hijos[i]);
                }
            }
        }
        CantidadNodosNivelActual = Cola.size();
        if (CantidadNodosNivelActual >= CantidadNodosMaximo) {// deberia camniar si estoy ma abajo o arriba??
            CantidadNodosMaximo = CantidadNodosNivelActual;
            NivelDensoMaximo = NivelActual;
        }
        NivelActual++;
    }
    return NivelDensoMaximo;//el valor de retorno es respecto al arbol general, no de forma relativba a la rama
}

EVRVisualizationTask AVRVisualization::GetVisualizationTask() {
    return CurrentVisualizationTask;
}

void AVRVisualization::SetVisualizationTask(EVRVisualizationTask NewVisualizationTask) {
    CurrentVisualizationTask = NewVisualizationTask;
}

EVRVisualizationMode AVRVisualization::GetVisualizationMode() {
    return CurrentVisualizationMode;
}

void AVRVisualization::RotarVisualizacion() {
    FRotator DeltaRotation = RightController->GetComponentRotation() - InitialRotationController;
    SetActorRotation(InitialRotation + DeltaRotation);
}

void AVRVisualization::CalcularEscalaTemporal() {
    float DistanciaEntreControles = (RightController->GetComponentLocation() - LeftController->GetComponentLocation()).Size();
    EscalaTemp = Escala + (DistanciaEntreControles - DistanciaInicialControles) / DistanciaInicialControles;
}

FVector AVRVisualization::BuscarNodo(int & IdNodoEncontrado) {
    IdNodoEncontrado = -1;
    return FVector();
}

void AVRVisualization::SetVisualizationMode(EVRVisualizationMode NewVisualizationMode) {
    CurrentVisualizationMode = NewVisualizationMode;
}

int AVRVisualization::mod(int a, int b) {
    int d = a / b;
    int m = a - b*d;
    if (m < 0)
        m += b;
    return m;
}

float AVRVisualization::modFloat(float a, float b) {
    //por ahora es solo para el exceso en resta, para el esferico nunca exceden el doblre asi que sera solo la suma para hacerlo positivo
    if (a > b) {
        return a - b;
    }
    if (a < 0) {
        return a + b;
    }
    return a;
}

void AVRVisualization::BuscandoNodoConLaser() {
    int IdNodoEncontrado;
    FVector PuntoImpacto = BuscarNodo(IdNodoEncontrado);
    //dilema existencial, debo usar aqui, el Id o el Nodo
    Nodo * NodoEncontrado = nullptr;
    if (IdNodoEncontrado != -1) {
        NodoEncontrado = Nodos[IdNodoEncontrado];
    }
    if (NodoEncontrado && !Interaction->IsOverHitTestVisibleWidget()) {//comprobamos la interaccion para que no se detecte lo que este detras del menu
		if (HitNodo) {//si en la llamada anterior ya habia apuntado a un nodo
			if (HitNodo != NodoEncontrado) {
                OcultarLabelNodo(HitNodo);
                MostrarLabelNodo(NodoEncontrado);
                //ActualizarCambiosVisuales();
                //UpdateNodosMesh();
				HitNodo = NodoEncontrado;//podria dejar ests 3 lineas, y borrar las de adentro
				bHitNodo = true;
			}
            ImpactPoint = PuntoImpacto;
		}
		else {//no tenian ningun nodo antes
			MostrarLabelNodo(NodoEncontrado);
			//ActualizarCambiosVisuales();
            //UpdateNodosMesh();
			HitNodo = NodoEncontrado;//podria dejar ests 3 lineas, y borrar las de adentro
			bHitNodo = true;
			ImpactPoint = PuntoImpacto;
		}
    }
    else {//no encontre ningun nodo
        if (HitNodo) {
            //if (MostrarLabel) {
			OcultarLabelNodo(HitNodo);
			//ActualizarCambiosVisuales();
            //UpdateNodosMesh();
			HitNodo = nullptr;
			bHitNodo = false;
        }
        //HitNodo = nullptr;
        //bHitNodo = false;
        ImpactPoint = PuntoImpacto;//para actualizar el limite maximo
        //el caso contrario, seria encontrar como lo deje con el if anterior, asi que no se hace nada
    }
    //todo esto podria ser una sola funcion
    //hasta aqui he verificado si encontre algun nodo, pero no si encotnre un, menu, y tampoo he ejecutado los cambios visuales
    //hagamos algo visual, antes de incluir los menus
    if (bHitNodo) {//quiza la verificacion que hago sobre si hubo cambio o no de hit nodo, ayude a evitar ciertos calculos, tal vez, por ejemplo si el laser siempre esa al maximo, no tiene mucho sentido seimpre setear con el mismo valor
        Usuario->CambiarPuntoFinal(GetTransform().TransformPosition(ImpactPoint));
        Usuario->EfectoImpacto->SetWorldLocation(GetTransform().TransformPosition(HitNodo->GetLocation()));
        //esto no estaba
        //if (MostrarLabel && WidgetCompLabelNodoActual->IsVisible()) {
            //WidgetCompLabelNodoActual->ActualizarRotacionNombre(Usuario->VRCamera->GetComponentLocation() - HitNodo->Nombre->GetComponentLocation());
            //WidgetCompLabelNodoActual->ActualizarRotacionNombre(Usuario->VRCamera->GetComponentLocation());
        //}
        if(Usuario->LaserActual() != 1){
            Usuario->CambiarLaser(1);
        }
        if (!Usuario->EfectoImpacto->IsVisible()) {//para el caso del puntero, no lo usao ahora
            Usuario->EfectoImpacto->SetVisibility(true);
        }
    }
    else {
        Usuario->CambiarPuntoFinal(RightController->GetComponentLocation() + RightController->GetForwardVector()*DistanciaLaserMaxima);//debieria tener un punto por defecto, pero mejor lo dejamos asi
        //esta funcion deberia administrar le punto recbido, y verficar si acutalmente el puntero de interaccion esta sobre el menu, y tomar el adecuado para cada situacion
        if(Usuario->LaserActual() != 0){
            Usuario->CambiarLaser(0);
        }
        if (Usuario->EfectoImpacto->IsVisible()) {
            Usuario->EfectoImpacto->SetVisibility(false);
        }
    }
    //creo que la parte de interacion con el menu, deberia estar manajedo por el pawn, asi dentro de la funcion cambiar punto final, evaluo o verifico que no este primero algun menu
    //la pregunta es como hare con los clicks digamos para el contenido, si estoy buscando algun nodo, quiza igual deberia evitar que de algun click, si tengo algun overlap en ferente, evaluar la mejor forma de hacer todo esto
    //o usar esto en lugar de un trace solo que debo hacer esto antes de que haga cambios visuales, obtener el punto y evaluar,  antes de setear lo de hit nodo y dema
}

void AVRVisualization::VisualizarNodo() {//se llama en cada tick
    if (NodosSeleccionados.Num()) {
        for (int i = 0; i < NodosSeleccionados.Num(); i++) {
            //NodosSeleccionados[i]->ActualizarRotacionNombre(Usuario->VRCamera->GetComponentLocation() - NodosSeleccionados[i]->Nombre->GetComponentLocation());
        }
    }
    BuscandoNodoConLaser();
}

FVector AVRVisualization::InterseccionLineaSuperficie() {
    return FVector();
}

void AVRVisualization::TraslacionConNodoGuia() {

}

void AVRVisualization::RotacionRama() {
}

void AVRVisualization::TraslacionVisualizacion() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    FVector PuntoInicial = RightController->GetComponentLocation();//lo mismo que en teorioa, GetComponentTransfor().GetLocation();
    FVector Vec = RightController->GetForwardVector();
    FVector PuntoFinal = PuntoInicial + Vec*DistanciaLaser;
    if(Usuario->LaserActual() != 6){
        Usuario->CambiarLaser(6);
    }
    Usuario->CambiarPuntoFinal(PuntoFinal);
    SetActorLocation(PuntoFinal + OffsetToPointLaser);
}

void AVRVisualization::SeleccionarNodoPressed() {
    if (bHitNodo) {
        if (HitNodo->Selected) {
            //DeseleccionarNodo(HitNodo);
            DeseleccionarRama(HitNodo);
        }
        else {
            //SeleccionarNodo(HitNodo);//seleccionar no deberia implicar cambio de color
            SeleccionarRama(HitNodo);//seleccionar no deberia implicar cambio de color
        }
        //ColorearNodo(HitNodo, ColorSeleccion);
        //ActualizarCambiosVisuales();//quiza no deberia estar esto aqui
        UpdateNodosMesh();
        //NodoGuia = HitNodo;
        //bNodoGuia = true;
		//DistanciaLaser = GetTransform().InverseTransformVector((GetTransform().TransformPosition(ImpactPoint) - RightController->GetComponentLocation())).Size();
    }
}

void AVRVisualization::TrasladarNodoPressed() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    if (bHitNodo) {
        //ColorearNodo(HitNodo, ColorSeleccion);
        SeleccionarNodo(HitNodo);//seleccionar no deberia implicar cambio de color
        //ActualizarCambiosVisuales();//quiza no deberia estar esto aqui
        UpdateNodosMesh();
        NodoGuia = HitNodo;
        bNodoGuia = true;
		DistanciaLaser = GetTransform().InverseTransformVector((GetTransform().TransformPosition(ImpactPoint) - RightController->GetComponentLocation())).Size();
    }
}

void AVRVisualization::VisualizarNodoPressed() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    if (bHitNodo) {
        //SeleccionarNodo(HitNodo);
        //if (HitNodo->Valido) {
            MostrarContenidoNodo(HitNodo);
        //}
        //HitNodo->MostrarNombre();
        //Cast<UControlMenu2VR>(Document->GetUserWidgetObject());
    }
}

void AVRVisualization::TrasladarRamaPressed() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    if (bHitNodo) {
        SeleccionarRama(HitNodo);//como coloreo la rama?
		//ColorearRama(HitNodo, ColorSeleccion);//creo que no hare esto
        NodoGuia = HitNodo;
        bNodoGuia = true;
		DistanciaLaser = GetTransform().InverseTransformVector((GetTransform().TransformPosition(ImpactPoint) - RightController->GetComponentLocation())).Size();
    }
}

void AVRVisualization::RotarRamaPressed() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    if (bHitNodo) {
        SeleccionarRama(HitNodo);
        NodoGuia = HitNodo;
        bNodoGuia = true;
    }
}

void AVRVisualization::ColorearRamaPressed() {
    if (bHitNodo) {
        ColorearRama(HitNodo, ColorPincel);
    }
}

void AVRVisualization::TrasladarTodoPressed() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    /*SeleccionarTodo();
    if (bHitNodo) {
        NodoGuia = HitNodo;
        bNodoGuia = true;
    }*/
}

void AVRVisualization::TrasladarVisualizacionPressed() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    if(bHitNodo){
        FVector WorldImpactPoint = GetTransform().TransformPosition(ImpactPoint);
		DistanciaLaser = (WorldImpactPoint - RightController->GetComponentLocation()).Size();
        OffsetToPointLaser = GetActorLocation() - WorldImpactPoint;
		//NodoGuia = HitNodo;
        bNodoGuia = true;
    }
}

void AVRVisualization::RotarVisualizacionPressed() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    InitialRotationController = RightController->GetComponentRotation();
    InitialRotation = GetActorRotation();
    Rotar = true;
}

void AVRVisualization::SeleccionarNodoReleased() {
    //cuando haga release por ahora no hago nada
    //aun que podria habre un feedbakc a modo de click
}

void AVRVisualization::TrasladarNodoReleased() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
	if(bNodoGuia){
		//DeseleccionarTodo();//creoque debo deseleccionar el nodo
		DeseleccionarNodo(NodoGuia);
        UpdateNodosMesh();
		//ActualizarCambiosVisuales();
		bNodoGuia = false;
		//for (int i = 0; i < Aristas.Num(); i++) {
			//Aristas[i]->ActualizarCollision();
		//}
		Usuario->CambiarLaser(0);
	}
    //Usuario->CambiarPuntoFinal(GetTransform().TransformPosition(ImpactPoint));
}

void AVRVisualization::VisualizarNodoReleased() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    /*for (int i = 0; i < NodosSeleccionados.Num(); i++) {
        NodosSeleccionados[i]->OcultarNombre();
    }
    DeseleccionarTodo();
    Usuario->CambiarLaser(0);*/
}

void AVRVisualization::TrasladarRamaReleased() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    if(bNodoGuia){
        DeseleccionarRama(NodoGuia);//con el deseleccionar todo tambien bastaria
        UpdateNodosMesh();
		//ActualizarCambiosVisuales();
        bNodoGuia = false;
        //for (int i = 0; i < Aristas.Num(); i++) {
            //Aristas[i]->ActualizarCollision();
        //}
        Usuario->CambiarLaser(0);
    }
}

void AVRVisualization::RotarRamaReleased() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    /*if(bNodoGuia){
        bNodoGuia = false;
        for (int i = 0; i < Aristas.Num(); i++) {
            Aristas[i]->ActualizarCollision();
        }
        DeseleccionarTodo();
        Usuario->CambiarLaser(0);
    }*/
}

void AVRVisualization::TrasladarVisualizacionReleased() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    bNodoGuia = false;
    Usuario->CambiarLaser(0);
}

void AVRVisualization::RotarVisualizacionReleased() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    Rotar = false;
    Usuario->CambiarLaser(0);
}

void AVRVisualization::TrasladarTodoReleased() {//esta es una funcion gloabl, ponerlo en la clase padre, analizar estas cosas
    /*DeseleccionarTodo();
    bNodoGuia = false;
    Usuario->CambiarLaser(0);*/
}

void AVRVisualization::TrasladarPressed() {
    if (bHitNodo && HitNodo->Selected) {//debo verificar que este seleccionado
        //SeleccionarRama(HitNodo);//ya hay seleccion
        NodoGuia = HitNodo;
        bNodoGuia = true;
		DistanciaLaser = GetTransform().InverseTransformVector((GetTransform().TransformPosition(ImpactPoint) - RightController->GetComponentLocation())).Size();
    }
}

void AVRVisualization::TrasladarReleased() {
    if(bNodoGuia){
        //DeseleccionarRama(NodoGuia);//con el deseleccionar todo tambien bastaria
        UpdateNodosMesh();
		//ActualizarCambiosVisuales();
        bNodoGuia = false;
        //for (int i = 0; i < Aristas.Num(); i++) {
            //Aristas[i]->ActualizarCollision();
        //}
        Usuario->CambiarLaser(0);
    }
}

void AVRVisualization::AsignarColorPincel(int IndicePaletaColores) {
    ColorPincel = PaletaColores[IndicePaletaColores];
}

bool AVRVisualization::InterseccionConNodo(Nodo * Node, FVector & Interseccion, float & T) {
    return false;
}

void AVRVisualization::FeedbackSeleccionarNodo(Nodo * NodoSeleccionado) {
}

void AVRVisualization::FeedbackDeseleccionarNodo(Nodo * NodoSeleccionado) {
}

void AVRVisualization::FeedbackResaltarNodo(Nodo * NodoSeleccionado, int ColorResaltado) {
}

void AVRVisualization::FeedbackDesresaltarNodo(Nodo * NodoSeleccionado, int ColorResaltado) {
}

void AVRVisualization::SeleccionarNodo(Nodo * NodoSeleccionado) {
    NodoSeleccionado->Selected = true;
    //NodoSeleccionado->CambiarColor(ColorSeleccion);
    FeedbackSeleccionarNodo(NodoSeleccionado);
    //ColorearNodo(NodoSeleccionado, ColorSeleccion);
    NodosSeleccionados.Add(NodoSeleccionado);
    //UpdateNodosMesh();
}

void AVRVisualization::DeseleccionarNodo(Nodo * NodoSeleccionado) {
    NodoSeleccionado->Selected = false;
    //NodoSeleccionado->CambiarColor(NodoSeleccionado->Color);
    //ColorearNodo(NodoSeleccionado, NodoSeleccionado->Color);
    FeedbackDeseleccionarNodo(NodoSeleccionado);
    NodosSeleccionados.Remove(NodoSeleccionado);
    //UpdateNodosMesh();
}

void AVRVisualization::SeleccionarRama(Nodo * NodoSeleccionado) {

	UE_LOG(LogClass, Log, TEXT("Nodo id = %d, file = %s"), NodoSeleccionado->Id, *(NodoSeleccionado->Url));
    TQueue<Nodo*> Cola;
    Cola.Enqueue(NodoSeleccionado);
    while (!Cola.IsEmpty()) {
        Cola.Dequeue(NodoSeleccionado);
        if (!NodoSeleccionado->Selected) {
            NodoSeleccionado->Selected = true;
            //NodoSeleccionado->CambiarColor(ColorSeleccion);
            //ColorearNodo(NodoSeleccionado, ColorSeleccion);
            FeedbackSeleccionarNodo(NodoSeleccionado);
            NodosSeleccionados.Add(NodoSeleccionado);
            if (NodoSeleccionado->Hijos[0]) {
                for (int i = 0; i < 2; i++) {
                    Cola.Enqueue(NodoSeleccionado->Hijos[i]);
                }
            }
        }
    }
}

void AVRVisualization::DeseleccionarRama(Nodo * NodoSeleccionado) {
    TQueue<Nodo*> Cola;
    Cola.Enqueue(NodoSeleccionado);
    while (!Cola.IsEmpty()) {
        Cola.Dequeue(NodoSeleccionado);
        if (NodoSeleccionado->Selected) {
            NodoSeleccionado->Selected = false;
            //NodoSeleccionado->CambiarColor(NodoSeleccionado->Color);
            //ColorearNodo(NodoSeleccionado, NodoSeleccionado->Color);
            FeedbackDeseleccionarNodo(NodoSeleccionado);
            NodosSeleccionados.Remove(NodoSeleccionado);
            if (NodoSeleccionado->Hijos[0]) {
                for (int i = 0; i < 2; i++) {
                    Cola.Enqueue(NodoSeleccionado->Hijos[i]);
                }
            }
        }
    }
}

void AVRVisualization::ColorearNodo(Nodo * Node, FLinearColor NuevoColor) {
}

void AVRVisualization::ColorearRama(Nodo * NodoSeleccionado, FLinearColor NuevoColor) {
    TQueue<Nodo*> Cola;
    Cola.Enqueue(NodoSeleccionado);
    while (!Cola.IsEmpty()) {
        Cola.Dequeue(NodoSeleccionado);
        if (NodoSeleccionado->Valido) {
            //NodoSeleccionado->CambiarColor(NuevoColor);
            ColorearNodo(NodoSeleccionado, NuevoColor);
        }
        if (NodoSeleccionado->Hijos[0]) {//este if esta en el anterior tambien, los validos no tienen hijos
            for (int i = 0; i < 2; i++) {
                Cola.Enqueue(NodoSeleccionado->Hijos[i]);
            }
        }
    }
}

void AVRVisualization::ResaltarNodo(Nodo * NodoResaltado, int ColorResaltado) {
    NodoResaltado->Resaltado = true;
    FeedbackResaltarNodo(NodoResaltado, ColorResaltado);
    NodosResaltados[ColorResaltado].Add(NodoResaltado);
}

void AVRVisualization::DesresaltarNodo(Nodo * NodoResaltado, int ColorResaltado) {
    NodoResaltado->Resaltado= false;
    FeedbackDesresaltarNodo(NodoResaltado, ColorResaltado);
    NodosResaltados[ColorResaltado].Remove(NodoResaltado);
}

void AVRVisualization::DesresaltarColor(int ColorResaltado) {
	TArray<Nodo *> NodosE = NodosResaltados[ColorResaltado];
	for (int i = 0; i < NodosE.Num(); i++) {
		DesresaltarNodo(NodosE[i], ColorResaltado);
	}
}

void AVRVisualization::TrasladarNodo(Nodo * Node, FVector VectorTraslacion) {
}

void AVRVisualization::SetLocationNodo(Nodo * Node, FVector NewPosition) {
}

void AVRVisualization::MostrarNumeracion() {
    /*for (int i = 0; i < Nodos.Num(); i++)
        Nodos[i]->Numero->SetVisibility(true);
    */
}

void AVRVisualization::OcultarNumeracion() {
    /*for (int i = 0; i < Nodos.Num(); i++)
        Nodos[i]->Numero->SetVisibility(false);*/
}

void AVRVisualization::ColorGeneral() {
    for (int i = 0; i < Nodos.Num(); i++) {
        if (Nodos[i]->Valido) {
            //Nodos[i]->CambiarColor(ColorReal);
            ColorearNodo(Nodos[i], ColorReal);
        }
    }
	UpdateNodosMesh();
}

void AVRVisualization::ColorClase() {
    for (int i = 0; i < Nodos.Num(); i++) {
        if (Nodos[i]->Valido) {
            //Nodos[i]->CambiarColor(Nodos[i]->Color);
            ColorearNodo(Nodos[i], Nodos[i]->Color);
        }
    }
	UpdateNodosMesh();
}

void AVRVisualization::UpdateNodosMesh() {
}

void AVRVisualization::UpdateAristasMesh() {
}

void AVRVisualization::MostrarLabelNodo(Nodo * Node) {
    if (Node->Valido) {
        WidgetCompLabelNodoActual->SetVisibility(true);
        WidgetCompLabelNodoActual->SetRelativeLocation(Node->GetLocation() + FVector(0.0f, 0.0f, RadioNodos));
        ULabelNodo * Label = Cast<ULabelNodo>(WidgetCompLabelNodoActual->GetUserWidgetObject());
        if (Label) {
            Label->NameNodo = FString (Node->Labels[0]);
            //Label->NameNodo = FString(Node->Nombre.c_str());
            Label->ActualizarLabel();
            //Label->NameNodo = "hola cambiando el nombre del nodo.";
        }
    }
    //ColorearNodo(Node, ColorSeleccion);//quitar, el efecto que deberia haber, es hacerse mas grande o algo asi
	//este efecto tampoco deberia estar aqui, el label no esta asociado al cambio de color,
}

void AVRVisualization::OcultarLabelNodo(Nodo * Node) {
    if (Node->Valido) {
        WidgetCompLabelNodoActual->SetVisibility(false);
    }
    //ColorearNodo(Node, Node->Color);//deberia haber una animacion de hacerse mas grande o algo asi
}

FString AVRVisualization::ObtenerContenidoNodo(Nodo * Node) {
    FString contenido;
    FString archivo("D:/Unreal Projects/NJVR/Content/Resources/cbr-ilp-ir-son/");
    archivo += Node->Url;
	UE_LOG(LogClass, Log, TEXT("nodo = %s"), *Node->Url);
    FFileHelper::LoadFileToString(contenido, *archivo);
    return contenido;
}

void AVRVisualization::MostrarContenidoNodo(Nodo * Node) {
    if (Node->Valido) {
        /*UWorld * const World = GetWorld();
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = Instigator;

        //FVector SpawnLocation(LatitudLongitudToXY(Stations[i].latitud, Stations[i].longitud));
        AContentNodoActor * const InstancedContentActor = World->SpawnActor<AContentNodoActor>(TypeContentNodoActor, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
        if (InstancedContentActor) {
            Contenidos.Add(InstancedContentActor);
            //InstancedContentActor->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
            InstancedContentActor->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);//segun el compilador de unral debo usar esto
            //InstancedContentActor->SetActorScale3D(FVector(1.0f));
            //no deberia hacer attach a este actor
            InstancedContentActor->Node = Node;
            InstancedContentActor->Usuario = Usuario;
            //ubicarme en el lugar del nodo
            InstancedContentActor->SetContent(ObtenerContenidoNodo(Node));
            //InstancedContentActor->SetActorRelativeLocation(Node->GetLocation());//falta convertir la posicion del nodo en la posicion global, aunque si le hago attach, deberia ser otra posicion
            FVector LocationContent = LocationForContenidoNodo(Node);
            InstancedContentActor->SetActorRelativeLocation(LocationContent);//falta convertir la posicion del nodo en la posicion global, aunque si le hago attach, deberia ser otra posicion
            //debo mandar a actualizar la informacion del Nodo;
        }*/

        UContentMenu * MenuContenido = Cast<UContentMenu>(PanelBotones->MenuContenido->GetUserWidgetObject());
        if (MenuContenido) {
            UContentNodo * ContenidoNodo = MenuContenido->CrearConentido();
            ContenidoNodo->SetNodo(Node);
            ContenidoNodo->SetContent(ObtenerContenidoNodo(Node));
            ContenidoNodo->ActualizarContent();
            ContenidoNodo->OutlineColor = Node->Color;
        }
    }
}

FVector AVRVisualization::LocationForContenidoNodo(Nodo * Node) {//sera trabajado en el espacio relativo, es decir en el espacio de la visaulizacion, ya que los contenidos estan como hijos de la visualizcion, esto podria cambiar en el futuro, si no quiciera que lo esten
    FVector UserLocation = GetTransform().InverseTransformPosition(Usuario->VRCamera->GetComponentLocation());
    FVector NormalPlano = (Node->GetLocation() - UserLocation).GetSafeNormal();
    //el centro siempre esta en 0
    float D = -(NormalPlano.X*UserLocation.X + NormalPlano.Y*UserLocation.Y + NormalPlano.Z*UserLocation.Z);
    float t = (-D/* - NormalPlano.X - NormalPlano.Y - NormalPlano.Z*/) / (NormalPlano.X*NormalPlano.X + NormalPlano.Y*NormalPlano.Y + NormalPlano.Z*NormalPlano.Z);//coo 0 vale 0,, ya no existe lo que acompaña a la D
    float Offset = 400.0f*GetActorScale3D().X;//por el valor de la escala, no se por que
    float DistanceToUser = 200.0f;
    FVector PC = t * NormalPlano;//en el 0,0,0, por eso no pongo punto de paso;
    FVector PCN = UserLocation + Offset * (UserLocation - PC).GetSafeNormal();
    return PCN + DistanceToUser*NormalPlano;
}

void AVRVisualization::SeleccionarTodo() {
    NodosSeleccionados = Nodos;// esta accion borra todos los elementos y le copia los elementos del segundo array //pero ahora debo marcarlos y 
    for (int i = 0; i < NodosSeleccionados.Num(); i++) {
        NodosSeleccionados[i]->Selected = true;
        //NodosSeleccionados[i]->CambiarColor(ColorSeleccion);
    }
}

void AVRVisualization::DeseleccionarTodo() {
	TArray<Nodo *> TempNodos = NodosSeleccionados;
    for (int i = 0; i < TempNodos.Num(); i++) {
		DeseleccionarNodo(TempNodos[i]);
    }
}

void AVRVisualization::DesresaltarTodo() {
    for (int i = 0; i < PaletaColores.Num(); i++) {
        DesresaltarColor(i);
    }
	UpdateNodosMesh();
}

void AVRVisualization::AplicarTraslacion(FVector Traslacion) {
    //algunso layouts requieres hacer mas cosas para la traslacion por eso aun no esta genral, por ejemplo, en el h3 la traslacion requiere modificar las matrices de cada nodo
}

void AVRVisualization::AplicarEscala(float NuevaEscala) {
    //esta forma es menos costosa, y parece mas sencialla de manipular todos los elementos, solo hya que hacer que todo tenga concordancia, no como la arista actual
    SetActorScale3D(FVector(NuevaEscala));
    WidgetCompLabelNodoActual->SetWorldScale3D(FVector(0.05));
    //FTransform NuevoTransform = GetTransform();
    //NuevoTransform.SetScale3D(FVector(NuevaEscala));
    //SetActorTransform(NuevoTransform);
}

void AVRVisualization::RotarLabelsNodo() {
    FRotator NewRotation = FRotationMatrix::MakeFromX(Usuario->VRCamera->GetComponentLocation() - WidgetCompLabelNodoActual->GetComponentLocation()).Rotator();
    WidgetCompLabelNodoActual->SetWorldRotation(NewRotation);
    //NewRotation = FRotationMatrix::MakeFromX(Direccion - Nombre->GetComponentLocation()).Rotator();
    //NewRotation = FRotationMatrix::MakeFromX(Direccion).Rotator();
    //Nombre->SetWorldRotation(NewRotation);
}

bool SortVectorCaracteristicas(pair<int, float> & a, pair<int, float> & b) {
    return a.second > b.second;
}

FString AVRVisualization::ObtenerTemaNodosSeleccionados(int NumeroTemas) {
    float * VectorResultante = new float[NombreDimensiones.Num()]; 
    for (int j = 0; j < NombreDimensiones.Num(); j++) {
        VectorResultante[j] = 0.0f;
    }
    for (int i = 0; i < NodosSeleccionados.Num(); i++) {
        if (NodosSeleccionados[i]->Valido) {
            for (int j = 0; j < NombreDimensiones.Num(); j++) {
                VectorResultante[j] += VectoresCaracteristicas[NodosSeleccionados[i]->Id][j];
            }
        }
    }
    vector<pair<int, float> > Prioridad (NombreDimensiones.Num());
    for (int i = 0; i < Prioridad.size(); i++) {
        Prioridad[i].first = i;
        Prioridad[i].second = VectorResultante[i];
    }

    sort(Prioridad.begin(), Prioridad.end(), SortVectorCaracteristicas);
    //me quedo con los n que tengan mas terminos
    //crear una cola o algo?
    //el sort parece buena idea, pero debi haber almacenado pairs
    //procedo a crear un label con los terminos escogidos separado por comas
    FString Temas;
    for(int i = 0;  i < NumeroTemas && i < Prioridad.size() && Prioridad[i].second > 0.0f; i++){
        Temas += NombreDimensiones[Prioridad[i].first] + "\n";
        UE_LOG(LogClass, Log, TEXT("Tema: %s (%f)"), *NombreDimensiones[Prioridad[i].first], Prioridad[i].second);
    }
    delete [] VectorResultante;
    return Temas;
}

void AVRVisualization::CreateLabelTemasForSelection(int NumeroTemas) {
    FString Temas;
    if (NodosSeleccionados.Num()) {// creo que este if es innecesario
		Temas = ObtenerTemaNodosSeleccionados(NumeroTemas);
    }
    UContentMenu * MenuLabel = Cast<UContentMenu>(PanelBotones->MenuLabel->GetUserWidgetObject());

	int ColorOutline = 0;
    if (MenuLabel) {
        UContentNodo * LabelSeleccion = MenuLabel->CrearConentido();
        LabelSeleccion->Title = "Temas";
        LabelSeleccion->SetContent(Temas);
        LabelSeleccion->ActualizarContent();
		ColorOutline = MenuLabel->Contenidos.Num() - 1;
        LabelSeleccion->OutlineColor = PaletaColores[ColorOutline];
    }

	//ademas de crear el label, debo realtar los nodos, y deseleccionarlos
	TArray<Nodo *> Feedback = NodosSeleccionados;
	for (int i = 0; i < Feedback.Num(); i++) {
		DeseleccionarNodo(Feedback[i]);
		ResaltarNodo(Feedback[i], ColorOutline);
	}
	UpdateNodosMesh();
}

void AVRVisualization::CreateContentsForSelection() {
    for (int i = 0; i < NodosSeleccionados.Num(); i++) {
        if (NodosSeleccionados[i]->Valido) {
            MostrarContenidoNodo(NodosSeleccionados[i]);
        }
    }
}

void AVRVisualization::RightPressed() {
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Right Pressed."));
    }
}

void AVRVisualization::RightReleased() {
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Right Released."));
    }
}

void AVRVisualization::RightHolding() {
    //acciono hold, para que en el tick se ejecute lo que deba ejecutarse
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Right Holding."));
    }
}

void AVRVisualization::RightClick() {
    //procedo a seleccionar en caso de que tenga un nodo apuntado
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Right Click."));
    }
}

void AVRVisualization::LeftPressed() {
}

void AVRVisualization::LeftReleased() {
}

void AVRVisualization::LeftHolding() {
}

void AVRVisualization::LeftClick() {
}

void AVRVisualization::Question(int Q) {
    DeseleccionarTodo();
    if (Q == 1) {
        ColorGeneral();
    }
    else if (Q == 2) {
        ColorClase();
    }
    else if (Q == 3) {
        //ColorearCluster(0, FLinearColor::Black);
        if (DataSetSeleccionado == 1) {
            SeleccionarNodo(Nodos[650]);
        }
        else if (DataSetSeleccionado == 21) {
            SeleccionarNodo(Nodos[208]);
        }
    }
    else if (Q == 7) {
        if (DataSetSeleccionado == 1) {
            SeleccionarRama(Nodos[1332]);
        }
        else if (DataSetSeleccionado == 21) {
            SeleccionarRama(Nodos[1064]);
        }
    }
    else if (Q == 8) {
        if (DataSetSeleccionado == 1) {
            SeleccionarNodo(Nodos[657]);
        }
        else if (DataSetSeleccionado == 21) {
            SeleccionarNodo(Nodos[404]);
        }
    }
    else if (Q == 9) {
        if (DataSetSeleccionado == 1) {
            SeleccionarRama(Nodos[1337]);
        }
        else if (DataSetSeleccionado == 21) {
            SeleccionarRama(Nodos[930]);
        }
    }
    UpdateNodosMesh();
}

void AVRVisualization::ColorearCluster(int Cluster, FLinearColor NuevoColor) {
    for (int i = 0; i < Nodos.Num(); i++) {
        if (Nodos[i]->Valido && Nodos[i]->ColorNum == Cluster) {
            ColorearNodo(Nodos[i], NuevoColor);
        }
    }
    UpdateNodosMesh();
}

void AVRVisualization::CalcularNJ() {
    //LeerDatosDePrueba();//aqui deberia ir construir matriz de distancias
    //ConstruirMatrizDeDistanciasDeConsulta();
	//leer los vectores de caracteristicas, y crear elementos, con estos elementos crear la matriz de distanacias, y crear los nombres, para luego pasarlos al NJ
	//del dataset seleccionado
	//ya no leo nada de xmls
    TArray<int> numerocolores;

	//TArray<FElement *> Elementos = UDatasetLibrary::CreateElementsFromDatasetXml(PathDataSets + DataSetsNames[DataSetSeleccionado] + "/" + DataSetsNames[DataSetSeleccionado] + "fd.xml", EElementType::EImageType);
	TArray<FElement *> Elementos;
	UDatasetLibrary::CreateVectorsAndElementsFromFile(PathDataSets + DataSetsNames[DataSetSeleccionado] + "/" + DataSetsNames[DataSetSeleccionado] + ".data", VectoresCaracteristicas, NombreDimensiones, Elementos);
	//float ** Matriz = UDatasetLibrary::ConstruirMatrizDeDistancias(VectoresCaracteristicas, Elementos.Num(), NombreDimensiones.Num());//para construir la matriz de distancias
    float ** Matriz;
    UDatasetLibrary::CreateDistanceMatrixFromFile(PathDataSets + DataSetsNames[DataSetSeleccionado] + "/" + DataSetsNames[DataSetSeleccionado] + ".dmat", Matriz);
	TArray<FString> NombresElementos;
	string * NombresEle = new string[Elementos.Num()];
	for (int i = 0; i < Elementos.Num(); i++) {
		NombresElementos.Add(Elementos[i]->FileName);
		NombresEle[i] = TCHAR_TO_UTF8(*(Elementos[i]->FileName));
        numerocolores.AddUnique(Elementos[i]->Cluster);
	}
	//UDatasetLibrary::SaveDistanceMatrixInFile(PathDataSets + DataSetsNames[DataSetSeleccionado] + "/" + DataSetsNames[DataSetSeleccionado] + ".dmat", NombresElementos, Matriz);


    NJ * AlgoritmoNJ = new NJ;
    AlgoritmoNJ->DatosIniciales(NombresEle, Elementos.Num());
	Nodo ** ArbolGenerado = nullptr;
    int TamArbolGenerado = AlgoritmoNJ->GenerarArbol(Matriz, Elementos.Num(), ArbolGenerado);//los nodos de este arbol se han quedado guardados en ArbolGeneradoa, siendo su ultimo elemento la raiz
    //ImprimirNodos(ArbolGenerado, TamArbolGenerado);
	//copiar al vector de nodos
    delete AlgoritmoNJ;
    //AlgoritmoNJ = nullptr;
    for(int i = 0; i < Elementos.Num(); i++){
        delete [] Matriz[i];
    }
    delete [] Matriz;
    delete [] NombresEle;
	//esta funcion no esta haciendo clear de los arrays de la visualizacion

	//copiar el el arbol generado al tarrya y asignar los colores y otra data que antes se hacia

    for(int i = 0; i < TamArbolGenerado; i++){//hay informacion que le falta a los nodos
        Nodos.Add(ArbolGenerado[i]);
        //NodoInstanciado->GetRootComponent()->SetupAttachment(RootComponent);// para hacerlo hioj de la visualización, aunque parece que esto no es suficient
    }
    //UE_LOG(LogClass, Log, TEXT("NumeroColor = %d"), numerocolores.Num());
    int variacion = 360 / numerocolores.Num();
    for (int k = 0; k < numerocolores.Num(); k++) {
        int h = (k*variacion) % 360;
        Colores.Add(UKismetMathLibrary::HSVToRGB(h, 1.0f, IntensidadColor, 1.0f));
    }
    //añandiendo nodos al procedular mesh
    for (int i = 0; i < Nodos.Num(); i++) {
        Nodos[i]->Selected = false;
        if (Nodos[i]->Valido) {//tiene hijos
            //estableciendo parametros y copiando informacion de su elemento
            Nodos[i]->ColorNum = Elementos[i]->Cluster;
            Nodos[i]->Color = Colores[Nodos[i]->ColorNum];//eliminar para darle un color uniforme a los nodos al inicio (negro)
            Nodos[i]->FileName = Elementos[i]->FileName;
            //AddNodoToMesh(FVector(Nodos[i]->X, Nodos[i]->Y, Nodos[i]->Z), RadioNodos, Nodos[i]->Color, i);
        }
        else {//no es valido
            Nodos[i]->Color = ColorVirtual;
        }
    }
    //Calculos2();
    //Calc();//no estaba antes

}

void AVRVisualization::ExportVisualization() {
    if (GEngine) {
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Exportando."));
    }
    UE_LOG(LogClass, Log, TEXT("exportando"));
    UExportLibrary::ExportToMetricasFormat(TCHAR_TO_UTF8(*(DataSetsNames[DataSetSeleccionado])), Nodos, RadioNodos, RadioNodosVirtuales, 3, Colores.Num());
}

