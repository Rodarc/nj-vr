// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "XmlParser.h"
#include "Components/WidgetComponent.h"
#include "../Interfaz/LabelNodo.h"
#include "../Interfaz/ContentNodo.h"
#include "../Actors/ContentNodoActor.h"
#include "../Actors/PanelBotones.h"
#include "VRVisualization.generated.h"


UENUM(BlueprintType)
enum class EVRVisualizationTask : uint8 {
    ESelectionTask UMETA(DisplayName = "Seleccionar"),
    ETraslationTask UMETA(DisplayName = "Trasladar"),
    ERotationTask UMETA(DisplayName = "Rotar"),
    EScaleTask UMETA(DisplayName = "Escalar"),
    EColorTask UMETA(DisplayName = "Colorear"),
    EVisualizationTask UMETA(DisplayName = "Visualizar"),
    ENoTask UMETA(DisplayName = "Ninguno")
};

UENUM(BlueprintType)//para que nos permita usarlo en blueprint, en el blueprint del HUD
enum class EVRVisualizationMode : uint8 {//al parecer estos estados son predefinidos
    EAll UMETA(DisplayName = "Todo"),
    ENode UMETA(DisplayName = "Nodo"),
    EBranch UMETA(DisplayName = "Rama"),
    EArea UMETA(DisplayName = "Area"),
    ENoMode UMETA(DisplayName = "Niguno")//quiza este debe estar en el otro modo, cuado quiere ver lo de editar, debe haber un modo seleecion traslacion rotar, editar, y un ninguno, que seria este, por ahora lo usare como  es, pero despues este representara el deseleccionar todo lo que haya
};//estado por defecto cuando algunas cosas no se han establecido aun

UCLASS()
class NJVR_API AVRVisualization : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVRVisualization();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    virtual void EjecutarAnimaciones(float DeltaTime);

    bool bAnimatingConstruction;

    int CurrentNodeConstruction;

    float TimeStepConstruction;
    
    float ActualTimeStepConstruction;

    virtual void EjecutarAnimacionCreacionArbol();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Interfaz")
    APanelBotones * PanelBotones;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    FString PathDataSets;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    TArray<FString> DataSetsNames;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    TArray<FString> DataSets;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    int DataSetSeleccionado;

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    float ** VectoresCaracteristicas;

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    TArray<FString> NombreDimensiones;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Referencias")
    class AVRPawn* Usuario;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Referencias")
    class UMotionControllerComponent* RightController;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Referencias")
    class UMotionControllerComponent* LeftController;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - ElementosVisuales")
    bool bColorear;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - ElementosVisuales")
    bool bInstanciarAristas;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool bHitNodo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool bHitBoard;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    FVector ImpactPoint;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Referencias")
    AActor* HitActor;

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Referencias")
    class Nodo* HitNodo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Referencias")
    AActor* HitLimite;

    //Dimension que ocupara cada nodo
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    float RadioNodos;

    //Dimension que ocupara cada nodo
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    float RadioNodosVirtuales;

    //Dimension que ocupara cada arista
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    float RadioAristas;

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    TArray<class Nodo *> Nodos;

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    TArray<class Arista*> Aristas;

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    TArray<class Nodo*> NodosSeleccionados;

    TArray<TArray<class Nodo*>> NodosResaltados;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool bNodoGuia;

    class Nodo * NodoGuia;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    float IntensidadColor;// 0 a 1

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    TArray<FLinearColor> Colores;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    FLinearColor ColorSeleccion;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    FLinearColor ColorVirtual;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    FLinearColor ColorReal;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    FLinearColor ColorPincel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Elementos Visuales")
    TArray<FLinearColor> PaletaColores;

    FXmlFile XmlSource;// o podria tener un puntero e ir genereando nuevos FXmlFile, todo debepnde, eso por que el contructor podria recibir el path, al ser creado, 

    FXmlFile * XmlSourceP;// o podria tener un puntero e ir genereando nuevos FXmlFile, todo debepnde, eso por que el contructor podria recibir el path, al ser creado, 
	
    void LoadNodos();//solo esto por que solo me interesa leer la informacion de los vertex, para saber quienes son hijos y padres, por eso tal vez no se trabaje como unity o si, probar

    virtual void CreateNodos();

    virtual void CreateVisualNodos();

    virtual void CreateAristas();

    virtual void CreateVisualAristas();

    virtual void DestroyNodos();

    virtual void DestroyVisualNodos();

    virtual void DestroyAristas();
    
    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void AplicarLayout();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void AplicarLayoutConAnimacion();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void CargarDataSet(int indice);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void LimpiarVisualizacion();


    int mod(int a, int b);

    float modFloat(float a, float b);

    int NivelMasDenso();

    void NivelMasDenso(int & NivelDenso, int & CantidadNodos);

    int NivelMasDensoRama(Nodo * Node);

    //Task
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Modos y Tareas")
    EVRVisualizationTask CurrentVisualizationTask;
	
    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void SetVisualizationTask(EVRVisualizationTask NewVisualizationTask);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    EVRVisualizationTask GetVisualizationTask();

    //Mode
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Modos y Tareas")
    EVRVisualizationMode CurrentVisualizationMode;

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void SetVisualizationMode(EVRVisualizationMode NewVisualizationMode);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    EVRVisualizationMode GetVisualizationMode();

    //Para el modo general
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    float DistanciaLaserMaxima;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    float DistanciaLaser;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    FVector OffsetToPointLaser;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    float Escala;

    //Rotacion del controll capturado al presionar el trigger
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    FRotator InitialRotationController;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    FRotator InitialRotation;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool Rotar;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool MostrarLabel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool MostrarNumero;

    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Referencias")
    //class UWidgetComponent * Document;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Referencias")
    class UWidgetInteractionComponent * Interaction;

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void MostrarNumeracion();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void OcultarNumeracion();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void ColorGeneral();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void ColorClase();

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool bGripIzquierdo;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool bGripDerecho;

    UPROPERTY(BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool bCalcularEscala;// si no quiero que aparezca en el editor

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    float DistanciaInicialControles;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    float EscalaTemp;

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void RotarVisualizacion();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void CalcularEscalaTemporal();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual FVector BuscarNodo(int &IdNodoEncontrado);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void BuscandoNodoConLaser();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void VisualizarNodo();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual FVector InterseccionLineaSuperficie();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TraslacionConNodoGuia();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void RotacionRama();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TraslacionVisualizacion();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void SeleccionarNodoPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarNodoPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void VisualizarNodoPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarRamaPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void RotarRamaPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void ColorearRamaPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarTodoPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarVisualizacionPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void RotarVisualizacionPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void SeleccionarNodoReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarNodoReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void VisualizarNodoReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarRamaReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void RotarRamaReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarVisualizacionReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void RotarVisualizacionReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarTodoReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarPressed();//para la interaccion AM

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TrasladarReleased();//para la interaccion AM

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void AsignarColorPincel(int IndicePaletaColores);//esto deberia ser un linear color dado por un color picker o algo asi


    virtual bool InterseccionConNodo(Nodo * Node, FVector & Interseccion, float & T);

    virtual void FeedbackSeleccionarNodo(Nodo * NodoSeleccionado);

    virtual void FeedbackDeseleccionarNodo(Nodo * NodoSeleccionado);

    virtual void FeedbackResaltarNodo(Nodo * NodoSeleccionado, int ColorResaltado);

    virtual void FeedbackDesresaltarNodo(Nodo * NodoSeleccionado, int ColorResaltado);

    //acciones sobre nodos
    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    void SeleccionarNodo(Nodo * NodoSeleccionado);

    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    void DeseleccionarNodo(Nodo * NodoSeleccionado);

    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    void SeleccionarRama(Nodo * NodoSeleccionado);

    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    void DeseleccionarRama(Nodo * NodoSeleccionado);

    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void ColorearNodo(Nodo * Node, FLinearColor NuevoColor);

    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void ColorearRama(Nodo * NodoSeleccionado, FLinearColor NuevoColor);

    //acciones sobre nodos
    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    void ResaltarNodo(Nodo * NodoResaltado, int ColorResaltado);

    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    void DesresaltarNodo(Nodo * NodoResaltado, int ColorResaltado);

	void DesresaltarColor(int ColorResaltado);

    virtual void TrasladarNodo(Nodo * Node, FVector VectorTraslacion);//podria tener una funcion cambiar posicion 

    virtual void SetLocationNodo(Nodo * Node, FVector NewPosition);//podria tener una funcion cambiar posicion 

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * WidgetCompLabelNodoActual;//este es el nodo label que siempre tengo activo, si en algun momento fijo el label de un nodo, pues creo uno uevo copio la inof y se guarda en un arrya, y este se oculta temporalmente hasta que le apunte a otro nodo
    //todos estos componentes deben mantener la escala global de 0.15
    //despues de aplicar escala debo llamar sobre estos elementos setworld scale

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    TSubclassOf<ULabelNodo> TypeLabelNodo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    TSubclassOf<UContentNodo> TypeContentNodo;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    TArray<UWidgetComponent *> WidgetCompentsContenidoNodo;//este es el nodo label que siempre tengo activo, si en algun momento fijo el label de un nodo, pues creo uno uevo copio la inof y se guarda en un arrya, y este se oculta temporalmente hasta que le apunte a otro nodo

    virtual void UpdateNodosMesh();

    virtual void UpdateAristasMesh();

    //TArray<UWidgetComponent *> LabelsNodosActivos;//no se necesito esto

    void MostrarLabelNodo(Nodo * Node);

    void OcultarLabelNodo(Nodo * Node);

    //UFUNCTION(BlueprintCallable, Category = "Visualization")
    FString ObtenerContenidoNodo(Nodo * Node);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stations - Barra")
    TSubclassOf<AContentNodoActor> TypeContentNodoActor;

    TArray<AContentNodoActor *> Contenidos;

    void MostrarContenidoNodo(Nodo * Node);

    FVector LocationForContenidoNodo(Nodo * Node);

    //void OcultarLabelNodo(Nodo * Node);//se cerrara con el boton,
    
    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void SeleccionarTodo();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void DeseleccionarTodo();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void DesresaltarTodo();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void AplicarTraslacion(FVector Traslacion);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void AplicarEscala(float NuevaEscala);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void RotarLabelsNodo();//plura por si uso un array para mostrar varios labels, o los labels de los seleccionados

    FString ObtenerTemaNodosSeleccionados(int NumeroTemas);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void CreateLabelTemasForSelection(int NumeroTemas);

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void CreateContentsForSelection();

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool bRightHold;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization - Auxiliar")
    bool bLeftHold;

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void RightPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void RightReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void RightHolding();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void RightClick();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void LeftPressed();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void LeftReleased();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void LeftHolding();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void LeftClick();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void Question(int Q);

    virtual void ColorearCluster(int Cluster, FLinearColor NuevoColor);

	void CalcularNJ();


    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void ExportVisualization();

}; 