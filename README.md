# NJ-VR

## Descripción

Herramienta de visulaizción de árboles de similutd en realidad virtual.

## Screens
![njvr](Screens/NJVR.jpg "NJVR")

![Etiquetado](Screens/EtiquetadoNodo.jpg "Etiquetado")

![Contenido](Screens/ContenidoNodos.jpg "Contenido")

![TemaGrupos](Screens/TemaGrupos.jpg "Tema Grupos")

![TemaSubramas](Screens/TemaSubramas.jpg "Tema Subramas")

![Escalabilidad](Screens/NJVREscalabilidad.jpg "Escalabilidad")

## Requisitos

- Unreal Engine 4.26
- HTC VIVE
- Datasets

